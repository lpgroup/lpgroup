# vite-plugin-replace

Replace a string with another string in all code files.
Primary needed for the entry-point index.html file.

## Usage

```js
// vite.config.ts
import replace from "@lpgroup/vite-plugin-replace";

const replaceArr = [
  { src: "http://localhost:8189/harper-0.0.1.umd.js", production: "/harper/harper-0.0.1.umd.js" },
];

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    plugins: [
      // ...otherplugins
      replace(replaceArr, mode),
    ],
  };
});
```
