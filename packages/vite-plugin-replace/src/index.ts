import type { Plugin } from "vite";
import type { UserOptions } from "./lib/options.js";
export type { UserOptions } from "./lib/options.js";

function escapeRegExp(string: string) {
  return string.replaceAll(/[.*+?^${}()|[\]\\]/g, String.raw`\$&`); // $& means the whole matched string
}

function replaceAll(previous: string, find: string, replace: string): string {
  return previous.replaceAll(new RegExp(escapeRegExp(find), "g"), replace);
}

export const VitePluginReplace = (userOptions: UserOptions = {}): Plugin => {
  const options: UserOptions = {
    replace: [],
    mode: "development",
    ...userOptions,
  };
  const { replace, mode } = options;

  const transformIndexHtml = (html: string) => {
    const result = replace?.reduce<string>((previousValue, currentValue) => {
      if (mode === "production" && currentValue.production !== undefined) {
        return replaceAll(previousValue, currentValue.src, currentValue.production);
      }
      if (mode === "development" && currentValue.development !== undefined) {
        return replaceAll(previousValue, currentValue.src, currentValue.production);
      }

      return previousValue;
    }, html);

    return result;
  };

  return {
    name: "vite-plugin-pr",
    enforce: "post",
    transform(source) {
      return { code: transformIndexHtml(source), map: undefined };
    },
    transformIndexHtml,
  };
};
