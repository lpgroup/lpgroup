/**
 * MPA Plugin options.
 */

export interface ReplaceEntry {
  src: string;
  production: string;
  development: string;
}

export interface MpaOptions {
  /**
   * array of object with key/value replace
   * @default []
   */
  replace: ReplaceEntry[];
  /**
   * production or development mode, can get this from vite.
   * @default 'development'
   */
  mode: string;
}

export type UserOptions = Partial<MpaOptions>;
