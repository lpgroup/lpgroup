# @lpgroup/feathers-mongodb-hooks

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-mongodb-hooks.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-mongodb-hooks) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-mongodb-hooks/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-mongodb-hooks)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Collection of hooks to use togheter with fethers-mongodb

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-mongodb-hooks
```

## Example

```js
const auth = require("@lpgroup/feathers-mongodb-hooks");
```

## API

### `xxx`

#### `xxx(xxx)`

```js
xxx(xxx);
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
