import { startGetAndLockTransaction } from "../sessions.js";

export default (options = {}) =>
  async (context) => {
    await startGetAndLockTransaction(context, options.collections);

    return context;
  };
