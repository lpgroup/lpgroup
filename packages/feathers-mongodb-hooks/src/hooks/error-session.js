import { log } from "@lpgroup/utils";
import { endSession, debugMsg } from "../sessions.js";

const { error } = log("database");

function shouldLogError(err) {
  if (["jwt expired"].includes(err.message)) return false;

  // 405 method not allowed
  if ([405, 404].includes(err.code)) return false;

  // 11000 - some mongodb error
  // E11000 duplicate key error collection:
  if ([11000].includes(err.code)) return false;
  if (err.message.startsWith("E11000 duplicate key error")) return false;

  // Not found in authentication
  if (err.code === 401 && err.message.startsWith("Invalid login")) return false;
  // TODO: Maybe not neccessary anymore.
  if (err.data && [11000].includes(err.data.code)) return false;

  // Transaction writeconflict
  if (err.codeName === "WriteConflict") return false;

  return true;
}

// eslint-disable-next-line no-unused-vars
export default (options = {}) =>
  async (context) => {
    debugMsg("errorSession ", context, context.error.message);

    if (context?.error?.errorLabels?.includes("TransientTransactionError")) {
      error("Should do a retry ");
    }

    if (shouldLogError(context.error)) {
      error(
        "feathers-mongodb-hooks/error-session: ",
        context.method,
        context.path,
        context.error.codeName,
        context.error.Symbol,
        "\n",
        context.error.message,
        context.error.errors,
        "\n",
        context.error.inheritedStack || "",
        context.error.stack,
        context.data,
      );
    }

    await endSession(context.params);

    return context;
  };
