import { pick } from "lodash-es";
import { format } from "date-fns";
import { onPluginReady } from "@lpgroup/feathers-plugins";

async function incCounter(counterCollection, _id, group, period, step, description) {
  const now = new Date();
  const query = { _id };
  const insertData = {
    _id,
    group,
    period,
    description,
    added: { at: now.getTime(), by: "superuser" },
    owner: { user: { _id: "superuser" } },
  };

  const updateData = {
    changed: { at: now.getTime(), by: "superuser" },
  };

  const update = { $inc: { value: step }, $set: updateData, $setOnInsert: insertData };
  const options = { upsert: true, returnDocument: "after" };
  const result = await counterCollection.findOneAndUpdate(query, update, options);
  return result;
}

const transactCount = async (_id, group, period, step, description) => {
  const { database } = await onPluginReady("mongodb");
  const counterCollection = await database.collection("counters");
  const result = await incCounter(counterCollection, _id, group, period, step, description);
  return result;
};

const replaceDoubleBraces = (uniqueFormat, item, query) => {
  return uniqueFormat.replace(/{(.+?)}/g, (_, g1) => {
    return query[g1] || item[g1] || g1;
  });
};

export function parseFormat(context, uniqueFormat) {
  const item = context.data || context.result || {};
  return replaceDoubleBraces(uniqueFormat, item, context?.params?.query || {});
}

function parsePeriod(periodFmt) {
  const now = new Date();
  return periodFmt ? format(now, periodFmt) : null;
}

export const updateOrCreateCount = async (
  context,
  uniqueFormat,
  periodFmt,
  step = 1,
  description = null,
) => {
  const group = parseFormat(context, uniqueFormat);
  const period = parsePeriod(periodFmt);
  const _id = period ? `${group}-${period}` : group;
  const res = await transactCount(_id, group, period, step, description);
  const counterInstance = pick(res, ["_id", "alias", "period", "value"]);
  return counterInstance;
};
