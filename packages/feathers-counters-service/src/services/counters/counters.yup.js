import * as cy from "@lpgroup/yup";

const requestSchema = {
  _id: cy.id(),
  group: cy.shortText().defaultNull(),
  period: cy.shortText().defaultNull(),
  value: cy.number().defaultNull(),
  description: cy.shortText().defaultNull(),
};

const dbSchema = {
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};

export default cy.buildValidationSchema(requestSchema, dbSchema);
