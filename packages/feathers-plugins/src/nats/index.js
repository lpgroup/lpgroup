/* eslint-disable class-methods-use-this */
/* eslint-disable no-use-before-define */
import nats from "nats";
import { log } from "@lpgroup/utils";
import { checkExpected } from "@lpgroup/feathers-utils";

const { info, error } = log("nats");

export { natsYupError } from "./natsYupError.js";

/**
 * Start a nats server used by microservices
 *
 * "nats": {
 *   "name": "lpgroup-ng",
 *   "uri": "nats://localhost:4222",
 *   "queue": "file-ng-dev",
 *   "messagePrefix": "dev",
 *   "versions": {
 *     "post.document.transaction-forms": "v1",
 *     "post.document.transaction-forms.response": "v1",
 *   }
 * },
 */

export async function createNatsServer(options) {
  const { queue } = options;
  const nc = nats.connect({
    name: `${options.name} ${options.messagePrefix}`,
    uri: options.uri,
    reconnect: true,
    maxReconnectAttempts: -1,
    reconnectTimeWait: 2000,
    // TODO: ,json: true
  });

  await new Promise((connected) => {
    nc.on("connect", () => {
      // Do something with the connection
      info(`Connected to NATS server: ${options.uri} and queue ${options.queue}`);
      connected(nc);
    });

    nc.on("disconnect", () => {
      info(`disconnect ${options.uri} ${options.queue}`);
    });

    nc.on("reconnecting", () => {
      info(`reconnecting ${options.uri} ${options.queue}`);
    });

    nc.on("reconnect", () => {
      info(`reconnect ${options.uri} ${options.queue}`);
    });

    nc.on("close", () => {
      info(`close ${options.uri} ${options.queue}`);
    });

    nc.on("error", (err) => {
      error("error", err, options);
      // TODO: Maybe?
      // if (err.code === "CONN_ERR") {
      //   throw err;
      // }
    });

    nc.closePlugin = () => {
      nc.close();
    };

    // TODO:
    // eslint-disable-next-line no-unused-vars
    nc.waitOnServerReady = async (url) => {};

    const validate = async (request, service) => {
      if (service.options.schema?.request)
        return validateYup(request, service.options.schema.request);
      return request;
    };

    nc.sub = (subject, service, optionsOverride = {}) => {
      const subscribeSubject = nc.getSubscribeSubject(subject, optionsOverride);
      let serviceQueue = queue;
      if (service?.options?.serviceQueue) serviceQueue = `${queue}.${service.options.serviceQueue}`;
      info(`NATS Subscribe on ${subscribeSubject} with queue ${serviceQueue}`);

      nc.subscribe(subscribeSubject, { queue: serviceQueue }, (msg, reply) => {
        try {
          const request = JSON.parse(msg);
          info(`Incoming on "${subscribeSubject}" ${reply || ""}`);
          validate(request, service)
            .then((v) => {
              service.request(v, reply).catch((err) => {
                error(
                  "error 1: ",
                  subscribeSubject,
                  request,
                  JSON.stringify({ ...err, message: err?.message, stack: err?.stack }, null, 2),
                );
                service.error(request, err, reply);
              });
            })
            .catch((err) => {
              error("error 2: ", subscribeSubject, request, err);
              service.error(request, err, reply);
            });
        } catch (err) {
          error(`Incoming error on "${subscribeSubject}" ${reply || ""} ${err.message}`);
          service.error({}, err, reply);
        }
      });
    };

    nc.pub = async (subject, msg, optionsOverride = {}) => {
      const subscribeSubject = nc.getSubscribeSubject(subject, optionsOverride);

      let replyTo;
      if (optionsOverride.replyTo) {
        replyTo = optionsOverride.noReplyPrefix
          ? optionsOverride.replyTo
          : nc.getSubscribeSubject(optionsOverride.replyTo, optionsOverride);
      }

      if (replyTo) {
        info(`Publish on "${subscribeSubject}" with replyTo "${replyTo}"`);
      } else {
        info(`Publish on "${subscribeSubject}" with no reply`);
      }
      const request = JSON.stringify(msg);
      return new Promise((resolve) => {
        nc.publish(subscribeSubject, request, replyTo, resolve);
      });
    };

    nc.req = async (subject, msg = {}, optionsOverride = {}) => {
      const subscribeSubject = nc.getSubscribeSubject(subject, optionsOverride);
      info(`RequestOne on "${subscribeSubject}"`);
      const request = JSON.stringify(msg);
      const timeout = optionsOverride.timeout || options.timeout || 5000;

      return new Promise((resolve, reject) => {
        nc.requestOne(subscribeSubject, request, timeout, (responseMsg) => {
          info(`RequestOne - Incoming on "${subscribeSubject}"`);
          const response = typeof responseMsg === "string" ? JSON.parse(responseMsg) : responseMsg;

          if (response.code === 200 || response.status === "OK" || Array.isArray(response)) {
            resolve(response);
            return;
          }

          if (response.name === "NatsError" && response.code === nats.REQ_TIMEOUT) {
            reject(new Error("Didn't get a response (timeout)"));
            return;
          }

          reject(response);
        });
      });
    };

    nc.reqSub = (subject, msg, cb, optionsOverride = {}) => {
      const subscribeSubject = nc.getSubscribeSubject(subject, optionsOverride);
      info(`RequestOne on "${subscribeSubject}"`);
      const request = JSON.stringify(msg);
      const timeout = optionsOverride.timeout || options.timeout || 5000;

      nc.subscribe(`${subscribeSubject}.updated`, {}, (responseMsg) => {
        info(`RequestSubscribe updated - Incoming on "${subscribeSubject}"`);
        const response = typeof responseMsg === "string" ? JSON.parse(responseMsg) : responseMsg;
        cb(response);
      });

      nc.requestOne(subscribeSubject, request, timeout, (responseMsg) => {
        const response = typeof responseMsg === "string" ? JSON.parse(responseMsg) : responseMsg;
        if (response.code === 200 || response.status === "OK") {
          info(`RequestSubscribe - Incoming on "${subscribeSubject}"`);
          cb(response);
        }
      });
    };

    nc.broadcast = (subject, request, broadcastEmitter, optionsOverride = {}) => {
      const subscribeSubject = nc.getSubscribeSubject(subject, optionsOverride);
      const requestMsg = JSON.stringify(request);
      info(`Broadcast - Subscribe on "${subscribeSubject}.response"`);
      nc.subscribe(`${subscribeSubject}.response`, (responseMsg) => {
        if (requestMsg !== responseMsg) {
          info(`Broadcast - Incoming  on "${subscribeSubject}.response"`);
          const response = typeof responseMsg === "string" ? JSON.parse(responseMsg) : responseMsg;
          broadcastEmitter.emit("incoming", response);
        }
      });

      info(`Broadcast - Subscribe on "${subscribeSubject}"`);
      nc.subscribe(subscribeSubject, () => {
        info(`Broadcast - Incoming on "${subscribeSubject}"`);
        info(`Broadcast - Publish on "${subscribeSubject}.response"`);
        nc.publish(`${subscribeSubject}.response`, requestMsg);
      });

      info(`Broadcast - Publish on "${subscribeSubject}"`);
      nc.publish(subscribeSubject, "");
    };

    // Store subscribtion promises from subscribe()
    // incoming() can then wait for them to finish.
    nc.subscribes = {};

    nc.subExpect = (subject, configOptions = {}) => {
      const maxIncomeCounter = configOptions.incomeCounter || 0;
      const ignoreKeyCompare = configOptions.ignoreKeyCompare || options?.ignoreKeyCompare;
      const subscribeSubject = nc.getSubscribeSubject(subject);
      const promise = new Promise((resolve) => {
        let incomeCounter = 0;
        nc.subscribe(subscribeSubject, (msg, reply, _subject, sid) => {
          incomeCounter += 1;

          info(`Incoming on "${subscribeSubject}" ${reply || ""}`);
          const request = JSON.parse(msg);
          if (configOptions.callback) configOptions.callback(request);
          if (checkExpected(request, { ...configOptions, ignoreKeyCompare }, { method: "nats" })) {
            error(`Failed incoming: ${subscribeSubject}`);
            resolve({ sid, request });
          } else if (incomeCounter >= maxIncomeCounter) {
            resolve({ sid, request });
          }
        });
      });

      nc.subscribes[subscribeSubject] = nc.subscribes[subscribeSubject]
        ? nc.subscribes[subscribeSubject].push(promise)
        : [promise];
    };

    nc.waitForSubExpect = async (subject) => {
      const subscribeSubject = nc.getSubscribeSubject(subject);
      info(`Wait for incoming: ${subscribeSubject}`);
      if (nc.subscribes[subscribeSubject] && nc.subscribes[subscribeSubject].length > 0) {
        const res = Promise.all(nc.subscribes[subscribeSubject])
          .then(async (result) => {
            return Promise.all(
              result.map(async ({ sid, request }) => {
                info(`Unsubscribe ${subscribeSubject} ${sid}`);
                await nc.unsubscribe(sid);
                return request;
              }),
            );
          })
          .catch(() => {
            error(`Failed incoming: ${subscribeSubject}`);
          });

        delete nc.subscribes[subscribeSubject];
        return res;
      }
      error(`No subscription added for: ${subscribeSubject}`);
      return undefined;
    };

    /**
     * Merge prefix: "int", version: "v2"  and subject: "post.email.response"
     * to "int.v5.post.email.response"
     */
    nc.getSubscribeSubject = (subject, optionsOverride = {}) => {
      if (optionsOverride.absoluteSubject) return subject;

      // TODO: messagePrefix -> subjectPrefix
      const prefix =
        optionsOverride.messagePrefix !== undefined
          ? optionsOverride.messagePrefix
          : options.messagePrefix;
      const suffix =
        optionsOverride.subjectSuffix !== undefined
          ? optionsOverride.subjectSuffix
          : options.subjectSuffix;
      const version =
        optionsOverride.version !== undefined ? optionsOverride.version : options.version;

      let ret = subject;
      if (version) ret = `${version}.${ret}`;
      if (prefix) ret = `${prefix}.${ret}`;
      if (suffix) ret = `${ret}.${suffix}`;
      return ret;
    };
  });

  return nc;
}

function validateYup(data, validateSchema) {
  return validateSchema.validate(data, { strict: false, abortEarly: false, stripUnknown: false });
}

export async function initWithOptions(options) {
  return createNatsServer(options);
}

export class ServiceNats {
  constructor(options) {
    this.options = options;
  }

  async setup(app, subject) {
    info(`Setup is called on ${subject}`);
  }

  async teardown(app, subject) {
    info(`Teardown is called on ${subject}`);
  }

  // eslint-disable-next-line no-unused-vars
  async request(requests, replyTo) {
    info("Request is called");
  }
}
