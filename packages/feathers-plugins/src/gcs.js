/* eslint-disable no-use-before-define */
import { Storage } from "@google-cloud/storage";
import dauria from "dauria";
import { log } from "@lpgroup/utils";

// Setup Google Cloud Storage plugin
const { info, error } = log("gcs");

let storage = null;
let bucket = null;

export async function initWithOptions(options) {
  if (!storage) {
    await setupCloudStorage(
      options.imageGcsProject,
      options.imageGcsCredentials,
      options.imageGcsBucket,
    );
  }
  return {
    storage,
    bucket,
    closePlugin,
    exists,
    uploadFile,
    downloadFile,
    removeFile,
    listFiles,
  };
}

async function setupCloudStorage(projectId, keyFilename, bucketName) {
  storage = new Storage({ projectId, keyFilename });
  bucket = storage.bucket(bucketName);
  return verifyBucket(bucketName);
}

/**
 * Verify that bucket exist on google cloud storage.
 */
async function verifyBucket(bucketName) {
  return bucket
    .getFiles({ prefix: "don't like to find anything" })
    .then(() => {
      info(`Connected to GCP bucket: ${bucketName}`);
    })
    .catch((err) => {
      error(`Can't connect to gcp bucket: ${bucketName}`, err);
      throw Error(`Can't connect to gcp bucket: ${bucketName}`);
    });
}

async function closePlugin() {
  // TODO: Does this exist?
  // bucket.close();
  // storage.close();
}

async function exists(fileName) {
  const file = bucket.file(fileName);
  const result = await file.exists();
  return result[0];
}

async function uploadFile(fileName, uri, meta = {}) {
  info(`Upload to gcs ${fileName}`);
  const result = dauria.parseDataURI(uri);
  const file = bucket.file(fileName);
  await file.save(result.buffer, { resumable: false }).catch((err) => {
    error(`Cant save file to cloud storage: ${fileName}`, err);
    throw err;
  });
  await file
    .setMetadata({
      metadata: {
        ...meta,
        // Enable long-lived HTTP caching headers
        // Use only if the contents of the file will never change
        // (If the contents will change, use cacheControl: 'no-cache')
        cacheControl: "public, max-age=600",
      },
    })
    .catch((err) => {
      error(`Cant store metadata to cloud storage: ${fileName}`, err);
      throw err;
    });

  return {
    src: file.metadata.name,
    contentType: file.metadata.contentType,
    size: file.metadata.size,
  };
}

async function downloadFile(fileName) {
  const file = bucket.file(fileName);
  return file
    .download()
    .then((v) => {
      return v[0];
    })
    .catch((err) => {
      const message = `Can't download file from gcs: ${fileName} ${err.code} ${err.response.statusMessage}`;
      error(`GCS: ${message}`);
      throw Error(message);
    });
}

async function removeFile(fileName) {
  const [files] = await bucket.getFiles({ prefix: fileName });
  const result = files.map(async (file) => {
    await file.delete().catch((err) => {
      error(`Error deleting file from gcs: ${err}`);
    });
    return { src: file.metadata.name };
  });
  return Promise.all(result);
}

async function listFiles(prefix) {
  const [files] = await bucket.getFiles({ prefix });
  return files.map((x) => ({
    name: x.name,
    md5Checksum: x.metadata.metadata.gdriveMD5,
    mediaLink: x.metadata.mediaLink,
    size: x.metadata.size,
  }));
}
