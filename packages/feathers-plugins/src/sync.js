/* eslint-disable no-param-reassign */
/* eslint-disable no-use-before-define */
import { log } from "@lpgroup/utils";
import feathersSync from "feathers-sync";
import { onPluginReady } from "./utils.js";

const { info, debug } = log("sync");
const { core } = feathersSync;

export async function initWithOptions({ app }) {
  try {
    app.sync = await configureSync(app);
    return app.sync;
  } catch {
    console.error("Need app");
    throw Error("Need app");
  }
}

async function configureSync(app) {
  app.configure(core);

  // NOTE: Need to configure nats before configure sync
  const nc = await onPluginReady("nats");
  const sync = {
    type: "nats",
    ready: new Promise((resolve, reject) => {
      nc.on("connect", (c) => {
        resolve(c);
        info(`  Sync ready ${nc.messagePrefix}`);
      });

      nc.on("error", (err) => {
        reject(err);
      });
    }),
    serialize: (value) => {
      const { sessionId } = value.context.params;
      const { session } = value.context.params.mongodb;
      delete value.context.params.sessionId;
      delete value.context.params.mongodb.session;
      const result = JSON.stringify(value);
      if (sessionId) {
        value.context.params.sessionId = sessionId;
        value.context.params.mongodb.session = session;
      }
      return result;
    },
    deserialize: JSON.parse,
  };

  // Sent every time a service
  const subject = nc.getSubscribeSubject("sync");
  app.on("sync-out", (data) => {
    debug(`out ${subject}`, data);
    nc.publish(subject, data);
  });

  nc.subscribe(subject, (data) => {
    debug(`in ${subject}`, data);
    app.emit("sync-in", data);
  });

  return sync;
}
