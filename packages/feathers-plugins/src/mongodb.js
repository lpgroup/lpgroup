/* eslint-disable no-use-before-define */
import { log } from "@lpgroup/utils";
import { MongoClient } from "mongodb";
import { hooks } from "@lpgroup/feathers-mongodb-hooks";

const { info, error } = log("mongodb");

// System globals to access MongoDb
let client = null;
let database = null;

export async function initWithOptions(options) {
  if (!client) {
    await createMongoClient(options.uri);
  }
  return { client, database, closePlugin };
}

async function createMongoClient(uri) {
  const databaseName = uri.substr(uri.lastIndexOf("/") + 1);
  client = new MongoClient(uri, {});

  database = await client
    .connect()
    .then((c) => {
      info(`Connected to Mongo db: ${databaseName}`);
      const db = c.db(databaseName);
      // TODO Remove
      db.createIndexThrowError = createIndexThrowError;
      return db;
    })
    .catch((err) => {
      error("Mongo error: ", err);
    });
  return database;
}

async function closePlugin() {
  client.close();
}

/**
 * Regular mongodb createIndex with error catcher.
 */
async function createIndexThrowError(table, fieldOrSpec, options) {
  return this.collection(table)
    .createIndex(fieldOrSpec, options)
    .then((index) => {
      info(`Created index: ${table}.${index}`);
    })
    .catch((err) => {
      error("error", err.message);
      throw Error(`error${JSON.stringify(err.message, null, 2)}`);
    });
}

export function startSession() {
  return async (context) => {
    const startTrx = hooks.startSession({ client, database: await database });
    return startTrx(context);
  };
}

const { endSession, errorSession } = hooks;
export { endSession, errorSession };
