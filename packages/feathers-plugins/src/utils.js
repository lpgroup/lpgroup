import { log } from "@lpgroup/utils";
import { sleep } from "@lpgroup/feathers-utils";

const { info } = log("app");

const pluginByName = {};
const plugins = [];

export function addPluginWithOptions(name, plugin, options = {}) {
  pluginByName[name] = plugin.initWithOptions(options).then(async (v) => {
    if (options.readyServer) {
      await v.waitOnServerReady(options.readyServer);
    }
    return v;
  });

  plugins.push(pluginByName[name]);
}

export function closePlugins() {
  Object.values(pluginByName).forEach(async (plugin) => {
    const p = await plugin;
    info("Close down plugin");
    // It's not required to have a closePlugin function in a plugin
    if (p.closePlugin) p.closePlugin();
  });
}

export function hasPlugin(name) {
  return name in pluginByName;
}

export function checkHasPlugin(name) {
  if (!hasPlugin(name)) throw Error(`Plugin ${name} are not added`);
}

// wait - milliseconds to wait before checking if plugin exist
//        sometimes it's needed to wait for the app to get time to
//        add the plugin.
export async function onPluginReady(name, wait = 0) {
  if (wait !== 0) await sleep(wait);
  checkHasPlugin(name);
  return pluginByName[name];
}

export async function onPluginsReady() {
  return Promise.all(plugins);
}
