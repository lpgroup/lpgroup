/* eslint-disable no-use-before-define */
import { log } from "@lpgroup/utils";
import amqp from "amqplib";
import { sleep } from "@lpgroup/feathers-utils";

const { info, error } = log("rabbitmq");

// TODO:
// - Change to this wrapper, https://github.com/guidesmiths/rascal ??
// - Error handling, when loosing connection https://github.com/squaremo/amqp.node/issues/266

/**
 * Start a rabbitmq server used by microservices
 *
 * options {
 *   "uri": "amqps://user:password@wolf.rmq.cloudamqp.com/xxx",
 *   "queue": "dev"
 * }
 */

export async function initWithOptions(options) {
  return setupRabbitMQ(options);
}

export async function setupRabbitMQ(options) {
  const { conn, channel } = await getChannel(options);

  return {
    conn,
    channel,

    consume(queue, consumer) {
      info(`RabbitMQ Consume queue ${queue}`);
      channel
        .assertQueue(queue, { durable: true })
        .then(() => channel.prefetch(1))
        .then(
          () =>
            channel.consume(queue, (msg) => {
              const body = msg.content.toString();
              info(`  Incoming on ${queue} ${body}`);
              consumer
                .consume(body)
                .then(() => {
                  channel.ack(msg);
                })
                .catch(async (e) => {
                  error(e);
                  // Return to queue after 5 seconds and try again.
                  await sleep(5000);
                  channel.nack(msg);
                });
            }),
          { noAck: false, requeue: true },
        );
    },

    async closePlugin() {
      await channel.close();
      conn.legitClose = true;
      await conn.close();
    },

    async sentToQueue(queue, request) {
      return channel
        .assertQueue(queue, { durable: true })
        .then(() => {
          channel.sendToQueue(queue, Buffer.from(request), {
            persistent: true,
          });
          info(`Send to queue: ${queue}`);
        })
        .catch((err) => {
          error(`RabbitMq: ${err}`);
        });
    },
  };
}

let amqpConn = null;
let amqpReconnectCounter = 0;

async function start(options) {
  if (!options?.uri) return null;

  const uri = `${options.uri}?heartbeat=30`;
  amqpConn = await amqp
    .connect(uri)
    .then(async (v) => {
      info(`Connected to RabbitMQ server: ${options.uri} with queue ${options.queue}`);
      return v;
    })
    .catch((err) => {
      error(`Can't connect to RabbitMQ ${uri}\n`, err.message);
      throw new Error(`Can't connect to RabbitMQ ${uri}\n`, err.message);
    });

  amqpConn.on("error", (err) => {
    error("conn error", err.message);
    if (err.message !== "Connection closing") {
      error("Should we restart docker pod here?");
    }
  });

  amqpConn.on("close", () => {
    if (amqpConn.legitClose) {
      info(`Connection closed to RabbitMQ ${uri}\n`);
    } else {
      amqpReconnectCounter += 1;
      error(`conn reconnecting ${amqpReconnectCounter} times`);
      setTimeout(start, 1000, options);
    }
  });

  return amqpConn;
}

async function getChannel(options) {
  const uri = `${options.uri}?heartbeat=30`;
  const conn = await start(options);

  const channel = await conn.createChannel();
  channel.once("error", (err) => {
    error(`Can't connect to RabbitMQ ${uri}\n`, err.message);
    if (err.code !== 404) {
      throw err;
    }
  });
  channel.on("error", (err) => {
    error("channel error", err.message);
  });
  channel.on("close", () => {
    info("channel closed");
  });

  return { conn, channel };
}
