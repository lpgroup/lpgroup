import React, { ReactNode } from "react";
import { Popover, alpha, styled, PopoverProps } from "@mui/material";

interface MenuPopoverProps extends PopoverProps {
  children: ReactNode;
  sx?: React.CSSProperties;
  anchorOrigin?: { vertical: "top" | "center" | "bottom"; horizontal: "left" | "center" | "right" };
  transformOrigin?: {
    vertical: "top" | "center" | "bottom";
    horizontal: "left" | "center" | "right";
  };
}

const ArrowStyle = styled("span")(({ theme }) => ({
  [theme.breakpoints.up("sm")]: {
    top: -7,
    zIndex: 1,
    width: 12,
    right: 20,
    height: 12,
    content: "''",
    position: "absolute",
    borderRadius: "0 0 4px 0",
    transform: "rotate(-135deg)",
    background: theme.palette.background.paper,
    borderRight: `solid 1px ${alpha(theme.palette.grey[500], 0.12)}`,
    borderBottom: `solid 1px ${alpha(theme.palette.grey[500], 0.12)}`,
  },
}));

const MenuPopover: React.FC<MenuPopoverProps> = ({
  children,
  sx,
  anchorOrigin,
  transformOrigin,
  ...rest
}) => {
  return (
    <Popover
      anchorOrigin={anchorOrigin || { vertical: "bottom", horizontal: "right" }}
      transformOrigin={transformOrigin || { vertical: "top", horizontal: "right" }}
      PaperProps={{
        sx: {
          mt: 1.5,
          ml: 0.5,
          overflow: "inherit",
          overflowY: "scroll",
          ...sx,
        },
      }}
      {...rest}
    >
      <ArrowStyle className="arrow" />
      {children}
    </Popover>
  );
};

export default MenuPopover;
