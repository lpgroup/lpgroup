// @ts-nocheck

import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LabelList,
} from "recharts";
import { Spinner } from "./Progress";

const groupByPeriodAscending = (data) => {
  const groups = {};
  data.forEach((obj) => {
    const { period } = obj;
    if (!groups[period]) {
      groups[period] = [];
    }
    groups[period].push(obj);
  });
  const sortedKeys = Object.keys(groups).sort((a, b) => new Date(`${a}-01`) - new Date(`${b}-01`));
  const result = sortedKeys.map((period) => ({ period, data: groups[period] }));
  return result;
};

const getData = (arr) => {
  const transformed = [];
  arr.forEach((item) => {
    const newItem = { name: item.period };
    item.data.forEach((d) => {
      newItem[d.group] = d.value;
    });
    transformed.push(newItem);
  });
  return transformed;
};

const getUniquePropertyValues = (arr, prop) => Array.from(new Set(arr.map((item) => item[prop])));

const colors = [
  ["#00425A"],
  ["#00425A", "#1F8A70"],
  ["#3AB4F2", "#00425A", "#1F8A70"],
  ["#3AB4F2", "#00425A", "#FC7300", "#1F8A70"],
  ["#1B9C85", "#3AB4F2", "#00425A", "#FC7300", "#1F8A70"],
];

const getColor = (length, index) => {
  if (length <= colors.length) {
    return colors[length - 1][index];
  }

  return colors[colors.length - 1][index % colors.length];
};

const CounterStatistics = ({ counters }) => {
  const [data, setData] = useState(null);
  const [groups, setGroups] = useState(null);

  useEffect(() => {
    if (counters?.length) {
      const periods = groupByPeriodAscending(counters);
      const dataArr = getData(periods);
      setData(dataArr);

      const groupsArr = getUniquePropertyValues(counters, "group");
      setGroups(groupsArr);
    }
  }, [counters]);

  if (!counters) return <div>No Data</div>;
  if (!data) return <Spinner />;

  if (!data.length) {
    return <div>No Data</div>;
  }

  return (
    <ResponsiveContainer
      width="95%"
      height={data.length > 1 ? 50 * data.length * groups.length : 100}
      debounce={50}
    >
      <BarChart
        width={500}
        height={300}
        data={data.reverse()}
        layout="vertical"
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis type="number" />
        <YAxis type="category" dataKey="name" interval={0} />
        <Tooltip />
        <Legend />
        {groups.map((group, index) => {
          return (
            <Bar key={`${group}-${index}`} dataKey={group} fill={getColor(groups.length, index)}>
              <LabelList dataKey={group} position="right" style={{ fill: "black" }} />
            </Bar>
          );
        })}
      </BarChart>
    </ResponsiveContainer>
  );
};

export default CounterStatistics;
