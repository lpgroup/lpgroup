// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const SimpleButton = ({
  name = "",
  tooltipText = "",
  handleClick,
  startIcon,
  endIcon,
  size = "medium",
  color = "primary",
  variant = "outlined",
}) => {
  return (
    <Tooltip title={tooltipText}>
      <Button
        id={`${name.toLowerCase()}-button`}
        variant={variant}
        className="button-simple"
        onClick={handleClick}
        startIcon={startIcon}
        endIcon={endIcon}
        size={size}
        sx={{ fontSize: 12 }}
        color={color}
      >
        {name}
      </Button>
    </Tooltip>
  );
};

// SimpleButton.propTypes = {
//   name: PropTypes.string.isRequired,
//   tooltipText: PropTypes.string,
//   handleClick: PropTypes.func.isRequired,
//   startIcon: PropTypes.node,
//   endIcon: PropTypes.node,
//   size: PropTypes.oneOf(["small", "medium", "large"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
// };

export default SimpleButton;
