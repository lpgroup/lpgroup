// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";
import { KeyboardReturn as KeyboardReturnIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const ReturnButton = ({
  tooltipText = "",
  handleReturn,
  buttonText = "Return",
  variant = "outlined",
  color = "primary",
  size = "medium",
}) => {
  return (
    <Tooltip title={tooltipText}>
      <Button
        variant={variant}
        color={color}
        className={"button-return"}
        onClick={handleReturn}
        endIcon={<KeyboardReturnIcon fontSize="inherit" />}
        size={size}
        sx={{ fontSize: 12 }}
      >
        {buttonText}
      </Button>
    </Tooltip>
  );
};

// ReturnButton.propTypes = {
//   tooltipText: PropTypes.string,
//   handleReturn: PropTypes.func.isRequired,
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   size: PropTypes.oneOf(["small", "medium", "large"]),
// };

export default ReturnButton;
