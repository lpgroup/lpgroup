// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";
import { Delete as DeleteIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const DeleteButton = ({
  handleDelete,
  buttonText = "Delete",
  tooltipText = "delete this item",
  variant = "outlined",
  color = "primary",
  size = "medium",
  isDisabled = false,
}) => {
  return isDisabled ? (
    <Button
      id="admin-delete-button"
      variant={variant}
      color={color}
      className="button-delete"
      onClick={handleDelete}
      endIcon={<DeleteIcon fontSize="inherit" />}
      disabled
      size={size}
      sx={{ fontSize: 12 }}
    >
      {buttonText}
    </Button>
  ) : (
    <Tooltip title={tooltipText}>
      <Button
        id="admin-delete-button"
        variant={variant}
        color={color}
        className="button-delete"
        onClick={handleDelete}
        endIcon={<DeleteIcon fontSize="inherit" />}
        size={size}
        sx={{ fontSize: 12 }}
      >
        {buttonText}
      </Button>
    </Tooltip>
  );
};

// DeleteButton.propTypes = {
//   handleDelete: PropTypes.func.isRequired,
//   isDisabled: PropTypes.bool,
//   tooltipText: PropTypes.string,
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   size: PropTypes.oneOf(["small", "medium", "large"]),
// };

export default DeleteButton;
