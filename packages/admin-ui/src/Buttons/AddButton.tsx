// @ts-nocheck

import React from "react";
import { Button as MuiButton, Tooltip, styled, Theme } from "@mui/material";
import { Add as AddIcon } from "@mui/icons-material";

interface AddButtonProps {
  handleAdd: () => void;
  tooltipText?: string;
  text?: string;
  variant?: "text" | "outlined" | "contained";
  color?: "inherit" | "primary" | "secondary" | "warning" | "info";
  size?: "small" | "medium" | "large";
}

const Button = styled(MuiButton)(({ theme }: { theme: Theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const AddButton: React.FC<AddButtonProps> = ({
  handleAdd,
  tooltipText = "",
  buttonText = "ADD",
  variant = "outlined",
  color = "primary",
  size = "medium",
}) => {
  return (
    <Tooltip title={tooltipText}>
      <Button
        id="admin-add-button"
        variant={variant}
        color={color}
        className="button-add"
        endIcon={<AddIcon fontSize="inherit" />}
        onClick={handleAdd}
        size={size}
        sx={{ fontSize: 12 }}
      >
        {buttonText}
      </Button>
    </Tooltip>
  );
};

export default AddButton;
