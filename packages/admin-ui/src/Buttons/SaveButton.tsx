// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";
import { Save as SaveIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const SaveButton = ({
  handleSave,
  isDisabled = false,
  tooltipText = "save changes",
  buttonText = "Save",
  variant = "outlined",
  color = "primary",
  size = "medium",
}) => {
  return isDisabled ? (
    <Button
      variant={variant}
      className={"button-save"}
      onClick={handleSave}
      endIcon={<SaveIcon fontSize="inherit" />}
      disabled
      color={color}
      size={size}
      sx={{ fontSize: 12 }}
    >
      {buttonText}
    </Button>
  ) : (
    <Tooltip title={tooltipText}>
      <Button
        variant={variant}
        className={"button-save"}
        onClick={handleSave}
        endIcon={<SaveIcon fontSize="inherit" />}
        color={color}
        size={size}
        sx={{ fontSize: 12 }}
      >
        {buttonText}
      </Button>
    </Tooltip>
  );
};

// SaveButton.propTypes = {
//   handleSave: PropTypes.func.isRequired,
//   isDisabled: PropTypes.bool,
//   tooltipText: PropTypes.string,
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   size: PropTypes.oneOf(["small", "medium", "large"]),
// };

export default SaveButton;
