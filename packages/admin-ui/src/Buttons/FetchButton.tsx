// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";
import { ArrowDownward as ArrowDownwardIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const FetchButton = ({
  handleFetch,
  tooltipText = "fetch information from external systems",
  text = "FETCH",
  variant = "outlined",
  color = "primary",
  size = "medium",
}) => {
  return (
    <Tooltip title={tooltipText}>
      <Button
        variant={variant}
        color={color}
        className="button-fetch"
        endIcon={<ArrowDownwardIcon />}
        onClick={handleFetch}
        size={size}
        sx={{ fontSize: 12 }}
      >
        {text}
      </Button>
    </Tooltip>
  );
};

// FetchButton.propTypes = {
//   handleFetch: PropTypes.func.isRequired,
//   tooltipText: PropTypes.string,
//   text: PropTypes.string,
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   size: PropTypes.oneOf(["small", "medium", "large"]),
// };

export default FetchButton;
