// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import { Button as MuiButton, Tooltip, styled } from "@mui/material";
import { History as HistoryIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const HistoryButton = ({
  handleHistory,
  tooltipText = "view changes",
  variant = "outlined",
  color = "primary",
  size = "medium",
}) => {
  return (
    <Tooltip title={tooltipText}>
      <Button
        variant={variant}
        color={color}
        className={"button-history"}
        onClick={handleHistory}
        size={size}
        sx={{ fontSize: 12 }}
        endIcon={<HistoryIcon />}
      >
        History
      </Button>
    </Tooltip>
  );
};

// HistoryButton.propTypes = {
//   handleHistory: PropTypes.func.isRequired,
//   tooltipText: PropTypes.string,
//   variant: PropTypes.oneOf(["text", "outlined", "contained"]),
//   color: PropTypes.oneOf(["inherit", "primary", "secondary", "warning", "info"]),
//   size: PropTypes.oneOf(["small", "medium", "large"]),
// };

export default HistoryButton;
