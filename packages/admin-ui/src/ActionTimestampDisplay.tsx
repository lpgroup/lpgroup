import React from "react";
import { Box, Typography, BoxProps } from "@mui/material";

interface ActionTimeStampDisplayProps {
  added: {
    by: string;
    at: string;
  };
  changed: {
    by: string;
    at: string;
  };
}

const renderText = (action: string, { by, at }: { by: string; at: string }) => {
  return (
    <Typography variant="caption" color="gray">
      {`${action} by `}
      <span className="text-highlight">{by}</span> at <span className="text-highlight">{at}</span>
    </Typography>
  );
};

const ActionTimeStampDisplay: React.FC<ActionTimeStampDisplayProps & BoxProps> = ({
  added,
  changed,
  ...props
}) => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
      {...props}
    >
      {renderText("added", added)}
      {renderText("changed", changed)}
    </Box>
  );
};

export default ActionTimeStampDisplay;
