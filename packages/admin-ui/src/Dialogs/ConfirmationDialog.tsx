// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import {
  Button as MuiButton,
  Dialog,
  DialogContent,
  DialogActions,
  DialogContentText,
  styled,
} from "@mui/material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const ConfirmationDialog = ({
  dialogText = "Dialog",
  dialogIcon = "",
  isOpen,
  setOpen,
  isProcessing = false,
  handleOperation,
  confirmText = "Proceed",
  cancelText = "No",
  confirmTextColor = "warning",
  handleCancel = () => {},
}) => {
  const handleClose = () => {
    setOpen(false);
    handleCancel();
  };

  return (
    <Dialog
      open={isOpen}
      onClose={() => setOpen(false)}
      maxWidth="xs"
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent sx={{ textAlign: "center" }}>
        {dialogIcon}
        <DialogContentText style={{ textAlign: "center" }} id="alert-dialog-description">
          {dialogText}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          className="button-primary"
          onClick={handleOperation}
          disabled={isProcessing}
          color={confirmTextColor}
          size="small"
        >
          {confirmText}
        </Button>
        <Button
          variant="outlined"
          className="button-primary"
          autoFocus
          onClick={handleClose}
          size="small"
        >
          {cancelText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

// ConfirmationDialog.propTypes = {
//   dialogText: PropTypes.string.isRequired,
//   dialogIcon: PropTypes.node,
//   isOpen: PropTypes.bool.isRequired,
//   setOpen: PropTypes.func.isRequired,
//   isProcessing: PropTypes.bool.isRequired,
//   handleOperation: PropTypes.func.isRequired,
//   confirmText: PropTypes.string,
//   confirmTextColor: PropTypes.string,
//   handleCancel: PropTypes.func,
// };

export default ConfirmationDialog;
