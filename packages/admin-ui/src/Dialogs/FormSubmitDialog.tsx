// @ts-nocheck

import React from "react";
// import * as PropTypes from "prop-types";
import {
  DialogContentText,
  Button as MuiButton,
  Dialog,
  DialogTitle,
  DialogContent,
  Container,
  FormControl,
  IconButton,
  styled,
} from "@mui/material";
import { Close as CloseIcon, Save as SaveIcon } from "@mui/icons-material";

const Button = styled(MuiButton)(({ theme }) => ({
  color: theme.color,
  background: theme.background,
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, pt: 4, px: 6 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 34,
            top: 24,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

const FormSubmitDialog = ({
  title,
  submitButtonLabel,
  isOpen,
  children,
  handleClose,
  handleSubmit,
  errorAlert,
  successAlert,
  isDisabled,
  dialogText,
  isSaveButton,
  saveButtonText = "Save",
  color,
  isFullWidthButton,
}) => {
  return (
    <Dialog open={isOpen} onClose={handleClose} scroll="body" fullWidth>
      <form onSubmit={handleSubmit} noValidate>
        <BootstrapDialogTitle onClose={handleClose}>{title}</BootstrapDialogTitle>
        <DialogContent>
          <Container>
            <DialogContentText id="alert-dialog-description">{dialogText}</DialogContentText>
            {errorAlert}
            {successAlert}
            {children}
            <FormControl margin="normal" sx={{ display: "flex", alignItems: "flex-end" }}>
              {isSaveButton ? (
                <Button
                  disabled={isDisabled}
                  display="none"
                  className={`button-primary ${
                    submitButtonLabel || "submit".replace(/\s/g, "").toLowerCase()
                  }-button`}
                  style={{ marginBottom: "20px" }}
                  color={color}
                  endIcon={<SaveIcon />}
                  type="submit"
                  variant="contained"
                  fullWidth={isFullWidthButton}
                  size="small"
                >
                  {saveButtonText}
                </Button>
              ) : (
                <Button
                  disabled={isDisabled}
                  display="none"
                  className={`button-primary ${
                    submitButtonLabel || "submit".replace(/\s/g, "").toLowerCase()
                  }-button`}
                  style={{ marginBottom: "20px" }}
                  size="small"
                  type="submit"
                  color={color}
                  fullWidth={isFullWidthButton}
                  variant="contained"
                >
                  {submitButtonLabel || "submit"}
                </Button>
              )}
            </FormControl>
          </Container>
        </DialogContent>
      </form>
    </Dialog>
  );
};

// FormSubmitDialog.propTypes = {
//   title: PropTypes.string.isRequired,
//   submitButtonLabel: PropTypes.string,
//   isOpen: PropTypes.bool.isRequired,
//   children: PropTypes.node,
//   handleClose: PropTypes.func.isRequired,
//   handleSubmit: PropTypes.func.isRequired,
//   errorAlert: PropTypes.node,
//   successAlert: PropTypes.node,
//   isDisabled: PropTypes.bool,
//   dialogText: PropTypes.string,
//   isSaveButton: PropTypes.bool,
//   color: PropTypes.string,
//   isFullWidthButton: PropTypes.bool,
// };

export default FormSubmitDialog;
