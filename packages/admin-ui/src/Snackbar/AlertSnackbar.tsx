import React from "react";
import { Snackbar, Alert } from "@mui/material";

interface AlertSnackbarProps {
  message: string;
  severity: "success" | "error" | "warning";
  isOpen: boolean;
  handleClose: () => void;
}

const AlertSnackbar: React.FC<AlertSnackbarProps> = ({
  message,
  severity,
  isOpen,
  handleClose,
}) => (
  <Snackbar open={isOpen} autoHideDuration={6000} onClose={handleClose}>
    <Alert
      elevation={6}
      variant="filled"
      onClose={handleClose}
      severity={severity}
      sx={{ width: "100%" }}
    >
      {message}
    </Alert>
  </Snackbar>
);

export default AlertSnackbar;
