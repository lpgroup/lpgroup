import React from "react";
import { IconButton, IconButtonProps } from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";
import { useSnackbar } from "notistack";

interface SnackbarCloseButtonProps {
  snackbarKey: string | number;
  color?: "light" | "dark";
}

const SnackbarCloseButton: React.FC<SnackbarCloseButtonProps & IconButtonProps> = ({
  snackbarKey,
  color = "light",
}) => {
  const { closeSnackbar } = useSnackbar();

  const handleClose = () => {
    closeSnackbar(snackbarKey);
  };

  return (
    <IconButton onClick={handleClose}>
      <CloseIcon sx={{ color: color === "light" ? "#ffffff" : "#000000" }} />
    </IconButton>
  );
};

export default SnackbarCloseButton;
