import React from "react";
import { Button, ButtonProps } from "@mui/material";
import { Save as SaveIcon } from "@mui/icons-material";

interface SaveButtonProps extends ButtonProps {
  disabled?: boolean;
  buttonText?: string;
}

const SaveButton: React.FC<SaveButtonProps> = ({
  disabled = false,
  buttonText = "Save",
  ...field
}) => {
  return (
    <Button disabled={disabled} type="submit" endIcon={<SaveIcon fontSize="inherit" />} {...field}>
      {buttonText}
    </Button>
  );
};

export default SaveButton;
