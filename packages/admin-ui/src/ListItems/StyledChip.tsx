import React from "react";
import { Chip, ChipProps } from "@mui/material";

interface StyledChipProps extends ChipProps {
  label: string;
  handleClick: () => void;
  handleDelete: () => void;
}

const StyledChip: React.FC<StyledChipProps> = ({ label, handleClick, handleDelete, ...props }) => {
  return (
    <Chip
      sx={{ width: "100%" }}
      label={label}
      onDelete={handleDelete}
      onClick={handleClick}
      {...props}
    />
  );
};

export default StyledChip;
