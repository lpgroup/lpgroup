import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import SimpleButton from "../src/Buttons/SimpleButton.tsx";

describe("<SimpleButton />", () => {
  it("renders the button and calls handleClick on click", () => {
    const mockHandleClick = vi.fn();

    render(<SimpleButton name="Test Button" handleClick={mockHandleClick} />);

    const testButton = screen.getByText("Test Button");
    expect(testButton).toBeInTheDocument();

    fireEvent.click(testButton);
    expect(mockHandleClick).toHaveBeenCalledTimes(1);
  });

  it("renders the button with a tooltip", () => {
    render(<SimpleButton name="Test Button" handleClick={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });
});
