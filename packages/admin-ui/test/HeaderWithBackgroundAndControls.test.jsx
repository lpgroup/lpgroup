import React from "react";
import { render, screen } from "@testing-library/react";
import { HeaderWithBackgroundAndControls } from "../src/Header/index.ts";
import { getById } from "./utils";

describe("<HeaderWithBackgroundAndControls />", () => {
  const title = "Header With Background";

  it("renders header with title and controls", () => {
    const controls = <button data-testid="test-button">Click me</button>;
    const backgroundColor = "#abcdef";

    const { container } = render(
      <HeaderWithBackgroundAndControls
        title={title}
        controls={controls}
        backgroundColor={backgroundColor}
      />,
    );

    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByTestId("test-button")).toBeInTheDocument();
    const headerElement = getById(container, "header-with-background");
    expect(headerElement).toHaveStyle(`background-color: ${backgroundColor}`);
  });

  it("renders header without controls when controls prop is not provided", () => {
    render(<HeaderWithBackgroundAndControls title={title} />);
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.queryByTestId("test-button")).toBeNull();
  });
});
