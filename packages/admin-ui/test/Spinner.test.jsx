import React from "react";
import { render, screen } from "@testing-library/react";
import { Spinner } from "../src/Progress/index.ts";

describe("<Spinner />", () => {
  it("renders Spinner with default size", () => {
    render(<Spinner />);

    const spinner = screen.getByRole("progressbar");

    expect(spinner).toBeInTheDocument();
    expect(spinner).toHaveStyle({ width: "40px" });
  });

  it("renders Spinner with custom size", () => {
    const customSize = 60;
    render(<Spinner size={customSize} />);

    const spinner = screen.getByRole("progressbar");

    expect(spinner).toBeInTheDocument();
    expect(spinner).toHaveStyle({ width: `${customSize}px` });
  });
});
