import { screen, queryByAttribute } from "@testing-library/react";

export const getById = queryByAttribute.bind(null, "id");

export const findByTextWithRegExp = (regexp) => {
  return screen.getByText((content, node) => {
    const hasText = (text) => regexp.test(text);
    const nodeHasText = hasText(node.textContent);
    const childrenDontHaveText = Array.from(node.children).every(
      (child) => !hasText(child.textContent),
    );
    return nodeHasText && childrenDontHaveText;
  });
};
