import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { vi } from "vitest";
import { AlertSnackbar } from "../src/Snackbar/index.ts";

describe("<AlertSnackbar />", () => {
  const message = "Test Message";
  const severity = "success";
  const mockHandleClose = vi.fn();

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("renders AlertSnackbar with message", async () => {
    render(
      <AlertSnackbar message={message} severity={severity} isOpen handleClose={mockHandleClose} />,
    );

    const snackbar = screen.getByRole("alert");

    expect(snackbar).toBeInTheDocument();
    expect(screen.getByText(message)).toBeInTheDocument();
  });

  it("calls handleClose when Snackbar is closed manually", () => {
    render(
      <AlertSnackbar message={message} severity={severity} isOpen handleClose={mockHandleClose} />,
    );

    const closeIcon = screen.getByRole("button", { name: /close/i });

    fireEvent.click(closeIcon);

    expect(mockHandleClose).toHaveBeenCalled();
  });
});
