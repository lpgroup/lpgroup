import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { vi } from "vitest";
import ConfirmationDialog from "../src/Dialogs/ConfirmationDialog.tsx";

describe("<ConfirmationDialog />", () => {
  it("renders with default props", () => {
    render(<ConfirmationDialog isOpen={true} setOpen={() => {}} handleOperation={() => {}} />);

    expect(screen.getByText("Dialog")).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "Proceed" })).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "No" })).toBeInTheDocument();
  });

  it('calls handleOperation when "Proceed" button is clicked', () => {
    const mockHandleOperation = vi.fn();

    render(
      <ConfirmationDialog isOpen={true} setOpen={() => {}} handleOperation={mockHandleOperation} />,
    );

    fireEvent.click(screen.getByRole("button", { name: "Proceed" }));
    expect(mockHandleOperation).toHaveBeenCalled();
  });

  it('calls handleCancel when "No" button is clicked', () => {
    const mockHandleCancel = vi.fn();

    render(<ConfirmationDialog isOpen={true} setOpen={() => {}} handleCancel={mockHandleCancel} />);

    fireEvent.click(screen.getByRole("button", { name: "No" }));
    expect(mockHandleCancel).toHaveBeenCalled();
  });

  it("renders custom dialog text and icon", () => {
    const customText = "Custom Dialog Text";
    const customIcon = <div data-testid="custom-icon" />;

    render(
      <ConfirmationDialog
        isOpen={true}
        setOpen={() => {}}
        handleOperation={() => {}}
        dialogText={customText}
        dialogIcon={customIcon}
      />,
    );

    expect(screen.getByText(customText)).toBeInTheDocument();
    expect(screen.getByTestId("custom-icon")).toBeInTheDocument();
  });
});
