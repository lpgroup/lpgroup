import React from "react";
import { render, fireEvent, renderHook } from "@testing-library/react";
import { useForm } from "react-hook-form";
import ToggleSwitch from "../src/Form/ToggleSwitch.tsx";

describe("<ToggleSwitch />", () => {
  const { result } = renderHook(() => useForm());

  it("renders with default props", async () => {
    const { getByLabelText } = render(
      <ToggleSwitch
        control={result.current.control}
        name="toggle-switch"
        label="Test Toggle Switch"
      />,
    );

    const switchElement = getByLabelText("Test Toggle Switch");
    expect(switchElement).not.toBeChecked();

    fireEvent.click(switchElement);
    expect(switchElement).toBeChecked();
  });

  it("renders disabled toggle switch when disabled prop is true", async () => {
    const { getByLabelText } = render(
      <ToggleSwitch
        control={result.current.control}
        name="toggle-switch"
        label="Test Toggle Switch"
        disabled={true}
      />,
    );

    const switchElement = getByLabelText("Test Toggle Switch");
    expect(switchElement).toBeDisabled();
  });
});
