import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { vi } from "vitest";
import { SearchBar } from "../src/InputFields/index.ts";
import { getById } from "./utils";

describe("<SearchBar />", () => {
  const mockHandleSearch = vi.fn();

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("renders Search Bar", () => {
    const label = "Search Label";
    const size = "medium";
    const value = "Initial Value";

    const { container } = render(
      <SearchBar handleSearch={mockHandleSearch} size={size} label={label} value={value} />,
    );

    expect(screen.getByLabelText(label)).toBeInTheDocument();
    expect(screen.getByRole("textbox")).toHaveValue(value);
    expect(screen.queryByTestId("SearchIcon")).toBeInTheDocument();
    expect(getById(container, "search-input")).toBeInTheDocument();
  });

  it("renders SearchBar without icon when noIcon prop is true", () => {
    render(<SearchBar handleSearch={mockHandleSearch} noIcon />);

    expect(screen.queryByTestId("SearchIcon")).toBeNull();
  });

  it("calls handleSearch on input change", () => {
    const label = "Search Label";

    render(<SearchBar handleSearch={mockHandleSearch} label={label} />);

    const inputElement = screen.getByRole("textbox");

    fireEvent.change(inputElement, { target: { value: "New Value" } });

    expect(mockHandleSearch).toHaveBeenCalledWith(expect.anything());
  });
});
