import React from "react";
import { render, fireEvent } from "@testing-library/react";
import { vi } from "vitest";
import SaveButton from "../src/Form/SaveButton.tsx";

describe("<FormSaveButton />", () => {
  test("renders with default props", () => {
    const { getByText } = render(<SaveButton />);

    const saveButton = getByText("Save");
    expect(saveButton).toBeInTheDocument();
    expect(saveButton).not.toHaveAttribute("disabled");
  });

  test("renders with disabled prop", () => {
    const { getByText } = render(<SaveButton disabled />);

    const saveButton = getByText("Save");
    expect(saveButton).toBeInTheDocument();
    expect(saveButton).toHaveAttribute("disabled");
  });

  test("calls onClick callback when clicked", () => {
    const onClickMock = vi.fn();
    const { getByText } = render(<SaveButton onClick={onClickMock} />);

    const saveButton = getByText("Save");
    fireEvent.click(saveButton);
    expect(onClickMock).toHaveBeenCalled();
  });
});
