import React from "react";
import { render } from "@testing-library/react";
import EmptyState from "../src/UIStates/EmptyState.tsx";

describe("<EmptyState />", () => {
  it("renders with given props", () => {
    const title = "Empty State Title";
    const subtitle = "Empty State Subtitle";
    const imgSrc = "path/to/image.jpg";
    const ctaButton = <button>Call to Action</button>;

    const { getByText, getByAltText } = render(
      <EmptyState title={title} subtitle={subtitle} imgSrc={imgSrc} ctaButton={ctaButton} />,
    );

    expect(getByText(title)).toBeInTheDocument();
    expect(getByText(subtitle)).toBeInTheDocument();
    expect(getByText("Call to Action")).toBeInTheDocument();
    expect(getByAltText("Empty State")).toBeInTheDocument();
  });
});
