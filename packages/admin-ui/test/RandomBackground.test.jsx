import React from "react";
import { render, screen } from "@testing-library/react";
import { vi, done } from "vitest";
import { RandomBackground } from "../src/Backgrounds/index.ts";
import { getById } from "./utils";

vi.useFakeTimers();

describe("<RandomBackground />", () => {
  it("renders children after a delay", async () => {
    const { container } = render(
      <RandomBackground>
        <div data-testid="child-content">Child Content</div>
      </RandomBackground>,
    );

    expect(getById(container, "background-loading-indicator")).toBeInTheDocument();

    setTimeout(() => {
      const loadingIndicatorAfterLoad = getById(container, "background-loading-indicator");
      expect(loadingIndicatorAfterLoad).not.toBeInTheDocument();

      const sampleChildren = screen.getByText("Child Content");
      expect(sampleChildren).toBeInTheDocument();

      done();
    }, 1000);
  });
});
