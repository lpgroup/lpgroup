import React from "react";
import { vi } from "vitest";
import { render, screen, fireEvent } from "@testing-library/react";
import AddButton from "../src/Buttons/AddButton.tsx";

describe("<AddButton />", () => {
  const handleAddMock = vi.fn();

  it("renders the button with the provided text", () => {
    render(<AddButton handleAdd={handleAddMock} buttonText="Custom Add" />);
    const addButton = screen.getByText("Custom Add");
    expect(addButton).toBeInTheDocument();
  });

  it("calls handleAdd when the button is clicked", () => {
    render(<AddButton handleAdd={handleAddMock} />);
    const addButton = screen.getByText("ADD");
    fireEvent.click(addButton);
    expect(handleAddMock).toHaveBeenCalledTimes(1);
  });

  it("renders a tooltip with the provided tooltipText", () => {
    render(<AddButton handleAdd={handleAddMock} tooltipText="Custom Tooltip" />);
    const tooltip = screen.getByLabelText("Custom Tooltip");
    expect(tooltip).toBeInTheDocument();
  });
});
