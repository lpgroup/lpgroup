import React from "react";
import { render } from "@testing-library/react";
import Copyright from "../src/Copyright.tsx";

describe("<Copyright />", () => {
  test("renders with default props", () => {
    const siteName = "YourSite";

    const { getByText } = render(<Copyright siteName={siteName} />);

    const copyrightText = getByText(`Copyright © ${siteName} ${new Date().getFullYear()}`);
    expect(copyrightText).toBeInTheDocument();
  });

  test("renders with custom color", () => {
    const siteName = "YourSite";
    const color = "rgb(255, 0, 0)";

    const { getByText } = render(<Copyright siteName={siteName} color={color} />);

    const copyrightText = getByText(`Copyright © ${siteName} ${new Date().getFullYear()}`);
    expect(copyrightText).toHaveStyle(`color: ${color}`);
  });
});
