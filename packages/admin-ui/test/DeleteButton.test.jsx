import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import DeleteButton from "../src/Buttons/DeleteButton.tsx";

describe("<DeleteButton />", () => {
  it("renders the delete button and calls handleClick on click", () => {
    const mockHandleDelete = vi.fn();

    render(<DeleteButton handleDelete={mockHandleDelete} />);

    const testButton = screen.getByText("Delete");
    expect(testButton).toBeInTheDocument();

    fireEvent.click(testButton);
    expect(mockHandleDelete).toHaveBeenCalledTimes(1);
  });

  it("renders the button with a tooltip", () => {
    render(<DeleteButton handleDelete={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });

  it("renders disabled button when isDisabled is true", () => {
    render(<DeleteButton handleDelete={() => {}} isDisabled={true} />);

    const testButton = screen.getByText("Delete");
    expect(testButton).toBeDisabled();
  });
});
