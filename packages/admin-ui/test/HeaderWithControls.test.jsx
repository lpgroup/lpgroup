import React from "react";
import { render, screen } from "@testing-library/react";
import { HeaderWithControls } from "../src/Header/index.ts";

describe("<HeaderWithControls />", () => {
  const title = "Header With Controls";

  it("renders header with title and controls", () => {
    const controls = <button data-testid="test-button">Click me</button>;

    render(<HeaderWithControls title={title} controls={controls} />);

    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByTestId("test-button")).toBeInTheDocument();
  });

  it("renders header without controls when controls prop is not provided", () => {
    render(<HeaderWithControls title={title} />);

    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.queryByTestId("test-button")).toBeNull();
  });
});
