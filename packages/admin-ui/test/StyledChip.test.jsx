import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { vi } from "vitest";
import { StyledChip } from "../src/ListItems/index.ts";

describe("<StyledChip />", () => {
  const label = "Test Chip";
  const mockHandleClick = vi.fn();
  const mockHandleDelete = vi.fn();

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it("renders StyledChip with label", () => {
    render(
      <StyledChip label={label} handleClick={mockHandleClick} handleDelete={mockHandleDelete} />,
    );

    expect(screen.getByText(label)).toBeInTheDocument();
  });

  it("calls handleClick on chip click", () => {
    render(
      <StyledChip label={label} handleClick={mockHandleClick} handleDelete={mockHandleDelete} />,
    );

    const chip = screen.getByText(label);
    fireEvent.click(chip);
    expect(mockHandleClick).toHaveBeenCalled();
  });

  it("calls handleDelete on chip delete", () => {
    render(
      <StyledChip label={label} handleClick={mockHandleClick} handleDelete={mockHandleDelete} />,
    );

    const deleteIconButton = screen.getByTestId("CancelIcon");
    fireEvent.click(deleteIconButton);
    expect(mockHandleDelete).toHaveBeenCalled();
  });
});
