import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { SnackbarProvider, useSnackbar } from "notistack";
import { SnackbarCloseButton } from "../src/Snackbar/index.ts";

const Snackbar = () => {
  const { enqueueSnackbar } = useSnackbar();

  const handleSnackbar = () => {
    enqueueSnackbar("Test Message", { key: "testKey" });
  };

  return (
    <div>
      <button onClick={handleSnackbar}>Show Snackbar</button>
      <SnackbarCloseButton snackbarKey="testKey" />
    </div>
  );
};

describe("<SnackbarCloseButton />", () => {
  it("closes the snackbar when clicked", async () => {
    render(
      <SnackbarProvider>
        <Snackbar />
      </SnackbarProvider>,
    );

    const closeIconButton = screen.getByTestId("CloseIcon");
    expect(closeIconButton).toBeInTheDocument();

    fireEvent.click(screen.getByText("Show Snackbar"));

    await screen.findByText("Test Message", { exact: false, timeout: 500 });
  });
});
