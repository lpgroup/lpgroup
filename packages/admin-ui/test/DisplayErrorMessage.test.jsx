import React from "react";
import { render } from "@testing-library/react";
import DisplayErrorMessage from "../src/Errors/DisplayErrorMessage.tsx";

describe("<DisplayErrorMessage />", () => {
  it("renders with default props", () => {
    const { getByText } = render(<DisplayErrorMessage />);

    expect(getByText("404: the requested page or resource is not available.")).toBeInTheDocument();
  });

  it("renders error 405 with default message", () => {
    const customStatus = 405;

    const { getByText } = render(<DisplayErrorMessage status={customStatus} />);

    expect(getByText("Permission Denied")).toBeInTheDocument();
  });

  it("renders with custom props", () => {
    const customStatus = 500;
    const customMessage = "Some error";

    const { getByText } = render(
      <DisplayErrorMessage status={customStatus} statusText={customMessage} />,
    );

    expect(getByText(`${customStatus}: ${customMessage}`)).toBeInTheDocument();
  });
});
