import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import FetchButton from "../src/Buttons/FetchButton.tsx";

describe("<FetchButton />", () => {
  it("renders the button and calls handleFetch on click", () => {
    const mockHandleFetch = vi.fn();

    render(<FetchButton handleFetch={mockHandleFetch} />);

    const fetchButton = screen.getByText("FETCH");
    expect(fetchButton).toBeInTheDocument();

    fireEvent.click(fetchButton);
    expect(mockHandleFetch).toHaveBeenCalledTimes(1);
  });

  it("renders the button with a tooltip", () => {
    render(<FetchButton handleFetch={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });
});
