# @lpgroup/feathers-auth-service

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-auth-service.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-auth-service) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-auth-service/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-auth-service)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Services and hooks to handle auth, user and organisations in feathersjs.

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-auth-service
```

## Example

```js
const auth = require("@lpgroup/feathers-auth-service");
```

## API

### `xxx`

#### `xxx(xxx)`

```js
xxx(xxx);
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
