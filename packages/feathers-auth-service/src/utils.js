import { createHmac } from "node:crypto";

const cachedUsers = [];
export async function getCachedUser(app, email) {
  if (!cachedUsers[email]) {
    const userData = await app.service("users").find({ limit: 1, query: { email } });

    // eslint-disable-next-line prefer-destructuring
    cachedUsers[email] = userData.data[0];
  }
  return cachedUsers[email];
}

// renamed due to in conflict with hassPassword-hook upon search
export function digestPassword(text, hashingSecret) {
  if (!text) return null;
  const hash = createHmac("sha256", hashingSecret).update(text).digest("hex");
  return hash;
}
