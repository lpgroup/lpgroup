import * as cy from "@lpgroup/yup";

const requestSchema = {
  email: cy.email().required(),
  password: cy.password().required(),
  firstName: cy.labelText().defaultNull(),
  lastName: cy.labelText().defaultNull(),
  phone: cy.phone().defaultNull(),
};

const dbSchema = {};

export default cy.buildValidationSchema(requestSchema, dbSchema);
