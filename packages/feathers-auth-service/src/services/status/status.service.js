import { Status } from "./status.class.js";
import hooks from "./status.hooks.js";

export default (app) => {
  const options = {};
  app.use("/status", new Status(options, app));
  const service = app.service("status");
  service.hooks(hooks);
};
