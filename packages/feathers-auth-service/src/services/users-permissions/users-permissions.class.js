import { getPermissions } from "../../hooks/check-permissions.js";

export class UsersPermissions {
  constructor(options, app) {
    this.app = app;
  }

  // TODO: Will return permissions for other users. Should only do for the ones where we have access
  async find(params) {
    return getPermissions({ app: this.app, params });
  }
}
