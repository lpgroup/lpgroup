import { UsersPermissions } from "./users-permissions.class.js";
import hooks from "./users-permissions.hooks.js";

export default (app) => {
  const options = {
    id: "_id",
  };
  app.use("/users/:userId/permissions", new UsersPermissions(options, app));
  const service = app.service("users/:userId/permissions");
  service.hooks(hooks);
};
