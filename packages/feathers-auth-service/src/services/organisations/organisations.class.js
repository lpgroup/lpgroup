import { onPluginReady } from "@lpgroup/feathers-plugins";
import { internalParams, ServiceCache } from "@lpgroup/feathers-utils";

export class Organisations extends ServiceCache {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("organisations");
      database.createIndexThrowError("organisations", { alias: 1 }, { unique: true });
    });
  }

  async remove(id, params) {
    const organisation = await super.remove(id, params);

    // Cascade delete
    const grants = await this.app
      .service("organisations/:organisationAlias/grants")
      .find(
        internalParams({ ...params, superUser: true }, { organisationAlias: organisation.alias }),
      );

    // Run sequential, otherwise grants will access users collection
    // a bit random.
    await grants.data.reduce(async (previous, organisationGrant) => {
      await previous;
      return this.app
        .service("organisations/:organisationAlias/grants")
        .remove(
          organisationGrant.userId,
          internalParams({ ...params, superUser: true }, { organisationAlias: organisation.alias }),
        );
    }, Promise.resolve());

    return organisation;
  }
}
