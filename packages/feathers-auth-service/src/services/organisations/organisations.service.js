import { Organisations } from "./organisations.class.js";
import hooks from "./organisations.hooks.js";
import schema from "./organisations.yup.js";

export default (app, optionsOverride) => {
  const options = {
    id: "alias",
    paginate: app.get("paginate"),
    schema: optionsOverride?.schema ? schema.override(optionsOverride.schema) : schema,
    whitelist: ["$regex"],
    cache: {
      get: { max: 500 },
      find: { max: 10 },
    },
  };
  app.use("/organisations", new Organisations(options, app));
  const service = app.service("organisations");
  service.hooks(hooks);
};
