import * as cy from "@lpgroup/yup";

export const preValidate = cy.object({
  apiKey: cy.password(),
});

const requestSchema = {
  _id: cy.id(),
  name: cy.labelText().defaultNull(),
  apiKey: cy.longText().required(),
  expires: cy.number().defaultNull(),
  active: cy.boolean().default(true),
};

const dbSchema = {
  userId: cy.id(),
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};

export default cy.buildValidationSchema(requestSchema, dbSchema);
