import { discard, iff, isProvider } from "feathers-hooks-common";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import { validReq, validDB } from "@lpgroup/yup";
import { auth, changed, hashPassword } from "../../hooks/index.js";
import setUserId from "./hooks/set-userId-user.js";
import preValidate from "./hooks/preValidate.js";

export default {
  before: {
    all: [auth()],
    find: [],
    get: [],
    create: [
      preValidate(["apiKey"]),
      hashPassword("apiKey"),
      validReq(),
      setUserId(),
      changed(),
      validDB(),
    ],
    update: [
      preValidate(["apiKey"]),
      hashPassword("apiKey"),
      validReq(),
      loadData(),
      changed(),
      validDB(),
    ],
    patch: [
      preValidate(["apiKey"]),
      hashPassword("apiKey"),
      validReq(),
      patchData(),
      changed(),
      validDB(),
    ],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" }), iff(isProvider("external"), discard(["apiKey"]))],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
