import { OrganisationGrants } from "./organisations-grants.class.js";
import hooks from "./organisations-grants.hooks.js";
import schema from "./organisations-grants.yup.js";

export default (app) => {
  const options = { schema };
  app.use("/organisations/:organisationAlias/grants", new OrganisationGrants(options, app));
  const service = app.service("organisations/:organisationAlias/grants");
  service.hooks(hooks);
};
