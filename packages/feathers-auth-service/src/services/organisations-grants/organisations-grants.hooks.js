import { disallow, iff, isProvider } from "feathers-hooks-common";
import { hooks } from "@feathersjs/authentication";
import { validReq } from "@lpgroup/yup";
import { lockData } from "@lpgroup/feathers-mongodb-hooks/hooks";
// eslint-disable-next-line import/no-named-default
import { default as checkPermissions } from "../../hooks/check-permissions.js";

const { authenticate } = hooks;

export default {
  before: {
    all: [authenticate("jwt"), iff(isProvider("external"), checkPermissions())],
    find: [],
    get: [],
    create: [validReq()],
    update: [disallow()],
    patch: [disallow()],
    remove: [lockData({ collections: [{ collection: "users" }] })],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
