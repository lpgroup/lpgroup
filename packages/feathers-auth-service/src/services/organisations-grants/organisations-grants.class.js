import errors from "@feathersjs/errors";
import { v4 as uuidv4 } from "uuid";
import { internalParams } from "@lpgroup/feathers-utils";

// TODO: We are bypassing the yup schemas privilegie.
const PRIVILEGIE = "standard_organisation";

function userHasPrivilege(user, privilegeAlias, organisationAlias) {
  return user.privileges.find(
    (v) => v.privilegesAlias === privilegeAlias && v.params.organisationAlias === organisationAlias,
  );
}

const hasAuthPrivilege = (creatorUser, privilegeAlias, organisationAlias) => {
  return creatorUser.privileges.find(
    (v) =>
      (v.params.organisationAlias === organisationAlias && v.privilegesAlias === privilegeAlias) ||
      v.privilegesAlias === "super_user",
  );
};

function removePrivilege(user, privilegeAlias, organisationAlias) {
  return user.privileges
    .filter(
      (v) =>
        v.privilegesAlias === privilegeAlias && v.params.organisationAlias === organisationAlias,
    )
    .map((v) => ({ _id: v._id }));
}

export class OrganisationGrants {
  constructor(options, app) {
    this.options = options;
    this.app = app;
  }

  async find(params) {
    const { organisationAlias } = params.query;
    const usersResponse = await this.app.service("users").find(
      internalParams(
        { ...params, superUser: true },
        {
          "privileges.params.organisationAlias": organisationAlias,
        },
      ),
    );
    const grants = usersResponse.data.map((u) => {
      return {
        userId: u._id,
        firstName: u.firstName,
        lastName: u.lastName,
        privileges: u.privileges,
      };
    });
    return { ...usersResponse, data: grants };
  }

  async create(data, params) {
    const { organisationAlias } = params.query;
    const { userId, privilegie } = data;

    const user = await this.app
      .service("users")
      .get(userId, internalParams({ ...params, superUser: true }));
    if (userHasPrivilege(user, privilegie, organisationAlias))
      throw new errors.GeneralError(`E11000 duplicate key error: { privilegie: "${privilegie}"}`);

    const patchedUser = await this.app.service("users").patch(
      userId,
      {
        privileges: [
          { _id: uuidv4(), privilegesAlias: privilegie, params: { userId, organisationAlias } },
        ],
      },
      internalParams({ ...params, superUser: true }),
    );
    return {
      userId,
      firstName: patchedUser.firstName,
      lastName: patchedUser.lastName,
      privilegie,
    };
  }

  async get(id, params) {
    const { organisationAlias } = params.query;
    const user = await this.app
      .service("users")
      .get(
        id,
        internalParams(
          { ...params, superUser: true },
          { "privileges.params.organisationAlias": organisationAlias },
        ),
      );
    return {
      userId: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      privilegie: PRIVILEGIE,
    };
  }

  // eslint-disable-next-line consistent-return
  async remove(id, params) {
    const { organisationAlias } = params.query;
    if (!id) throw new errors.GeneralError(`User id cannot be ${id}`);

    if (!params.superUser) {
      if (params.user._id === id) {
        throw new errors.Forbidden("Removing own grant is not allowed.");
      }

      // Check permission on the loged in user
      if (!hasAuthPrivilege(params.user, PRIVILEGIE, organisationAlias)) {
        throw new errors.Forbidden(`Access denied to ${organisationAlias}`);
      }
    }

    // note: This function will also be executed when a user is removed
    //       and the cascade delete will remove all permissions on all organisations.
    //       and then the user will not exist at this state.
    try {
      const user = await this.app
        .service("users")
        .get(id, internalParams({ ...params, superUser: true }));

      const privileges = removePrivilege(user, "standard_organisation", organisationAlias);
      await this.app
        .service("users")
        .patch(id, { privileges }, internalParams({ ...params, superUser: true }));

      return {
        userId: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        privilegie: PRIVILEGIE,
      };
    } catch (err) {
      if (err.code !== 404) {
        throw err;
      }
    }
  }
}
