import { OrganisationsUsers } from "./organisations-users.class.js";
import hooks from "./organisations-users.hooks.js";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/organisations/:organisationAlias/users", new OrganisationsUsers(options, app));
  const service = app.service("organisations/:organisationAlias/users");
  service.hooks(hooks);
};
