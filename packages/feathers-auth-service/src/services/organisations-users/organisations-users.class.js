import { omit } from "lodash-es";
import { internalParams } from "@lpgroup/feathers-utils";

async function getUsers(app, params) {
  const users = await app
    .service("users")
    .find(
      internalParams(
        { ...params, superUser: true },
        { "privileges.params.organisationAlias": params.query.organisationAlias },
      ),
    );
  return users;
}

function filterUsers(users) {
  return users.data.map((v) =>
    omit(v, [
      "privileges",
      "userAgent",
      "ipNumber",
      "verified",
      "password",
      "signInEnabled",
      "signInCode",
    ]),
  );
}

export class OrganisationsUsers {
  constructor(options, app) {
    this.app = app;
  }

  async find(params) {
    const users = await getUsers(this.app, params);
    const filteredUsers = filterUsers(users);
    return filteredUsers;
  }
}
