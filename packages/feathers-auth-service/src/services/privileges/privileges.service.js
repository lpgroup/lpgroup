import { Privileges } from "./privileges.class.js";
import hooks from "./privileges.hooks.js";
import schema from "./privileges.yup.js";

export default (app) => {
  const options = {
    id: "alias",
    paginate: app.get("paginate"),
    schema,
    cache: {
      get: { max: 500 },
      find: { max: 10 },
    },
  };
  app.use("/privileges", new Privileges(options, app));
  const service = app.service("privileges");
  service.hooks(hooks);
};
