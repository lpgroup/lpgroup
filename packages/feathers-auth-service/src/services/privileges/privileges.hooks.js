import { iff, isProvider } from "feathers-hooks-common";
import { validReq, validDB } from "@lpgroup/yup";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import changed from "../../hooks/changed.js";
import auth from "../../hooks/auth.js";

export default {
  before: {
    all: [iff(isProvider("external"), auth())],
    find: [],
    get: [],
    create: [validReq(), changed(), validDB()],
    update: [validReq(), loadData(), changed(), validDB()],
    patch: [validReq(), patchData(), changed(), validDB()],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
