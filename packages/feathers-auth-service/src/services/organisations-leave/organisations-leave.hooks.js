import { disableSync } from "@lpgroup/feathers-utils/hooks";
import { disallow } from "feathers-hooks-common";

import auth from "../../hooks/auth.js";
import emitLeave from "./hooks/emit-leave.js";

export default {
  before: {
    all: [auth()],
    find: [disallow()],
    get: [disallow()],
    create: [],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [disableSync()],
    find: [],
    get: [],
    create: [emitLeave()],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
