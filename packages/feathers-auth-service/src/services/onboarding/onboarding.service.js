import { Onboarding } from "./onboarding.class.js";
import hooks from "./onboarding.hooks.js";

export default (app) => {
  app.use("/onboarding", new Onboarding());
  const service = app.service("onboarding");
  service.hooks(hooks);
};
