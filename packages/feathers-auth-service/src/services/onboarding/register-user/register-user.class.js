import { internalParams } from "@lpgroup/feathers-utils";

export class OnboardingRegisterUser {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async create(data, params) {
    const userData = {
      ...data,
      type: "user",
      privileges: [{ privilegesAlias: "standard_user", params: { userId: data._id } }],
    };
    return this.app
      .service("users")
      .create(userData, internalParams({ ...params, superUser: true }));
  }
}
