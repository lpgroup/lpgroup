import { ServiceProvider, IdentityProvider } from "saml2-js";
import { Parser } from "xml2js";
import fs from "node:fs";
import { log } from "@lpgroup/utils";

const { info } = log("saml");

// Function to read and parse SAML metadata XML
function parseSamlMetadata(filePath) {
  const xmlData = fs.readFileSync(filePath, "utf-8");

  const parser = new Parser({ explicitArray: false });
  let result;

  parser.parseString(xmlData, (err, parsedData) => {
    if (err) {
      throw new Error("Error parsing XML");
    }
    result = parsedData;
  });

  return result;
}

// Extract parameters for saml2-js IdentityProvider from parsed XML
function getIdpConfig(metadata) {
  const entityDescriptor = metadata["md:EntityDescriptor"] || metadata.EntityDescriptor;
  const idpSSODescriptor =
    entityDescriptor["md:IDPSSODescriptor"] || entityDescriptor.IDPSSODescriptor;

  // Extract entity ID
  // const { entityID } = entityDescriptor.$;

  // Extract certificates

  // Handle KeyDescriptor, which could be an array or a single object
  let keyDescriptors = idpSSODescriptor["md:KeyDescriptor"] || idpSSODescriptor.KeyDescriptor;
  if (!Array.isArray(keyDescriptors)) {
    keyDescriptors = [keyDescriptors]; // Convert to array if it's a single object
  }

  const certificates = keyDescriptors.map((descriptor) => {
    return (
      descriptor["ds:KeyInfo"]?.["ds:X509Data"]?.["ds:X509Certificate"] ||
      descriptor.KeyInfo?.X509Data?.X509Certificate
    )?.trim();
  });

  // Extract Single Sign-On Service URLs
  const ssoServices =
    idpSSODescriptor["md:SingleSignOnService"] || idpSSODescriptor.SingleSignOnService;
  let ssoRedirectUrl;
  let ssoPostUrl;
  if (ssoServices) {
    ssoRedirectUrl = ssoServices
      .find((service) => service.$.Binding.includes("HTTP-Redirect"))
      .$.Location.trim();
    ssoPostUrl = ssoServices
      .find((service) => service.$.Binding.includes("HTTP-POST"))
      .$.Location.trim();
  }

  return {
    sso_login_url: ssoRedirectUrl, // Use the HTTP-Redirect URL for login
    sso_logout_url: ssoPostUrl, // Use the HTTP-POST URL for logout
    certificates,
  };
}

const samlConfigs = {};

const createSamlConfig = (config) => {
  if (!config || !config.sp || !config.providers) {
    info("No SAML configuration found in authentication configuration. Skipping SAML setup.");
    return undefined;
  }

  const idps = {};
  const sps = {};
  if (config.providers) {
    Object.keys(config.providers).forEach((ipdAlias) => {
      const metadata = parseSamlMetadata(config.providers[ipdAlias]);
      const ipdConfig = getIdpConfig(metadata);
      if (!ipdConfig?.certificates?.length > 0) {
        throw new Error(
          "One or more trusted certificate paths must be provided in the SAML IdP config. Skipping SAML setup.",
        );
      }

      info("IDP", ipdConfig);
      const idp = new IdentityProvider(ipdConfig);
      idps[ipdAlias] = {
        ...ipdConfig,
        idp,
      };

      const spConfig = {
        ...config.sp,
        assert_endpoint:
          ipdAlias === "default"
            ? config.sp.assert_endpoint
            : `${config.sp.assert_endpoint}?idpAlias=${ipdAlias}`,
        private_key: config.sp.private_key
          ? fs.readFileSync(config.sp.private_key).toString()
          : null,
        certificate: config.sp.certificate
          ? fs.readFileSync(config.sp.certificate).toString()
          : null,
      };

      info("SP", spConfig);
      const sp = new ServiceProvider(spConfig);
      sps[ipdAlias] = {
        ...config.sp,
        sp,
      };
    });
  }

  return {
    ...config,
    sps,
    idps,
  };
};

// Setup function to configure the app with SAML
export const setupSaml = (options) => (app) => {
  const service = app.defaultAuthentication ? app.defaultAuthentication(options.authService) : null;

  if (!service) {
    throw new Error("An authentication service must exist before registering feathers-saml");
  }

  const allConfig = app.get("saml");
  if (allConfig) {
    Object.entries(allConfig.organisations).forEach(([organisationAlias, config]) => {
      samlConfigs[organisationAlias] = createSamlConfig(config);
      info(`${organisationAlias} configuration loaded`);
    });
  }

  app.set(`${options.authService}Saml`, samlConfigs);
};
