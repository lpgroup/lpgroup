import express from "@feathersjs/express";
import { log } from "@lpgroup/utils";
import errors from "@feathersjs/errors";
const { info, error } = log("saml");

const getSamlConfig = (samlConfigs, idpAlias, organisationAlias) => {
  if (!samlConfigs[organisationAlias])
    throw errors.BadRequest(`No configauration for ${organisationAlias}`);

  const config = samlConfigs[organisationAlias];

  const idp = config?.idps[idpAlias]?.idp || config?.idps?.default?.idp;
  const sp = config?.sps[idpAlias]?.sp || config?.sps?.default?.sp;

  return { ...config, sp, idp };
};

export const setupExpress = (options) => {
  return (app) => {
    const { authService } = options;
    const samlConfigs = app.get(`${authService}Saml`);
    if (!samlConfigs) {
      info("No SAML configuration found, skipping Express SAML setup");
      return;
    }
    const authApp = express();

    authApp.get("/:organisationAlias", (req, res) => {
      const { idpAlias } = req.query;
      const { sp, idp, loginRequestOptions } = getSamlConfig(
        samlConfigs,
        idpAlias,
        req.params.organisationAlias,
      );

      // Login to SAML IdP, ie google login page
      sp.create_login_request_url(idp, loginRequestOptions || {}, (err, loginUrl) => {
        if (err) {
          error("IDP missing login/post url");
          return res.sendStatus(500);
        }
        return res.redirect(loginUrl);
      });
    });

    authApp.get("/:organisationAlias/metadata.xml", (req, res) => {
      const { idpAlias } = req.query;
      const { sp } = getSamlConfig(samlConfigs, idpAlias, req.params.organisationAlias);
      res.type("application/xml");
      res.send(sp.create_metadata());
    });

    authApp.post("/:organisationAlias/assert", async (req, res, next) => {
      const { idpAlias } = req.query;
      const { organisationAlias } = req.params;
      const config = getSamlConfig(samlConfigs, idpAlias, organisationAlias);
      const { sp, idp } = config;
      const service = app.defaultAuthentication(authService);
      const [strategy] = service.getStrategies("saml");
      const params = { authStrategies: [strategy.name], organisationAlias, config };

      info("assert", req.params, req.query, req.body);

      const sendResponse = async (data) => {
        try {
          const redirect = await strategy.getRedirect(data, params);

          if (redirect) {
            info("sendResponse redirect", redirect);
            res.redirect(redirect);
          } else if (data instanceof Error) {
            info("sendResponse error", data);
            throw data;
          } else {
            info("sendResponse json", data);
            res.json(data);
          }
        } catch (err) {
          info("sendResponse err", err);
          error("SAML error", data?.email, err);
          next(err);
        }
      };

      try {
        const samlResponse = await new Promise((resolve, reject) => {
          const loginResponseOptions = {
            ...config.loginResponseOptions,
            request_body: req.body,
          };

          info("post_assert", req.params, loginResponseOptions);

          sp.post_assert(idp, loginResponseOptions, (err, samlResult) => {
            if (err) {
              info("post_assert error", err);
              reject(err);
            } else {
              info("post_assert result", samlResult);
              resolve(samlResult);
            }
          });
        });

        const authentication = { strategy: strategy.name, ...samlResponse };

        params.payload = {
          nameId: samlResponse?.user?.name_id || null,
          sessionIndex: samlResponse?.user?.session_index || null,
          samlToken: true,
        };

        info("SAML succesful login", params.payload.nameId);

        if (config.samlTokenExpiry) {
          params.jwtOptions = { expiresIn: config.samlTokenExpiry };
        }

        const authResult = await service.create(authentication, params);
        info("SAML successful authentication", params.payload.nameId);
        await sendResponse(authResult);
      } catch (err) {
        error("SAML authentication error", params?.payload?.nameId, err);
        await sendResponse(err);
      }
    });

    authApp.get("/:organisationAlias/logout", (req, res, next) => {
      const { idpAlias } = req.query;
      const { sp, idp, logoutRequestOptions } = getSamlConfig(
        samlConfigs,
        idpAlias,
        req.params.organisationAlias,
      );
      const { nameId, sessionIndex } = req.query;

      if (!nameId || !sessionIndex) {
        return next(
          new errors.BadRequest("`nameId` and `sessionIndex` must be set in query params"),
        );
      }

      logoutRequestOptions.name_id =
        typeof logoutRequestOptions.name_id === "object" ? { "#text": nameId } : nameId;

      logoutRequestOptions.session_index = sessionIndex;

      sp.create_logout_request_url(idp, logoutRequestOptions, (err, logoutUrl) => {
        if (err) {
          next(err);
        } else {
          res.redirect(logoutUrl);
        }
      });

      return undefined;
    });

    authApp.get("/:organisationAlias/slo", (req, res, next) => {
      const { idpAlias } = req.query;
      const { sp, idp, logoutResponseOptions } = getSamlConfig(
        samlConfigs,
        idpAlias,
        req.params.organisationAlias,
      );

      sp.create_logout_response_url(idp, logoutResponseOptions || {}, (err, requestUrl) => {
        if (err) {
          next(err);
        } else {
          res.redirect(requestUrl);
        }
      });
    });

    app.use("/saml", authApp);
  };
};
