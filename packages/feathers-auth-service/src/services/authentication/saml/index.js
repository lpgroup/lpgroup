import { setupExpress } from "./setup-express.js";
import { setupSaml } from "./setup-saml.js";

// Express setup function with optional settings
export const expressSaml =
  (options = {}) =>
  (app) => {
    app.configure(setupSaml(options));
    app.configure(setupExpress(options));
  };
