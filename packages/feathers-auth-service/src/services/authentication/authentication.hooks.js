import { disableSync } from "@lpgroup/feathers-utils/hooks";
import { validReq } from "@lpgroup/yup";
import { disallow, discard } from "feathers-hooks-common";
import clearProvider from "../../hooks/clear-provider.js";
import schema from "./authentication.yup.js";
import setUserData from "./hooks/setUserData.js";

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [clearProvider(), validReq({ schema })],
    update: [disallow()],
    patch: [disallow()],
    remove: [],
  },

  after: {
    all: [
      disableSync(),
      setUserData(),
      discard(
        "authentication",
        "user.password",
        "user.signInEnabled",
        "user.signInCode",
        "user.active",
        "user.added",
        "user.changed",
        "user.owner",
        "user.oAuthStrategy",
        "user.url",
        "user.myLock",
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
