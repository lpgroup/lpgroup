import { AuthenticationService } from "@feathersjs/authentication";
import { expressOauth } from "@feathersjs/authentication-oauth";

import { internalParams } from "@lpgroup/feathers-utils";
import { LocalStrategy } from "./strategies/local.js";
import { DeviceStrategy } from "./strategies/device.js";
import { ApiKeyStrategy } from "./strategies/api-key.js";
import { GoogleStrategy } from "./strategies/oauth-google.js";
import { FacebookStrategy } from "./strategies/oauth-facebook.js";
import { GithubStrategy } from "./strategies/oauth-github.js";
import { SamlStrategy } from "./strategies/saml.js";
import { JWTStrategy } from "./strategies/jwt.js";

import { expressSaml } from "./saml/index.js";
import hooks from "./authentication.hooks.js";

export default (app) => {
  const authentication = new AuthenticationService(app);

  authentication.register("jwt", new JWTStrategy());
  authentication.register("local", new LocalStrategy());
  authentication.register("device", new DeviceStrategy());
  authentication.register("apiKey", new ApiKeyStrategy());
  authentication.register("google", new GoogleStrategy());
  authentication.register("facebook", new FacebookStrategy());
  authentication.register("github", new GithubStrategy());
  authentication.register("saml", new SamlStrategy());

  app.use("/authentication", authentication);
  app.configure(expressOauth());
  app.configure(expressSaml());

  // Get our initialized service so that we can register hooks
  const service = app.service("authentication");
  service.hooks(hooks);
  app.on("disconnect", async (connection) => {
    if (connection?.user?._id) {
      await app
        .service("users")
        .patch(connection?.user?._id, { isOnline: false }, internalParams({ superUser: true }));
    }
  });
};
