import errors from "@feathersjs/errors";
import { AuthenticationBaseStrategy } from "@feathersjs/authentication";
import { internalParams } from "@lpgroup/feathers-utils";

const { NotAuthenticated } = errors;

export class ApiKeyStrategy extends AuthenticationBaseStrategy {
  get configuration() {
    const authConfig = this.authentication.configuration;
    const config = super.configuration || {};
    return {
      service: authConfig.service,
      entity: authConfig.entity,
      errorMessage: "Invalid login",
      ...config,
    };
  }

  async findEntity(userId) {
    const { entityService } = this;
    const { errorMessage } = this.configuration;
    try {
      const result = await entityService.get(userId, internalParams({ superUser: true }));
      return result;
    } catch {
      throw new errors.NotAuthenticated(errorMessage);
    }
  }

  async authenticate(data) {
    const { entity } = this.configuration;
    const { token } = data;
    const keyService = await this.app
      .service("/users/:userId/api-keys")
      .find(internalParams({ superUser: true }, { apiKey: token }));

    // or timeServer
    const currentTimeStamp = new Date().getTime();

    if (!keyService?.data[0]?.apiKey) throw new NotAuthenticated("Incorrect API Key");
    if (keyService?.data[0]?.active === false) throw new NotAuthenticated("Inactive API Key");
    if (keyService?.data[0]?.expires < currentTimeStamp)
      throw new NotAuthenticated("Expired API Key");

    const result = await this.findEntity(keyService?.data[0]?.userId);

    return {
      authentication: { strategy: this.name },
      [entity]: result,
      apiKey: true,
    };
  }
}
