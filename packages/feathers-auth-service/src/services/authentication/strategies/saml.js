/* eslint-disable class-methods-use-this */
import querystring from "node:querystring"; // @ts-ignore removed
import { AuthenticationBaseStrategy } from "@feathersjs/authentication";
import { log } from "@lpgroup/utils";
import { v4 as uuidv4 } from "uuid";
import { pick } from "lodash-es";
import errors from "@feathersjs/errors";
const { info } = log("saml");

export class SamlStrategy extends AuthenticationBaseStrategy {
  name = "SamlStrategy";

  get configuration() {
    const authConfig = this.authentication
      ? {
          service: this.authentication.configuration.service,
          entity: this.authentication.configuration.entity,
          entityId: this.authentication.configuration.entityId,
        }
      : {};
    const config = super.configuration || {};

    return {
      ...authConfig,
      ...config,
    };
  }

  get entityId() {
    const { entityService } = this;

    return this.configuration.entityId || entityService?.id;
  }

  /**
   * Retreive user based on email returned by IdP
   */
  async getEntityQuery(samlUser) {
    return {
      email: samlUser.attributes.email?.[0]?.toLowerCase(),
    };
  }

  /**
   * Create/update local user and set their email and fullName as returned by the IdP
   */
  async getEntityData(samlUser, _existingEntity, params) {
    const { attributes } = samlUser;

    // The idp sends attributs in different cases.
    const samlAttributes = Object.keys(attributes).reduce((acc, key) => {
      acc[key.toLowerCase()] = attributes[key];
      return acc;
    }, {});

    if (!samlAttributes.email || !samlAttributes.firstname || !samlAttributes.lastname) {
      throw new errors.BadRequest(
        "You need to map email, firstName, lastName attribute on the Idp",
        samlUser,
      );
    }

    const { organisationAlias } = params;
    let userId;
    const privileges = [];

    if (_existingEntity) {
      userId = _existingEntity._id;
      // remove all other priviliges this user have only use the last saml login.
      privileges.push(..._existingEntity.privileges.map((v) => ({ _id: v._id })));
    } else {
      userId = uuidv4();
    }

    privileges.push({ _id: uuidv4(), privilegesAlias: "standard_user", params: { userId } });
    privileges.push({
      _id: uuidv4(),
      privilegesAlias: "standard_organisation",
      params: { userId, organisationAlias },
    });

    return {
      _id: userId,
      type: "saml",
      privileges,
      email: samlAttributes.email?.[0]?.toLowerCase(),
      firstName: samlAttributes.firstname?.[0],
      lastName: samlAttributes.lastname?.[0],
      strategy: {
        saml: {
          nameId: params.payload.nameId,
          sessionIndex: params.payload.sessionIndex,
        },
      },
    };
  }

  async getSamlUser(data) {
    return data.user;
  }

  async getCurrentEntity(params) {
    const { authentication } = params;
    const { entity } = this.configuration;

    if (authentication?.strategy) {
      info("getCurrentEntity with authentication", authentication);

      const { strategy } = authentication;
      const authResult = await authentication.authenticate(authentication, params, strategy);

      return authResult[entity];
    }

    return null;
  }

  async getRedirect(data, params) {
    if (!params.config.redirect) {
      return null;
    }

    const { redirect } = params.config;

    // eslint-disable-next-line no-nested-ternary
    const separator = redirect.endsWith("?") ? "" : redirect.indexOf("#") !== -1 ? "?" : "#";

    const authResult = data;
    const query = authResult.accessToken
      ? {
          access_token: authResult.accessToken,
        }
      : {
          error: data.message || "SAML Authentication not successful",
        };

    return redirect + separator + querystring.stringify(query);
  }

  async findEntity(samlUser, params) {
    const query = await this.getEntityQuery(samlUser, params);
    const result = await this.entityService.find({
      ...params,
      query,
    });
    const [entity = null] = result.data ? result.data : result;

    info("findEntity returning", query, entity?._id);
    return entity;
  }

  async createEntity(samlUser, params) {
    const data = await this.getEntityData(samlUser, null, params);

    const result = await this.entityService.create(data, params);
    info(`createEntity ${result._id} with email ${data?.email}`);
    return result;
  }

  async updateEntity(entity, samlUser, params) {
    const id = entity[this.entityId];
    const data = await this.getEntityData(samlUser, entity, params);

    info(`updateEntity ${id} with email ${data?.email}`);

    return this.entityService.patch(id, data, params);
  }

  async authenticate(data, params) {
    const superParams = { ...params, superUser: true };

    const { entity } = this.configuration;
    const samlUser = await this.getSamlUser(data, superParams);
    const existingEntity =
      (await this.findEntity(samlUser, superParams)) || (await this.getCurrentEntity(superParams));

    const authEntity = !existingEntity
      ? await this.createEntity(samlUser, superParams)
      : await this.updateEntity(existingEntity, samlUser, superParams);

    info("authenticated", pick(authEntity, ["_id", "privileges", "strategy"]));

    return {
      authentication: { strategy: this.name },
      [entity]: authEntity,
    };
  }
}
