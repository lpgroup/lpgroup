/* eslint-disable class-methods-use-this */
import { OAuthStrategy } from "@feathersjs/authentication-oauth";
import errors from "@feathersjs/errors";
import axios from "axios";
import { createAsNewUser } from "./utils.js";

async function getTokens({ code, key, secret, redirectUri }) {
  const url = "https://graph.facebook.com/v14.0/oauth/access_token";
  const values = {
    code,
    client_id: key,
    client_secret: secret,
    redirect_uri: redirectUri,
    grant_type: "authorization_code",
  };
  const qs = new URLSearchParams(values);
  return axios
    .get(`${url}?${qs.toString()}`)
    .then((res) => res.data)
    .catch(() => {
      throw new errors.GeneralError("Facebook failed to get token");
    });
}

async function getFacebookUser(facebookToken) {
  const values = {
    access_token: facebookToken.access_token,
    fields: "id,name,email,first_name,last_name,picture",
  };
  const qs = new URLSearchParams(values);
  return axios
    .get(`https://graph.facebook.com/me?${qs.toString()}`)
    .then((r) => r.data)
    .catch(() => {
      throw new errors.GeneralError("Facebook failed to get user");
    });
}
export async function getUserWithFacebook(app, params) {
  const domain = `${params.protocol}://${params.headers.host}`;
  const oauthConfig = app.get("authentication").oauth.facebook;
  const { key, redirectRelativePath, secret } = oauthConfig;
  const { code } = params.query;
  const redirectUri = `${domain}/${redirectRelativePath}`;
  const fbToken = await getTokens({ code, key, secret, redirectUri });
  const facebookUser = await getFacebookUser(fbToken);
  const users = await app.service("users").find({ query: { email: facebookUser.email } });
  let user = users.data[0];
  if (!user) {
    const data = {
      app,
      email: facebookUser.email,
      firstName: facebookUser.first_name,
      lastName: facebookUser.last_name,
      profileImageUrl: facebookUser?.picture?.data?.url || null,
      params,
      oAuthStrategy: "facebook",
    };
    user = await createAsNewUser(data);
  }
  return user;
}

export class FacebookStrategy extends OAuthStrategy {
  async getProfile(data, params) {
    try {
      const profile = await getUserWithFacebook(this.app, params);
      if (profile?.oAuthStrategy !== "facebook") {
        throw new errors.GeneralError(
          `This profile can only login using oauth strategy: ${profile?.oAuthStrategy}.`,
        );
      }
      return profile;
    } catch (err) {
      throw new errors.GeneralError(err.message);
    }
  }

  async updateEntity(entity, profile) {
    return profile;
  }
}
