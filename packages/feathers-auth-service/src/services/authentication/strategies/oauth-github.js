/* eslint-disable class-methods-use-this */
import { OAuthStrategy } from "@feathersjs/authentication-oauth";
import errors from "@feathersjs/errors";
import axios from "axios";
import { createAsNewUser } from "./utils.js";

async function getTokens({ code, key, secret, redirectUri }) {
  const url = "https://github.com/login/oauth/access_token";
  const values = {
    code,
    client_id: key,
    client_secret: secret,
    redirect_uri: redirectUri,
  };
  const qs = new URLSearchParams(values);
  return axios
    .post(url, qs.toString(), {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
      },
    })
    .then((res) => res.data)
    .catch(() => {
      throw new errors.GeneralError("Github failed to fetch token");
    });
}

async function getGithubUser(githubToken) {
  return axios
    .get("https://api.github.com/user", {
      headers: { Authorization: `Bearer ${githubToken.access_token}` },
    })
    .then((res) => res.data)
    .catch((err) => {
      throw new errors.GeneralError(`Github failed to fetch user: ${err.message}`);
    });
}

async function getGethubUserEmailOnly(githubToken) {
  return axios
    .get("https://api.github.com/user/emails", {
      headers: { Authorization: `Bearer ${githubToken.access_token}` },
    })
    .then((res) => res.data[res.data.length - 1].email)
    .catch((err) => {
      throw new errors.GeneralError(`Github failed to fetch userEmail: ${err.message}`);
    });
}

export async function getUserWithGithub(app, params) {
  const domain = `${params.protocol}://${params.headers.host}`;
  const oauthConfig = app.get("authentication").oauth.github;
  const { key, redirectRelativePath, secret } = oauthConfig;
  const { code } = params.query;
  const redirectUri = `${domain}/${redirectRelativePath}`;
  const githubToken = await getTokens({ code, key, secret, redirectUri });
  const githubUser = await getGithubUser(githubToken);
  if (!githubUser.email) {
    githubUser.email = await getGethubUserEmailOnly(githubToken);
  }
  const users = await app.service("users").find({ query: { email: githubUser.email } });
  let user = users.data[0];
  if (!user) {
    const data = {
      app,
      email: githubUser.email,
      firstName: githubUser.name,
      lastName: "Of github",
      profileImageUrl: githubUser.avatar_url || null,
      params,
      oAuthStrategy: "github",
    };
    user = await createAsNewUser(data);
  }
  return user;
}

export class GithubStrategy extends OAuthStrategy {
  async getProfile(data, params) {
    try {
      const profile = await getUserWithGithub(this.app, params);
      if (profile?.oAuthStrategy !== "github") {
        throw new errors.GeneralError(
          `This profile can only login using oauth strategy: ${profile?.oAuthStrategy}.`,
        );
      }
      return profile;
    } catch (err) {
      throw new errors.GeneralError(err.message);
    }
  }

  async updateEntity(entity, profile) {
    return profile;
  }
}
