import { LocalStrategy as FELocalStrategy } from "@feathersjs/authentication-local";

export class LocalStrategy extends FELocalStrategy {
  // eslint-disable-next-line class-methods-use-this, no-unused-vars
  async getEntityQuery(query, params) {
    // Query for user but only include users marked as `active`
    return {
      ...query,
      active: true,
      $limit: 1,
    };
  }
}
