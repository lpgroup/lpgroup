import { v4 as uuidv4 } from "uuid";
import { internalParams } from "@lpgroup/feathers-utils";

export async function createAsNewUser({
  app,
  email,
  firstName,
  lastName,
  profileImageUrl,
  oAuthStrategy,
}) {
  const userId = uuidv4();
  const userData = {
    _id: userId,
    email,
    firstName,
    lastName,
    verified: true,
    profileImageUrl,
    oAuthStrategy,
    type: "user",
    privileges: [{ privilegesAlias: "standard_user", params: { userId } }],
  };
  return app.service("users").create(userData, internalParams({ superUser: true }));
}
