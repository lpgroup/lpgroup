/* eslint-disable class-methods-use-this */
import { OAuthStrategy } from "@feathersjs/authentication-oauth";
import errors from "@feathersjs/errors";
import axios from "axios";
import { createAsNewUser } from "./utils.js";

async function getTokens({ code, key, secret, redirectUri }) {
  const url = "https://oauth2.googleapis.com/token";
  const values = {
    code,
    client_id: key,
    client_secret: secret,
    redirect_uri: redirectUri,
    grant_type: "authorization_code",
  };
  const qs = new URLSearchParams(values);
  return axios
    .post(url, qs.toString(), {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
    })
    .then((res) => res.data)
    .catch(() => {
      throw new errors.GeneralError("Google failed to get token");
    });
}

async function getGoogleUser(googleToken) {
  return axios
    .get(
      `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${googleToken.access_token}`,
      { headers: { Authorization: `Bearer ${googleToken.id_token}` } },
    )
    .then((res) => res.data)
    .catch(() => {
      throw new errors.GeneralError("Google failed to get user");
    });
}

export async function getUserWithGoogle(app, params) {
  const domain = `${params.protocol}://${params.headers.host}`;
  const oauthConfig = app.get("authentication").oauth.google;
  const { key, redirectRelativePath, secret } = oauthConfig;
  const { code } = params.query;
  const redirectUri = `${domain}/${redirectRelativePath}`;
  const googleToken = await getTokens({ code, key, secret, redirectUri });
  const googleUser = await getGoogleUser(googleToken);
  if (!googleUser.verified_email) {
    throw new errors.GeneralError("Google account is not verified");
  }
  const users = await app.service("users").find({ query: { email: googleUser.email } });
  let user = users.data[0];
  if (!user) {
    const data = {
      app,
      email: googleUser.email,
      firstName: googleUser.given_name,
      lastName: googleUser.family_name,
      profileImageUrl: googleUser.picture || null,
      params,
      oAuthStrategy: "google",
    };
    user = await createAsNewUser(data);
  }
  return user;
}

export class GoogleStrategy extends OAuthStrategy {
  async getProfile(data, params) {
    try {
      const profile = await getUserWithGoogle(this.app, params);
      if (profile?.oAuthStrategy !== "google") {
        throw new errors.GeneralError(
          `This profile can only login using oauth strategy: ${profile?.oAuthStrategy}.`,
        );
      }
      return profile;
    } catch (err) {
      throw new errors.GeneralError(err.message);
    }
  }

  async updateEntity(entity, profile) {
    return profile;
  }
}
