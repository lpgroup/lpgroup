import { JWTStrategy as FeathersJWTStrategy } from "@feathersjs/authentication";

export class JWTStrategy extends FeathersJWTStrategy {
  async authenticate(authentication, params) {
    const res = await super.authenticate(authentication, params);

    // If the token was generated using the SAML strategy, drop it from
    // the response, causing Feathers to generate a new one automatically.
    // The Feathers Client library will pick up this new token on its own
    // and use it for future requests.
    if (res.authentication.payload.samlToken === true) {
      delete res.accessToken;
    }

    return res;
  }
}
