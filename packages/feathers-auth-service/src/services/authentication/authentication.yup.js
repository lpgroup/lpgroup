import * as cy from "@lpgroup/yup";

const requestSchema = {
  strategy: cy
    .labelText()
    .matches(/(jwt|local|device|google|facebook|github|saml)/)
    .lowercase()
    .default("local"),
  accessToken: cy.mediumText(),
  _id: cy.uuid().defaultNull(),
  email: cy.email().defaultNull(),
  password: cy.labelText().defaultNull(),
  // saml
  type: cy.string(),
  response_header: cy.meta(),
  user: cy.meta(),
};
export default cy.buildValidationSchema(requestSchema);
