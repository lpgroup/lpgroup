/* eslint-disable no-unneeded-ternary */
import { internalParams } from "@lpgroup/feathers-utils";

export default () => {
  return async (context) => {
    const { app, params, result, method } = context;
    const userAtt = {};
    if (params.connection) {
      userAtt.isOnline = method !== "remove";
    }

    const response = await app.service("users").patch(
      result.user._id,
      {
        ipNumber: params.remoteAddress,
        userAgent: params.headers?.["user-agent"],
        ...userAtt,
      },
      internalParams({ ...params, superUser: true }),
    );
    context.result.user = { ...context.result.user, ...response };
    return context;
  };
};
