import { v4 as uuidv4 } from "uuid";

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    const { data } = context;
    if (data.type === "user") {
      data.signInCode = uuidv4();
      data.signInEnabled = true;
    } else {
      data.signInCode = null;
      data.signInEnabled = false;
    }

    return context;
  };
};
