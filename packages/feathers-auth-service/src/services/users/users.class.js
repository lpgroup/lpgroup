import { ServiceCache, internalParams } from "@lpgroup/feathers-utils";
import { onPluginReady } from "@lpgroup/feathers-plugins";

async function removeApiKey(userId, params, app) {
  const keyService = await app
    .service("users/:userId/api-keys")
    .find(internalParams({ superUser: true }, { userId }));
  const allKeys = keyService.data;
  await Promise.all(
    allKeys.map(async (k) =>
      app
        .service("users/:userId/api-keys")
        .remove(k._id, internalParams({ ...params, superUser: true })),
    ),
  );
}

export class Users extends ServiceCache {
  constructor(options, app) {
    super(options);
    this.app = app;
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("users");
      database.createIndexThrowError("users", { email: 1 }, { unique: true });
    });
  }

  async remove(userId, params) {
    const user = await super.remove(userId, params);
    await removeApiKey(userId, params, this.app);
    return user;
  }
}
