import * as cy from "@lpgroup/yup";

export const preValidate = cy.object({
  password: cy.password().defaultNull(),
});

// TODO: Måste validera olika beroende på type.
const requestSchema = {
  _id: cy.id(),
  email: cy.email().defaultNull(),
  password: cy.string().defaultNull(),
  firstName: cy.labelText().defaultNull(),
  lastName: cy.labelText().defaultNull(),
  phone: cy.phone().defaultNull(),
  verified: cy.boolean().default(false),
  ipNumber: cy.labelText().defaultNull(),
  userAgent: cy.mediumText().defaultNull(),
  isOnline: cy.boolean().default(false),
  oAuthStrategy: cy.labelText().defaultNull(),
  strategy: cy.object({
    saml: cy.object({
      nameId: cy.string().defaultNull(),
      sessionIndex: cy.string().defaultNull(),
      sessionId: cy.string().defaultNull(),
    }),
  }),
  onlineStatus: cy
    .labelText()
    .matches(/(ONLINE|OFFLINE|DO-NOT-DISTURB)/)
    .default("ONLINE"),
  profileImageUrl: cy.url().defaultNull(),

  // Not allowed to change this from external
  privileges: cy.arrayObject({
    _id: cy.id(),
    privilegesAlias: cy.alias(),
    params: cy.object({
      userId: cy.uuid().defaultNull(),
      organisationAlias: cy.uuid().defaultNull(),
    }),
  }),
  type: cy
    .labelText()
    .matches(/(user|device|saml)/)
    .default("device"),
};

const dbSchema = {
  active: cy.boolean().default(true),
  signInCode: cy.mediumText().defaultNull(),
  signInEnabled: cy.boolean().default(false),
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};

export default cy.buildValidationSchema(requestSchema, dbSchema);
