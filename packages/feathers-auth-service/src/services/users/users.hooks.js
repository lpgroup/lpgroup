/* eslint-disable import/no-named-default */
import { log } from "@lpgroup/utils";
import { discard, iff, isProvider } from "feathers-hooks-common";
import authHooks from "@feathersjs/authentication";
import authLocalHooks from "@feathersjs/authentication-local";
import { validReq, validDB } from "@lpgroup/yup";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import changed from "../../hooks/changed.js";
import { default as checkPermissions } from "../../hooks/check-permissions.js";
import owner from "./hooks/set-owner-user.js";
import signIn from "./hooks/set-signin.js";
import allowApiKey from "../../hooks/allow-apiKey.js";
import { disallowWhenNoPrivileges } from "../../hooks/index.js";
import preValidate from "./hooks/preValidate.js";

const { debug } = log("auth");

const { authenticate } = authHooks;
const hash = authLocalHooks.hooks.hashPassword;

const debugLoginError = (context) => {
  debug(`Invalid user/JWT ${context.id} ${context.error.message}`);
};

export default {
  before: {
    // Don't check permissions for internal calls.
    all: [
      allowApiKey(),
      authenticate("jwt", "apiKey"),
      iff(isProvider("external"), checkPermissions()),
    ],
    find: [],
    get: [],
    create: [
      iff(isProvider("external"), disallowWhenNoPrivileges(["privileges"])),
      preValidate(["password"]),
      hash("password"),
      validReq(),
      owner(),
      signIn(),
      changed(),
      validDB(),
    ],
    update: [
      iff(isProvider("external"), disallowWhenNoPrivileges(["privileges"])),
      preValidate(["password"]),
      hash("password"),
      validReq(),
      loadData(),
      owner(),
      signIn(),
      changed(),
      validDB(),
    ],
    patch: [
      iff(isProvider("external"), disallowWhenNoPrivileges(["privileges"])),
      preValidate(["password"]),
      hash("password"),
      validReq(),
      patchData(),
      owner(),
      signIn(),
      changed(),
      validDB(),
    ],
    remove: [],
  },

  after: {
    // Make sure the password field is never sent to the client
    // Always must be the last hook
    all: [
      url({ key: "_id" }),
      iff(isProvider("external"), discard("password", "privileges", "signInCode", "signInEnabled")),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [debugLoginError],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
