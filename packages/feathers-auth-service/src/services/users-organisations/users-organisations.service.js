import { UsersOrganisations } from "./users-organisations.class.js";
import hooks from "./users-organisations.hooks.js";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/users/:userId/organisations", new UsersOrganisations(options, app));
  const service = app.service("users/:userId/organisations");
  service.hooks(hooks);
};
