import { pick } from "lodash-es";
import { internalParams } from "@lpgroup/feathers-utils";

async function getPrivileges(app, params) {
  const users = await app.service("users").get(params.query.userId, internalParams(params));
  return users.privileges;
}

function filterOrganisationAlias(privileges) {
  return privileges.map((v) => v.params.organisationAlias).filter((v) => v);
}

async function getOrganisations(aliases, app, params) {
  const organisations = await app
    .service("organisations")
    .find(internalParams(params, { alias: { $in: aliases } }));
  return organisations.data.map((v) => pick(v, ["_id", "alias", "name", "title"]));
}

export class UsersOrganisations {
  constructor(options, app) {
    this.app = app;
  }

  async find(params) {
    const privileges = await getPrivileges(this.app, params);
    const aliases = filterOrganisationAlias(privileges);
    return getOrganisations(aliases, this.app, params);
  }
}
