export function getFacebookOauthUrl(app, params) {
  const facebookConfig = app.get("authentication").oauth.facebook;
  const { key, redirectRelativePath } = facebookConfig;
  const domain = `${params.protocol}://${params.headers.host}`;
  const rootUrl = "https://www.facebook.com/v14.0/dialog/oauth";
  const options = {
    redirect_uri: `${domain}/${redirectRelativePath}`,
    client_id: key,
    response_type: "code",
    scope: "email,public_profile",
  };
  const qs = new URLSearchParams(options);
  return `${rootUrl}?${qs.toString()}`;
}

export async function oAuthWithFacebook(app, params, responseRedirect) {
  try {
    const auth = await app.service("authentication").create({ strategy: "facebook" }, params);
    return responseRedirect({ isError: false, accessToken: auth.accessToken });
  } catch (err) {
    return responseRedirect({ isError: true, message: err.message });
  }
}
