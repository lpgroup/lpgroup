import { OAuth } from "./oauth.class.js";
import hooks from "./oauth.hooks.js";

export default (app) => {
  const options = {};
  app.use("/auth/:provider", new OAuth(options, app), (req, res, next) => {
    if (res.data.url) return res.redirect(301, res.data.url);
    return next();
  });
  const service = app.service("auth/:provider");
  service.hooks(hooks);
};
