/**
 * Disallow keys in data request when user doesn't have certain privileges.
 *
 * For example a request posts a key called meta, that should only
 * be change internally or by super-user.
 *
 */
import { disallow } from "feathers-hooks-common";

export default (restrictedFields = [], requiredPrivileges = []) => {
  const allRequiredPrivileges = [...requiredPrivileges, "super_user"];
  const disallowHook = disallow();

  return async (context) => {
    const { data, params } = context;
    if (data) {
      const isRestrictedFields = Object.keys(data).some((k) => restrictedFields.includes(k));
      const isAllowed = params?.user?.privileges?.some((p) =>
        allRequiredPrivileges.includes(p.privilegesAlias),
      );
      if (isRestrictedFields && !isAllowed) return disallowHook(context);
    }
    return context;
  };
};
