/**
 * Discard keys in result when user doesn't have certain privileges.
 *
 * For example a get request returns a key called meta, that should only
 * be visible to users with standard_organisation privileges.
 *
 */
import { discard } from "feathers-hooks-common";

export default (discardField = [], requiredPrivileges = []) => {
  const allRequiredPrivileges = [...requiredPrivileges, "super_user"];
  const discardHook = discard(...discardField);

  return async (context) => {
    const { params } = context;
    const isAllowed =
      params?.superUser ||
      params?.user?.privileges?.some((p) => allRequiredPrivileges.includes(p.privilegesAlias));
    if (isAllowed) return context;
    return discardHook(context);
  };
};
