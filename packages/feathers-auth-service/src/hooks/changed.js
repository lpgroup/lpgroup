/* eslint-disable no-param-reassign */
import { buildItemHook } from "@lpgroup/feathers-utils";

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return buildItemHook((context, item) => {
    const superUser = context.params.superUser ? { _id: "superuser" } : undefined;

    const user = superUser || item.user || context.params.user;
    if (user) {
      const userAndDate = { by: user._id, at: Date.now() };
      item.changed = userAndDate;

      // Only set added when creating an item
      if (!item.added) item.added = userAndDate;

      // Owner can be modified by other service hooks.
      // They are responsible for not overwriting the user._id
      if (!("owner" in item)) item.owner = {};
      if (user.organisationAlias) item.owner.organisation = { alias: user.organisationAlias };
      if (!("user" in item.owner)) item.owner.user = { _id: user._id };
    }
  });
};
