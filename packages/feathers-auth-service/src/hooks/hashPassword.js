/* eslint-disable no-param-reassign */
import { buildItemHook } from "@lpgroup/feathers-utils";
import { digestPassword } from "../utils.js";

export default (key) => {
  return buildItemHook((context, item) => {
    const enigma = context.app.settings.authentication.apiKeySecret;
    if (item[key]) item[key] = digestPassword(item[key], enigma);
  });
};
