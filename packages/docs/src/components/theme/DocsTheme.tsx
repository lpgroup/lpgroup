import * as React from "react";
import { useState, useEffect } from "react";
import { CssBaseline, Paper, ThemeProvider, createTheme } from "@mui/material";
import { getTheme } from "./getTheme";

const useDarkMode = () => {
  const [themeMode, setTheme] = useState("light");
  const [mountedComponent, setMountedComponent] = useState(false);

  const setMode = (mode: string) => {
    try {
      window.localStorage.setItem("themeMode", mode);
    } catch {
      /* do nothing */
    }

    setTheme(mode);
  };

  const themeToggler = () => {
    themeMode === "light" ? setMode("dark") : setMode("light");
  };

  useEffect(() => {
    try {
      const localTheme = window.localStorage.getItem("themeMode");
      localTheme ? setTheme(localTheme) : setMode("light");
    } catch {
      setMode("light");
    }

    setMountedComponent(true);
  }, []);

  return [themeMode, themeToggler, mountedComponent];
};
type TextAlign = "center" | "end" | "justify" | "left" | "match-parent" | "right" | "start";

type TypographyOptions = import("@mui/material/styles/createTypography").TypographyOptions;

interface ThemeProps {
  children: any;
  typography?: TypographyOptions;
  font?: string;
  textAlign?: TextAlign | undefined;
}

export const DocsTheme = ({ children, typography, font, textAlign }: ThemeProps) => {
  const [themeMode, themeToggler, mountedComponent] = useDarkMode();

  const theme = createTheme(getTheme(themeMode, themeToggler));
  React.useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles);
    }
  }, []);

  useEffect(() => {
    document.querySelectorAll(".logo").forEach((elems) => {
      if (elems instanceof HTMLImageElement) {
        if (themeMode === "light") {
          elems.style.filter = "invert(0)";
        } else {
          elems.style.filter = "invert(1)";
        }
      }
    });
  }, [mountedComponent, themeMode]);
  const systemFont = `Lato, Raleway, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", sans-serif`;

  const defTypography = {
    h1: {
      fontSize: "2.2rem",
      fontWeight: 600,
      fontFamily: font || systemFont,
      color: "#3cf",
    },
    h2: {
      fontSize: "1.5rem",
      fontWeight: 500,
      fontFamily: font || systemFont,
    },

    h3: {
      fontSize: "1.3rem",
      fontWeight: 500,
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
      fontFamily: font || systemFont,
    },
    h4: {
      fontSize: "1rem",
      fontWeight: 500,
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
    body1: {
      lineHeight: 1.4,
      fontFamily: font || systemFont,
    },
    body2: {
      "fontSize": 15,
      "fontFamily": font || `Raleway, ${systemFont}`,
      "&:hover": {
        color: "#6969ff",
      },
    },
  };

  theme.typography = {
    ...theme.typography,
    ...defTypography,
    ...typography,
  };

  theme.typography.h5.fontFamily = font || systemFont;
  theme.typography.h6.fontFamily = font || systemFont;
  theme.typography.body1.textAlign = textAlign || "start";

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Paper>{children}</Paper>
    </ThemeProvider>
  );
};
