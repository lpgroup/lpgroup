import * as React from "react";
import { useState, createContext, useContext } from "react";
const UserContext = createContext({ apiKey: "", isLogin: false });
const UserSetContext = createContext<{
  setIsLogin: React.Dispatch<React.SetStateAction<boolean>>;
  setApiKey: React.Dispatch<React.SetStateAction<string>>;
  // @ts-ignore
}>({ setIsLogin: () => null, setApiKey: () => null });

export function useUser() {
  return useContext(UserContext);
}
export function useSetUser() {
  return useContext(UserSetContext);
}

const apiKeyKey = "@lpgroup/apiKey";

export function UserProvider({ children }: { children: React.ReactNode }) {
  const arrPath = location.pathname.split("/");
  const password = arrPath[arrPath.length - 1];
  const [apiKey, setApiKey] = useState(() => localStorage.getItem(apiKeyKey) || password || "");
  const [isLogin, setIsLogin] = useState<boolean>(false);

  React.useEffect(() => {
    if (!isLogin) return;
    apiKey && localStorage.setItem(apiKeyKey, apiKey);
  }, [isLogin]);

  const value = React.useMemo(() => ({ apiKey, isLogin }), [apiKey, isLogin]);
  const setValue = React.useMemo(() => ({ setIsLogin, setApiKey }), [setIsLogin, setApiKey]);

  return (
    <UserContext.Provider value={value}>
      <UserSetContext.Provider value={setValue}>{children}</UserSetContext.Provider>
    </UserContext.Provider>
  );
}
