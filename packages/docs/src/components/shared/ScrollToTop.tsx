import * as React from "react";
import { Box, NoSsr, Zoom, Fab } from "@mui/material";
import { useScrollTrigger } from "@mui/material";
import { KeyboardArrowUp as KeyboardArrowUpIcon } from "@mui/icons-material";

export const ScrollToTop = () => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });
  return (
    <NoSsr>
      <Zoom in={trigger}>
        <Box
          onClick={() =>
            // @ts-ignore
            document
              ?.getElementById("top-page")
              .scrollIntoView()
          }
          role="presentation"
          sx={{ position: "fixed", bottom: 24, right: 32 }}
        >
          <Fab color="primary" size="small" aria-label="scroll back to top">
            <KeyboardArrowUpIcon />
          </Fab>
        </Box>
      </Zoom>
    </NoSsr>
  );
};
