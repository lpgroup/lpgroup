import * as React from "react";
import { Box, Skeleton, Stack } from "@mui/material";

export const DocsSkeleton = () => {
  return (
    <Stack spacing={1 / 2} mt={5}>
      {[...Array(3)].map((e, i) => (
        <Box key={i}>
          <Skeleton variant="text" width={200} height={60} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} sx={{ maxWidth: 400 }} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} sx={{ maxWidth: 400 }} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} />
          <Skeleton variant="text" height={30} sx={{ maxWidth: 400 }} />
          <Box py={3}>
            <Skeleton variant="rectangular" height={150} />
          </Box>
        </Box>
      ))}
    </Stack>
  );
};
