import * as React from "react";
import { useState, useEffect, useLayoutEffect } from "react";
import { Drawer, Box, Typography, Collapse } from "@mui/material";
import { List, ListItemButton, ListItemText } from "@mui/material";
import { KeyboardArrowDown as KeyboardArrowDownIcon } from "@mui/icons-material";
import { KeyboardArrowRight as KeyboardArrowRightIcon } from "@mui/icons-material";
import { formatAnchorId, useUser } from "../";

export const Sidebar = (props: any) => {
  const { open, variant, onClose, sideBarWidth, sideBarHeight } = props;
  const [headings, setHeadings] = useState<any[]>([[]]);
  const [tagLength, setTagLength] = useState(0);
  const { isLogin } = useUser();
  const [isOnload, setIsOnLoad] = useState(true);

  const processes = () => {
    const main: any[] = [];
    let sub: any[] = [];
    const all = [];
    let x = -1;
    const targetNode = document.querySelector("#top-page");
    if (!targetNode) return;
    const tags = targetNode.querySelectorAll("[id]");
    if (tags.length === 0) return;
    tags.forEach((item) => {
      const text = item.innerHTML;
      const tag = item.tagName;
      all.push({ tag, text });
      if (tag === "H1") {
        x += 1;
        sub = [];
      }
      sub.push({ tag, text });
      main[x] = sub;
    });
    setHeadings(main);
    if (document.location.hash) {
      const urlHash = document.location.hash.substring(1);
      const hashId = document.querySelector(`#${urlHash}`);
      if (!hashId) return;
      if (isOnload) {
        hashId.scrollIntoView({ behavior: "smooth" });
        setIsOnLoad(false);
      }
    }
  };

  React.useEffect(() => {
    const targetNode = document.querySelector("#top-page");
    if (!targetNode) return;
    const tags = targetNode.querySelectorAll("h1, h2");
    const cb = (e: Event | any) => {
      const hTagId = e?.target?.id;
      if (!hTagId) return;
      const urlHash = document.location.hash.substring(1);
      if (urlHash === hTagId) return;
      window.history.pushState(null, document.title, `#${hTagId}`);
    };
    tags.forEach((e) => {
      e.addEventListener("mouseenter", cb, false);
    });
    return () => {
      tags.forEach((e) => {
        e.removeEventListener("mouseenter", cb);
      });
    };
  }, [tagLength]);

  useEffect(() => {
    processes();
  }, [tagLength, isLogin]);

  useLayoutEffect(() => {
    const targetNode = document.querySelector("#top-page");
    if (!targetNode) return;
    const callback = () => {
      const ids = targetNode.querySelectorAll("[id]");
      setTagLength(ids.length);
    };
    const observer = new MutationObserver(callback);
    observer.observe(targetNode, {
      attributes: true,
      childList: true,
      subtree: true,
    });
    return () => observer.disconnect();
  }, []);

  return (
    <Drawer
      anchor="left"
      onClose={() => onClose()}
      open={open}
      variant={variant}
      sx={{
        "& .MuiPaper-root": {
          transition: "all  ",
          width: { xs: sideBarWidth.md, ...sideBarWidth },
          // overflow: "hidden",
          top: { xs: 0, md: sideBarHeight },
          height: { xs: "100%", md: "calc(100% - 60px)" },
        },
      }}
    >
      <Box padding={2}>
        <Box marginBottom={3} paddingTop={2}>
          <Typography
            variant="caption"
            // color={"text.secondary"}
            sx={{
              fontWeight: 700,
              fontSize: 15,
              color: "brown",
              textTransform: "uppercase",
              marginBottom: 1,
              display: "block",
            }}
          >
            Getting started
          </Typography>
          <Box>
            {headings.map((item, i) => {
              return <CollpasableTag title={item[0]?.text} key={i} item={item} />;
            })}
          </Box>
        </Box>
      </Box>
    </Drawer>
  );
};

const CollpasableTag = ({ title, item }: { title: string; item: any }) => {
  const [open, setOpen] = React.useState(false);

  const scrollToId = (itemText: string) => {
    const itemId = formatAnchorId(itemText);
    try {
      window.history.pushState(null, document.title, `#${itemId}`);
    } finally {
      const nd = document.getElementById(`${itemId}`);
      nd?.scrollIntoView({ behavior: "auto" });
    }
  };
  const onItemClick = (itemText: string | undefined) => {
    setOpen((v) => !v);
    itemText && scrollToId(itemText);
  };

  return (
    <div>
      <ListItemButton onClick={() => onItemClick(title)}>
        <ListItemText
          primary={title}
          primaryTypographyProps={{
            variant: "body2",
          }}
        />
        {open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit>
        {item.map((h2: any, x: any) => {
          if (h2.tag === "H1") return null;
          return (
            <List component="div" disablePadding key={x}>
              <ListItemButton onClick={() => scrollToId(h2.text)} dense sx={{ pl: 4, py: 0 }}>
                <ListItemText primary={h2.text} />
              </ListItemButton>
            </List>
          );
        })}
      </Collapse>
    </div>
  );
};
