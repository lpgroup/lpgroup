/* eslint-disable react/no-unescaped-entities */
import * as React from "react";
import { Box, Grid, Dialog, Tooltip } from "@mui/material";
import { TextField, Button, Typography } from "@mui/material";
import { Login as LoginIcon } from "@mui/icons-material";
import { Logout as LogoutIcon } from "@mui/icons-material";
import { useUser, useSetUser } from "../";

interface LoginProps {
  children?: React.ReactNode;
}

export const LoginButton = ({ children }: LoginProps) => {
  const [isDialog, setIsDialog] = React.useState(false);
  const { setIsLogin, setApiKey } = useSetUser();
  const { isLogin, apiKey } = useUser();
  const inputRef = React.useRef<any>(null);
  const [error, setError] = React.useState(false);
  const urlPath = window.location.origin + window.location.pathname;
  // @ts-ignore
  const BASE_URL = import.meta?.env?.BASE_URL || "/";

  const onSubmit = (e: any) => {
    e.preventDefault();
    setApiKey(e.target.password.value);
    setTimeout(() => setError(true), 500);
  };
  const buttonLogIn = () => {
    setIsDialog(true);
    setTimeout(() => {
      inputRef?.current?.focus();
    });
  };

  const buttonLogOut = () => {
    setIsLogin(false);
    setApiKey("");
    localStorage.removeItem("@lpgroup/apiKey");
    window.history.pushState(null, document.title, `${window.location.origin}${BASE_URL}`);
  };

  const onClose = () => {
    setIsDialog(false);
  };

  React.useEffect(() => {
    if (isLogin) {
      if (!urlPath.includes(apiKey)) {
        window.history.pushState(
          null,
          document.title,
          BASE_URL === "/" ? `${urlPath}${apiKey}` : `${urlPath}/${apiKey}`,
        );
      }
      onClose();
    }
    setError(false);
  }, [isLogin]);

  return (
    <>
      <Dialog open={isDialog} onClose={onClose}>
        <Box p={3} pt={1} border={"3px solid #1169efe8"}>
          <Box display={"flex"} justifyContent={"flex-end"}>
            <Box
              component={"svg"}
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              width={20}
              height={20}
              onClick={onClose}
              sx={{ cursor: "pointer" }}
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M6 18L18 6M6 6l12 12"
              />
            </Box>
          </Box>
          <Box marginBottom={2}>
            <Typography
              variant="h2"
              sx={{
                fontSize: 30,
                fontWeight: 600,
                pb: 0,
              }}
            >
              Sign in
            </Typography>
          </Box>
          <form onSubmit={onSubmit}>
            <Grid container spacing={4}>
              <Grid item xs={12}>
                <Box
                  display="flex"
                  flexDirection={{ xs: "column", sm: "row" }}
                  alignItems={{ xs: "stretched", sm: "center" }}
                  justifyContent={"space-between"}
                  width={1}
                  marginBottom={2}
                >
                  <Box marginBottom={{ xs: 1, sm: 0 }}>
                    <Typography variant={"subtitle2"} color="danger">
                      {error ? "Apikey is incorrect" : "Please enter customer key"}
                    </Typography>
                  </Box>
                </Box>
                <TextField
                  label="Password *"
                  variant="outlined"
                  name={"password"}
                  type={"password"}
                  inputRef={inputRef}
                  onChange={() => setError(false)}
                  fullWidth
                />
              </Grid>
              <Grid item container xs={12}>
                <Box
                  display="flex"
                  flexDirection={{ xs: "column", sm: "row" }}
                  alignItems={{ xs: "stretched", sm: "center" }}
                  justifyContent={"space-between"}
                  width={1}
                  maxWidth={600}
                  margin={"0 auto"}
                >
                  <Box marginBottom={{ xs: 1, sm: 0 }} />
                  <Button
                    size={"small"}
                    variant={"contained"}
                    sx={{ backgroundColor: "#3cf" }}
                    type={"submit"}
                  >
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Dialog>
      <Tooltip title={isLogin ? "Logout" : "Login"}>
        <Button
          sx={{
            borderRadius: 2,
            minWidth: "auto",
            padding: 1,
          }}
          onClick={isLogin ? buttonLogOut : buttonLogIn}
        >
          {children ? children : isLogin ? <LogoutIcon /> : <LoginIcon />}
        </Button>
      </Tooltip>
    </>
  );
};
