import * as React from "react";
import { Box, AppBar, Container, Button, Toolbar, alpha, useTheme } from "@mui/material";
import { Menu as MenuIcon } from "@mui/icons-material";
import { LoginButton } from "./LoginButton";
import { ThemeModeToggler } from "./ThemeModeToggler";

export const TopBar = ({ sxTopBar, onSidebarOpen, Logo, isAuth }: any) => {
  const theme = useTheme();
  return (
    <AppBar
      sx={{
        backgroundColor: theme.palette.background.paper,
        borderBottom: `1px solid ${alpha(theme.palette.divider, 0.1)}`,
        ...sxTopBar,
      }}
      elevation={0}
    >
      <Toolbar variant="dense">
        <Container sx={{ maxWidth: { lg: "none" } }}>
          <Box display={"flex"} justifyContent={"space-between"} alignItems={"center"} width={1}>
            <Box display={"flex"} alignItems={"center"} color={"primary.dark"}>
              <Box sx={{ display: { xs: "block", md: "none" } }} marginRight={2}>
                <Button
                  onClick={() => onSidebarOpen()}
                  aria-label="Menu"
                  variant={"text"}
                  sx={{
                    minWidth: "auto",
                    padding: 1,
                  }}
                >
                  <MenuIcon />
                </Button>
              </Box>
              <Logo />
            </Box>
            <Box display={"flex"}>
              <Box sx={{ display: "flex" }} alignItems={"center"}>
                {isAuth && <LoginButton />}
                <ThemeModeToggler />
              </Box>
            </Box>
          </Box>
        </Container>
      </Toolbar>
    </AppBar>
  );
};
