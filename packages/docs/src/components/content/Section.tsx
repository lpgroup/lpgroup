import * as React from "react";
import { Grid, Divider, Box } from "@mui/material";

export const wrapperSection = (props: any) => {
  const p = props.children;
  const children = p.type(p.type).props.children;
  const contents = [];
  let leftColumn: any = [];
  let rightColumn: any = [];
  let header: any = [];
  children.forEach((child: any) => {
    try {
      const tag = child.type(child.props);
      if (tag.props.variant === "h1" || tag.props.variant === "h2") {
        contents.push(
          <Section
            key={contents.length + 1}
            header={[...header]}
            leftColumn={[...leftColumn]}
            rightColumn={[...rightColumn]}
          />,
        );
        header = [child];
        leftColumn = [];
        rightColumn = [];
        // header.push(child);
      } else if (tag.type === "pre") {
        rightColumn.push(child);
      } else {
        leftColumn.push(child);
      }
    } catch {
      leftColumn.push(child);
    }
  });

  contents.push(
    <Section
      key={contents.length + 1}
      header={[...header]}
      leftColumn={[...leftColumn]}
      rightColumn={[...rightColumn]}
    />,
  );
  return <Box sx={{ mb: 5, mt: 8 }}>{contents}</Box>;
};

function Section(props: any) {
  if (props.leftColumn.length === 0 && props.rightColumn.length === 0) {
    return null;
  }

  return (
    <Grid container columnSpacing={5} maxWidth={"1400px"} margin={{ lg: "auto" }}>
      <Grid item xs={12} md={12} lg={12}>
        <Box>{props.header}</Box>
      </Grid>
      <Grid item xs={12} md={12} lg={6} paddingX={{ lg: 3 }}>
        <Box>{props.leftColumn}</Box>
      </Grid>
      <Grid item xs={12} md={12} lg={6} paddingX={{ lg: 3 }} marginTop={{ xs: 10, md: 10, lg: 0 }}>
        {props.rightColumn}
      </Grid>
      <Grid item xs={12} py={2} mb={5} md={12}>
        <Divider flexItem />
      </Grid>
    </Grid>
  );
}
