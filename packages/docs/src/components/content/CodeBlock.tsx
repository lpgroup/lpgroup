import * as React from "react";
import { Box, useTheme, Typography } from "@mui/material";
import SyntaxHighlighter from "react-syntax-highlighter";
import sunburst from "react-syntax-highlighter/dist/esm/styles/hljs/sunburst";
import vs2015 from "react-syntax-highlighter/dist/esm/styles/hljs/vs2015";

export const CodeBlock = (props: any) => {
  const { children } = props;
  if (!children || typeof children !== "string") return null;
  const language = props.className ? props.className.replace(/language-/, "") : "";
  if (!props?.children.includes("\n")) {
    return (
      <Typography
        component="span"
        variant="subtitle2"
        sx={{
          border: "1px solid #c0a5a5",
          bgcolor: "divider",
          borderRadius: "3px",
          py: 0.3,
          px: 0.5,
          opacity: "90%",
        }}
      >
        {children}
      </Typography>
    );
  }
  return <RenderCodeBlock code={children.trim()} language={language} />;
};

const RenderCodeBlock = ({ code = "", language = "jsx" }) => {
  const theme = useTheme();
  const isDarkMode = theme.palette.mode !== "light";
  return (
    <Box
      component={SyntaxHighlighter}
      language={language}
      style={isDarkMode ? sunburst : vs2015}
      padding={`${theme.spacing(2)} !important`}
      borderRadius={2}
    >
      {code}
    </Box>
  );
};
