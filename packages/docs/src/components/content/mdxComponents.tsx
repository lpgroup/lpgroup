import * as React from "react";
import { Box, Typography, Table, TableRow, TableContainer } from "@mui/material";

import { Attribute, Attributes } from "./Attributes";
import { CodeBlock } from "./CodeBlock";
import { formatAnchorId } from "./formatAchorId";
import { wrapperSection as wrapper } from "./Section";

type mdxComponentProps = import("mdx/types").MDXComponents | undefined;

export const mdxComponents: mdxComponentProps = {
  wrapper,
  a: (props) => <a {...props} />,
  blockquote: (props) => (
    <blockquote style={{ borderLeft: ".8rem solid #80808038", paddingLeft: "1rem" }} {...props} />
  ),
  code: CodeBlock,
  // delete: (props) => <delete {...props} />,
  em: (props) => <em {...props} />,
  h1: (props) => (
    <Typography variant="h1" id={formatAnchorId(props.children)} pb={1}>
      {props.children}
    </Typography>
  ),
  h2: (props) => (
    <Typography variant="h2" id={formatAnchorId(props.children)} pb={1 / 2}>
      {props.children}
    </Typography>
  ),
  h3: (props) => <Typography variant="h3">{props.children}</Typography>,
  h4: (props) => <Typography variant="h4">{props.children}</Typography>,
  h5: (props) => <Typography variant="h5">{props.children}</Typography>,
  h6: (props) => <Typography variant="h6">{props.children}</Typography>,
  hr: (props) => <hr {...props} />,
  // biome-ignore lint/a11y/useAltText:
  img: (props) => <img {...props} />,
  inlineCode: (props) => <code>{props.children}</code>,
  li: (props) => <li {...props} />,
  ol: (props) => (
    <Box component={"ol"} sx={{ paddingInlineStart: "40px", marginBlock: "1em" }}>
      {props.children}
    </Box>
  ),
  p: (props) => (
    <Typography variant="body1" py={1 / 2}>
      {props.children}
    </Typography>
  ),
  pre: (props) => <pre style={{ margin: "1em 0px" }} {...props} />,
  strong: (props) => <strong {...props} />,
  sup: (props) => <sup {...props} />,
  table: (props) => (
    <TableContainer sx={{ p: 2 }}>
      <Table>{props.children}</Table>
    </TableContainer>
  ),
  th: (props) => (
    <Box component={"th"} sx={{ whiteSpace: "nowrap", textAlign: "left", p: 1 }}>
      <Typography fontWeight="bold" variant="subtitle1">
        {props.children}
      </Typography>
    </Box>
  ),
  td: (props) => (
    <Box component={"td"} sx={{ p: 1, textAlign: "left" }}>
      <Typography variant="subtitle2">{props.children}</Typography>
    </Box>
  ),
  tr: (props) => (
    <TableRow
      sx={{
        "&:last-child td, &:last-child th": {
          borderBottom: "1px solid InactiveBorder",
        },
        "&:hover": { bgcolor: "divider" },
      }}
    >
      {props.children}
    </TableRow>
  ),
  ul: (props) => (
    <Box component={"ul"} sx={{ paddingInlineStart: "40px", marginBlock: "1em" }}>
      {props.children}
    </Box>
  ),
  Attribute,
  Attributes,
};
