# @lpgroup/feathers-utils

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-utils.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-utils) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-utils/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-utils)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Collection of feathers-utils hooks and helper functions

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-utils
```

## Example

```js
const auth = require("@lpgroup/feathers-utils");
```

## How to use cache feature

If you want to cache the mongodb document (JSON) in the cache the only thing you need to do is to extend from ServiceCache instead of
Service. it will use the default cache configuration. The cached item will be invalidated and removed on update, patch, removed.

max: 500 items
ttl: 300000 // 5 minutes

you can override this in service options

```js
const options = {
  id: "_id",
  paginate: app.get("paginate"),
  schema,
  cache: { max: 1000 },
};
```

Example using service cache

```js
const { ServiceCache } = require("@lpgroup/feathers-utils");

export class Service1 extends ServiceCache {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("serivce1");
    });
  }
}
```

If you want to cache just part of the the mongodb document (JSON) in the cache. You can create caches on the service that you can access anywhere where you have access to the app.

You can specify a callback to set data in the cache and options:

```js
const { CreateCache } = require("@lpgroup/feathers-utils");
import { Service } from "feathers-mongodb";

export class Service1 extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("serivce1");
    });

    CreateCache(
      this,
      "mapToOwner",
      (data) => {
        return { owner: data.owner };
      },
      { max: 1000 },
    );
  }
}
```

To use the cache you get access to the caches from the service:

```js
// setting the owner of the message to the same as the visitor, this makes it possible for
// admin to create messages for the visitors.
const { owner } = await app
  .service("/organisations/:organisationAlias/bots/:botAlias/visitors")
  .caches.mapToOwner.get(visitorId);

data.owner = owner;
```

## limitRequest

This is a middleware to limit the request. And will respond a status of 429.

```
import { limitRequest } from "@lpgroup/feathers-utils";

export default (app) => {
  app.use(limitRequest(app));
};

```

default.json

```
  "tooManyRequest": {
    "isEnabled": true,
    "buketSize": 80,
    "ttlSec": 2,
    "whiteList": ["127.0.0.2"]
  },

```

| Property   | Default | Type      | Description                    |
| ---------- | ------- | --------- | ------------------------------ |
| isEnabled  | `true`  | `boolean` | enable/disable 429             |
| bucketSize | `100`   | `number`  | num of request to display 429  |
| ttlSec     | `5` sec | `number`  | refresh bucketSize time in sec |
| whiteList  | `[]`    | `array`   | exempted IPs e.g ["127.0.0.1"] |

## API

### `xxx`

#### `xxx(xxx)`

```js
xxx(xxx);
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
