/* eslint-disable jest/no-disabled-tests */
import { fileURLToPath } from "node:url";
import { dirname, resolve } from "node:path";
import { executeDirectory } from "../src/index.js";

// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

describe("node-execute-directory", () => {
  // TODO: The dynamic import in executeDirectory fails sometimes.
  test.skip("Golden path", async () => {
    const fullPath = resolve(`${__dirname}/../data/ned`);
    const result = (await executeDirectory(fullPath)).map((v) => v.replace(fullPath, ""));
    expect(result).toEqual([
      "/01-data.js",
      "/02-data.js",
      "/03-data/03-01-data.js",
      "/03-data/03-02-data.js",
      "/03-data/03-data/03-03-01-data.js",
      "/03-data/03-data/03-03-02-data.js",
      "/03-data/04-data/03-04-01-data.js",
      "/03-data/04-data/03-04-02-data.js",
      "/04-data/04-01-data.js",
      "/04-data/04-02-data.js",
      "/04-data/03-data/04-03-01-data.js",
      "/04-data/03-data/04-03-02-data.js",
      "/04-data/04-data/04-04-01-data.js",
      "/04-data/04-data/04-04-02-data.js",
    ]);
  });
});
