/**
 * Feathersjs wrapper of axios http client.
 */
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import https from "node:https";
import { log } from "@lpgroup/utils";
import rateLimit from "axios-rate-limit";
import errors from "@feathersjs/errors";
import { memoize } from "lodash-es";
import { mergeData } from "./merge-data.js";
import { checkExpected } from "./compare.js";
import { getExponentialTimeoutWithJitter, sleep } from "./common.js";

const { info, error, debug } = log("axios");

const { NotFound, Timeout, GeneralError } = errors;

/**
 * setupAxios({baseURL: "http://cybercow.se", headers: { "X-Custom-Header": "LPGroup" }})
 */
const createOptions = {
  timeout: 60000,
  headers: { "X-Custom-Header": "lpGroup" },
};

const setupOptions = {};

let baseURL;
let errorHandlerWithException = true;
let maxRPS = 5;

// TODO: let requiredKeys = [];
let ignoreKeyCompare = [];

let globalUser;
let globalPassword;

function formatUrl(url) {
  return `${baseURL || ""}${url}`;
}

export function setupAxios(options = {}) {
  if (options.baseURL) baseURL = options.baseURL.replace(/\/$/, "");
  if (options.timeout) createOptions.timeout = options.timeout;
  if (options.customHeader) createOptions.headers["X-Custom-Header"] = options.customHeader;
  if (options["x-api-key"]) createOptions.headers["x-api-key"] = options["x-api-key"];
  if (options.headers) createOptions.headers = { ...createOptions.headers, ...options?.headers };
  if (options.acceptSelfSignedCert) setupOptions.acceptSelfSignedCert = true;

  if (options.maxRPS) maxRPS = options.maxRPS;

  if ("errorHandlerWithException" in options)
    errorHandlerWithException = options.errorHandlerWithException;

  // TODO: if ("requiredKeys" in options) requiredKeys = options.requiredKeys;
  if ("ignoreKeyCompare" in options) ignoreKeyCompare = options.ignoreKeyCompare;

  if ("user" in options) globalUser = options.user;
  if ("password" in options) globalPassword = options.password;
}

// eslint-disable-next-line no-unused-vars
function getLoginInstance(fullUrl, rawInstance, options = {}, ignoreCache = false) {
  return rawInstance.post(fullUrl, options).then((response) => {
    info(`Logged in as ${response.data.user.email}`);
    return response.data.accessToken;
  });
}
const getLoginInstanceCached = memoize(
  getLoginInstance,
  (fullUrl, rawInstance, options, ignoreCache) => JSON.stringify({ ...options, ignoreCache }),
);

// TODO: Change name?
// Other files are accessing this through, and that might be a bit confusing.
// const { instance } = require("@lpgroup/feathers-utils");
/**
 *
 * @param {*} instanceOptions
 * @param {*} reuseRawInstance An axios rateLimit object to reuse on this instance.
 */
export function instance(instanceOptions = {}, reuseRawInstance = undefined) {
  const mergeOptions = mergeData(createOptions, instanceOptions);
  if (setupOptions.acceptSelfSignedCert || instanceOptions.acceptSelfSignedCert) {
    mergeOptions.httpsAgent = new https.Agent({
      rejectUnauthorized: false,
    });
  }

  return {
    dbgIdentity: undefined,
    user: undefined,
    password: undefined,
    accessToken: undefined,
    lapse502ResponseCount: undefined,
    rawInstance: reuseRawInstance || rateLimit(axios.create(mergeOptions), { maxRPS }),

    /** Returns axios instance with login headers */
    ins() {
      if (this.accessToken) {
        this.rawInstance.defaults.headers.common.Authorization = this.accessToken;
      } else {
        delete this.rawInstance.defaults.headers.common.Authorization;
      }
      return this.rawInstance;
    },

    // TOOD: Wait longer and longer
    async waitOnServerReady(url) {
      for (let x = 0; x < 100; x += 1) {
        try {
          // eslint-disable-next-line no-await-in-loop
          const result = await this.ins().get(url);
          if (result !== undefined && result.status === 200) {
            info(`Connected to ${url}`);
            return;
          }
        } catch {
          info(`  Wait on server ${url}`);
        }
        // eslint-disable-next-line no-await-in-loop
        await sleep(500);
      }
      throw new Error("Server is not responding");
    },

    async closePlugin() {
      if (this.lapse502ResponseCount) {
        info(
          `  ${this.lapse502ResponseCount} of the import request lapsed against 502 reponse, consider running lpimport again`,
        );
      }
    },

    async login(user, password, ignoreCache = false, options = {}) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      newInstance.user = user || globalUser;
      newInstance.password = password || globalPassword;
      newInstance.dbgIdentity = newInstance.user;
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(
        fullUrl,
        this.rawInstance,
        {
          strategy: "local",
          email: newInstance.user,
          password: newInstance.password,
        },
        ignoreCache ? uuidv4() : false, // Set a random key to invalidate the cache
      ).catch(async (err) => {
        await newInstance.retryHandler("login", fullUrl, {
          ...options,
          user,
          password,
          ignoreCache,
        })(err);
        if (err?.response?.status === 502 && options?.retry >= 4) {
          throw err;
        }
        if (err?.response?.status !== 502) {
          throw err;
        }
      });
      return newInstance;
    },

    async loginDevice(deviceId, options) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      newInstance.dbgIdentity = deviceId;
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(fullUrl, this.rawInstance, {
        strategy: "device",
        _id: deviceId,
      }).catch(async (err) => {
        await newInstance.retryHandler("login", fullUrl, { ...options, deviceId })(err);
        if (err?.response?.status === 502 && options?.retry >= 4) {
          throw err;
        }
        if (err?.response?.status !== 502) {
          throw err;
        }
      });
      return newInstance;
    },

    async useApiKey(apiKey) {
      const apiKeyOptions = mergeData(instanceOptions, { headers: { "x-api-key": apiKey } });
      const newInstance = instance(apiKeyOptions);
      newInstance.dbgIdentity = apiKey;
      return newInstance;
    },

    async reauthenticate(accessToken, options) {
      const newInstance = instance(instanceOptions, this.rawInstance);
      const fullUrl = formatUrl("/authentication");
      newInstance.accessToken = await getLoginInstanceCached(fullUrl, this.rawInstance, {
        strategy: "jwt",
        accessToken,
      }).catch(async (err) => {
        await newInstance.retryHandler("reauth", fullUrl, { ...options, accessToken })(err);
        if (err?.response?.status === 502 && options?.retry >= 4) {
          throw err;
        }
        if (err?.response?.status !== 502) {
          throw err;
        }
      });
      return newInstance;
    },

    async post(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .post(fullUrl, data, { headers: options?.headers })
        .then(this.responseHandler("post", fullUrl, options, { data }))
        .catch(this.retryHandler("post", url, { data, ...options }));
    },

    async get(url, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .get(fullUrl, { headers: options?.headers })
        .then(this.responseHandler("get", fullUrl, options))
        .catch(this.retryHandler("get", url, options));
    },

    async patch(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .patch(fullUrl, data, { headers: options?.headers })
        .then(this.responseHandler("patch", fullUrl, options, { data }))
        .catch(this.retryHandler("patch", url, { data, ...options }));
    },

    async put(url, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .put(fullUrl, data, { headers: options?.headers })
        .then(this.responseHandler("put", fullUrl, options, { data }))
        .catch(this.retryHandler("put", url, { data, ...options }));
    },

    async delete(url, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .delete(fullUrl, { headers: options?.headers })
        .then(this.responseHandler("delete", fullUrl, options))
        .catch(this.retryHandler("delete", url, options));
    },

    async remove(url, options = {}) {
      return this.delete(url, options);
    },

    // TODO: Should handle other than pdf
    async downloadFile(url, options) {
      const fullUrl = formatUrl(url);
      const file = await this.ins()
        .get(url, {
          responseType: "arraybuffer",
          headers: {
            Accept: "application/pdf",
            ...options?.headers,
          },
        })
        .then(this.responseHandler("download", fullUrl))
        .catch(this.retryHandler("download", url, options));

      if (file) return file.toString("base64");
      throw Error(`Can't download file ${url}`);
    },

    async postPut(url, key, data) {
      return this.multiCallWithKey(this.postPutSingle, url, key, data);
    },

    async postPutSingle(url, key, data, options = {}) {
      const fullUrl = formatUrl(url);
      return this.ins()
        .post(fullUrl, data, { headers: options?.headers })
        .then(this.responseHandler("postPut", fullUrl, {}, { data }))
        .catch(async (err) => {
          const message = err.response?.data?.message;
          if (message?.startsWith("E11000 duplicate key error collection")) {
            const id = data[key];
            if (id) {
              return this.put(`${url}/${id}`, data);
            }
            error(`  POST: ${fullUrl} `);
            error(`    Missing ${key} in ${JSON.stringify(data)}`);
          }
          return this.retryHandler("postPutSingle", url, { key, data, ...options })(err);
        });
    },

    multiCallWithKey(func, id, key, data) {
      if (Array.isArray(data)) {
        return Promise.all(
          data.map(async (x) => {
            return func.call(this, id, key, x);
          }),
        );
      }
      return func.call(this, id, key, data);
    },

    retryHandler(method, fullUrl, options) {
      return async (err) => {
        const retry = options?.retry || 0;
        if ((!err?.response || err?.response?.status === 502) && retry <= 5) {
          error(
            `  Retrying ${method} request status: '${err?.response?.status}' message: '${err?.message}'.`,
          );
          const timeout = getExponentialTimeoutWithJitter(retry, 200, 100);
          await sleep(timeout);
          const newOptions = { ...options, retry: retry + 1 };
          switch (method) {
            case "login":
              return this.login(options.user, options.password, options.ignoreCache, newOptions);
            case "loginDevice":
              return this.loginDevice(options.deviceId, newOptions);
            case "reauth":
              return this.reauthenticate(options.accessToken, newOptions);
            case "post":
              return this.post(fullUrl, options.data, newOptions);
            case "get":
              return this.get(fullUrl, newOptions);
            case "patch":
              return this.patch(fullUrl, options.data, newOptions);
            case "put":
              return this.put(fullUrl, options.data, newOptions);
            case "delete":
              return this.remove(fullUrl, newOptions);
            case "download":
              return this.downloadFile(fullUrl, newOptions);
            case "postPutSingle":
              return this.postPutSingle(fullUrl, options.key, options.data, newOptions);
            default:
              return new Error("Something went wrong, axios");
          }
        }
        this.lapse502ResponseCount = (this.lapse502ResponseCount || 0) + 1;
        return this.errorHandler(method, fullUrl, options)(err);
      };
    },

    responseHandler(method, fullUrl, options = {}, request = {}) {
      request.method = method;
      return this.responseHandlerNoWait(method, fullUrl, options, request);
    },

    responseHandlerNoWait(method, fullUrl, options, request) {
      return (response) => {
        info(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
        checkExpected(response.data, { ignoreKeyCompare, ...options }, request);
        return response.data;
      };
    },

    errorHandler(method, fullUrl, options) {
      return (err) => {
        if (options?.expected || options?.expectedFile) {
          info(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
          if (err.response) {
            checkExpected(err.response.data, { ignoreKeyCompare, ...options }, { method });
          }
        } else if (errorHandlerWithException || options?.errorHandlerWithException) {
          debug(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
          this.errorHandlerExceptions(method, fullUrl, err);
        } else if (options?.ignoreError) {
          info(`  ${method}: ${this.dbgIdentity} ${fullUrl} `);
        } else {
          this.errorHandlerConsole(method, fullUrl, err);
        }
      };
    },

    errorHandlerConsole(method, fullUrl, err) {
      const data = err.response?.data || "";
      error(
        `ErrorHandlerConsole: ${method}: ${fullUrl} ${err.message} (${JSON.stringify(
          data,
          null,
          " ",
        )})`,
      );
    },

    errorHandlerExceptions(method, fullUrl, err) {
      if (err.response?.data) {
        if (err.response.data.code === 404) {
          throw new NotFound(err.response.data.message);
        }
        if (err.response.data.name === "GeneralError") {
          throw new GeneralError(err.response.data.message, err.response.data.data);
        }
      } else if (err.code === "ECONNABORTED" || err.code === "ECONNRESET") {
        error(`Internal timeout, (${err.code}) (${err.message})`);
        throw new Timeout("Internal timeout");
      } else if (err.code === "ECONNREFUSED") {
        error(`Can't access remote server (${err.message})`);
        throw new GeneralError("Subsystem is down (see server logs).");
      }

      throw err;
    },
  };
}

export async function login(user, password) {
  const loginInstance = instance();
  return loginInstance.login(user, password);
}

export async function noLogin(options = {}) {
  return instance(options);
}
