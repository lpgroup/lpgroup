import fs from "node:fs";

let currentFileCWD;
let testNameFileCWD;

const setActiveFileCWD = (dir = "") => {
  currentFileCWD = dir;
};

const activeFileCWD = () => currentFileCWD;

const getTestName = () => testNameFileCWD;

const setGetProperActiveCWD = (testName = null) => {
  const parts = activeFileCWD().split("/");
  const fileName = parts.pop().split(".").slice(0, -1).join(".");
  testNameFileCWD = testName || fileName;
  //
  const testResultFolder = `${parts.join("/")}/expected/${fileName}`;
  if (!fs.existsSync(testResultFolder)) {
    fs.mkdirSync(testResultFolder, { recursive: true });
  }
  return testResultFolder;
};

export { setActiveFileCWD, setGetProperActiveCWD, getTestName };
