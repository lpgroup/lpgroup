export function getUrl(context, ignoreUrls = []) {
  let url = Object.entries(context.params.query || {}).reduce((accUrl, [key, value]) => {
    return accUrl.replace(`:${key}`, value);
  }, context.path);

  if (context.id) url += `/${context.id}`;

  if (ignoreUrls.includes(url)) return "";
  return url?.charAt(0) === "/" ? url : `/${url}`;
}
