import errors from "@feathersjs/errors";
import { log } from "@lpgroup/utils";

const { error } = log("exception");

export default () => {
  return (context) => {
    const err = context.error;
    if (err) {
      if (err.isAxiosError) {
        error("axios", err.response.data);
      } else if (err.className === "TypeError") {
        error(err.stack);
      } else if (!context.result && err.type !== "FeathersError") {
        if (
          err.message === "Current topology does not support sessions" ||
          err.codeName === "NoSuchTransaction"
        ) {
          error(`Mongo: ${err.message}`);
        } else if (![11000].includes(err.code)) {
          error(`Stack: ${err.stack}`);
        }
      }

      if (!err.code) {
        const newError = new errors.GeneralError("server error");
        context.error = newError;
        return context;
      }

      if (err.code === 404 || process.env.NODE_ENV === "production") {
        err.stack = null;
      }
    }
    return context;
  };
};
