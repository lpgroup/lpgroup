/* eslint-disable import/no-extraneous-dependencies, no-param-reassign,  */
import { log } from "@lpgroup/utils";
import { buildItemHook } from "@lpgroup/feathers-utils";

const { error } = log("utils");

/**
 * Add url key to json row.
 */
export default () => {
  return buildItemHook((context, item) => {
    // eslint-disable-next-line no-underscore-dangle
    if (context.params._calledByHook) return context;
    const collectionKey = context?.service?.options?.id;

    const { app } = context;
    if (app.get("mainDomain")) {
      error("Deprecated: mainDomain config");
    }
    const host = app.get("apiDomain") || app.get("mainDomain");

    const query =
      context?.params?.query && Object.keys(context?.params?.query).length === 0
        ? Object.entries(context.params.query)
        : Object.entries(item);

    const url = query.reduce((accUrl, [key, value]) => {
      return accUrl.replace(`:${key}`, value);
    }, context.path);

    item.url = host.concat("/", url);
    if (collectionKey) item.url += `/${item[collectionKey]}`;

    return context;
  });
};
