/**
 * feathersjs supports both rest and websockets. Websockets
 * uses query-parameters instead of routing parameters. We decided
 * to only use query parameters everywhere. This code will MOVE
 * routing parameters to the query parameter.
 */

function getParameterSearchOperator(value) {
  const searchOperator = {
    $or: true,
    $ne: true,
    $gt: true,
    $gte: true,
    $lt: true,
    $lte: true,
  };

  if (typeof value === "object") {
    const operator = Object.keys(value)[0];
    if (searchOperator[operator]) return operator;
  }

  return undefined;
}

function convertToBoolean(value) {
  if (value === "true" || value === "TRUE") return true;
  if (value === "false" || value === "FALSE") return false;

  return value;
}

// convert all ".at" unixtime to number
function convertToTimestamp(queryName, value) {
  if (queryName.endsWith(".at")) {
    const parsed = Number.parseInt(value, 10);
    // eslint-disable-next-line no-restricted-globals
    if (!Number.isNaN(parsed)) {
      return parsed;
    }
  }
  return value;
}

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  // TODO: Maybe we should user $populate, looks like feathersjs standard?
  const whiteList = ["populate", "unpopulate"];
  return async (context) => {
    const { params } = context;

    Object.keys(params.query || {}).forEach((queryName) => {
      const searchOperator = getParameterSearchOperator(params.query[queryName]);

      // trim and modify all query params
      if (searchOperator && typeof params.query[queryName][searchOperator] === "string") {
        params.query[queryName][searchOperator] = convertToBoolean(
          convertToTimestamp(queryName, params.query[queryName][searchOperator].trim()),
        );
      } else if (typeof params.query[queryName] === "string") {
        params.query[queryName] = convertToBoolean(
          convertToTimestamp(queryName, params.query[queryName].trim()),
        );
      }

      // Move whiteListed query parameters to params
      if (whiteList.includes(queryName)) {
        params[queryName] = params.query[queryName];
        delete params.query[queryName];
      }
    });

    Object.keys(params.route || {}).forEach((param) => {
      params.query[param] = params.route[param];
      delete params.route[param];
    });
    return context;
  };
};
