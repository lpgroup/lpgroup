/* eslint-disable no-param-reassign */
import { log } from "@lpgroup/utils";
import { LRUCache } from "lru-cache";
import { Service } from "feathers-mongodb";
import { internalParams } from "./common.js";

const { info } = log("cache");

const globalOptions = {
  // the number of most recently used items to keep.
  // note that we may store fewer items than this if maxSize is hit.

  max: 500, // <-- mandatory, you must give a maximum capacity

  // max time to live for items before they are considered stale
  // note that stale items are NOT preemptively removed by default,
  // and MAY live in the cache, contributing to its LRU max, long after
  // they have expired.
  // Also, as this cache is optimized for LRU/MRU operations, some of
  // the staleness/TTL checks will reduce performance, as they will incur
  // overhead by deleting items.
  // Must be a positive integer in ms, defaults to 0, which means "no TTL"
  ttl: 1000 * 60 * 5,

  // return stale items from cache.get() before disposing of them
  // boolean, default false
  allowStale: false,

  // update the age of items on cache.get(), renewing their TTL
  // boolean, default false
  updateAgeOnGet: true,

  // update the age of items on cache.has(), renewing their TTL
  // boolean, default false
  updateAgeOnHas: true,
};

export function CreateCache(service, name, callback, options) {
  const lru = new LRUCache({ ...globalOptions, ...options });
  const newCache = {
    get: async (id, params = {}) => {
      if (!lru.has(id)) {
        const result = await service.get(id, internalParams({ ...params, superUser: true }));
        const cacheResult = callback(result);
        lru.set(id, cacheResult);
        info(`Set cache [${service.Model.namespace}][${name}]:`, id);
      }
      info(`Get cache [${service.Model.namespace}][${name}]:`, id);
      return lru.get(id);
    },
  };

  if (!service.caches) service.caches = {};
  service.caches[name] = newCache;
  return newCache;
}

export class ServiceCache extends Service {
  constructor(options) {
    super(options);
    this.cacheDefault = {
      get: { max: 500, callback: (data) => data },
      find: { max: 0, callback: (data) => data },
    };

    this.cacheOptions = {};
    this.cacheOptions.get = {
      ...globalOptions,
      ...this.cacheDefault.get,
      ...options?.cache?.get,
    };

    this.cacheOptions.find = {
      ...globalOptions,
      ...this.cacheDefault.find,
      ...options?.cache?.find,
    };

    if (this.cacheOptions.get.max > 0) this.lruGet = new LRUCache(this.cacheOptions.get);
    if (this.cacheOptions.find.max > 0) this.lruFind = new LRUCache(this.cacheOptions.find);
  }

  deleteCache(data) {
    if (this.lruGet) {
      const idKey = this.id;
      const idValue = data[idKey];
      if (idValue && this.lruGet.has(idValue)) {
        this.lruGet.delete(idValue);
        info(`Del cache ${idKey} [${this.Model.namespace}]:${idValue}`);
      }
    }

    if (this.lruFind) {
      this.lruFind.clear();
    }
  }

  setup() {
    // Need to listen on websockets, to handle several pods through feathers-sync
    this.on("removed", this.deleteCache);
    this.on("created", this.deleteCache);
    this.on("updated", this.deleteCache);
    this.on("patched", this.deleteCache);

    info(
      `Config cache [${this.Model.namespace}] getMax: ${this.cacheOptions.get.max}, findMax: ${this.cacheOptions.find.max}`,
    );
  }

  async find(params) {
    if (!this.lruFind) return super.find(params);

    const id = JSON.stringify(params.query);
    if (!this.lruFind.has(id)) {
      const result = await super.find(params);
      const cacheResult = this.cacheOptions.find.callback(result);
      this.lruFind.set(id, cacheResult);
      info(`Set cache [${this.Model.namespace}]:`, id);
      return cacheResult;
    }
    info(`Get cache [${this.Model.namespace}]:`, id);
    return this.lruFind.get(id);
  }

  async get(id, params) {
    if (!this.lruGet) return super.get(id, params);

    if (!this.lruGet.has(id)) {
      const result = await super.get(id, params);
      const cacheResult = this.cacheOptions.get.callback(result);
      this.lruGet.set(id, cacheResult);
      info(`Set cache [${this.Model.namespace}]:`, id);
      return cacheResult;
    }
    info(`Get cache [${this.Model.namespace}]:`, id);
    return this.lruGet.get(id);
  }

  async create(data, params) {
    const res = await super.create(data, params);
    this.deleteCache(res);
    return res;
  }

  async update(id, data, params) {
    const res = await super.update(id, data, params);
    this.deleteCache(res);
    return res;
  }

  async patch(id, data, params) {
    const res = await super.patch(id, data, params);
    this.deleteCache(res);
    return res;
  }

  async remove(id, params) {
    const res = await super.remove(id, params);
    this.deleteCache(res);
    return res;
  }
}
