import rateLimit from "express-rate-limit";
import { log } from "@lpgroup/utils";
/*
Requires remoteAddress middleware to set "remoteAddress".
*/

const { error } = log("app");

//
// TODO: Need to test this with both rest and socket
//
export default (app) => {
  const { isEnabled, ttlSec, buketSize, whiteList } = app.get("tooManyRequest") || {
    isEnabled: true,
    ttlSec: 5,
    buketSize: 100,
    whiteList: [],
  };

  if (!isEnabled)
    return (req, res, next) => {
      next();
    };

  const limiter = rateLimit({
    windowMs: ttlSec * 1000,
    max: buketSize,
    message: "Slow down, you incurred too many request",
    skip: (req) => {
      const ip = req.feathers.remoteAddress;
      if (!ip) error("Need to configure remoteAddress middleware when using limitRequest.");
      if (whiteList.includes(ip)) {
        return true;
      }
      return false;
    },
  });

  return limiter;
};
