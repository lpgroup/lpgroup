/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/**
 * Add the protocol that is used to params in all services.
 *
 * Example: app.use(headers( io, ["user-agent"]));
 * @param {*} socket true - is a socket middleware
 *                   false - is a rest middleware
 */
export function ioHeaders(socket, headers) {
  headers.forEach((headerKey) => {
    socket.feathers.headers[headerKey] = socket?.request?.headers[headerKey];
  });
}

export function restHeaders(app) {
  return (req, res, next) => {
    // Already have all headers here
    // req.feathers.headers;
    next();
  };
}
