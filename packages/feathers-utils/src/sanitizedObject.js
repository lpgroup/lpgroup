/* eslint-disable no-restricted-syntax */

/**
 * Sanitizes the given object by replacing specified keys with a provided replacement value.
 * @param {Object | Array} obj - The object to sanitize. Can be an object or an array.
 * @param {string[]} wildKeys - An array of keys to be replaced.
 * @param {string} [replaceWith="<ignored>"] - The value to replace the wildKeys with. Default is "<ignored>".
 * @returns {Object | Array} - The sanitized object.
 */
export function sanitizedObject(obj, wildKeys, replaceWith = "<ignored>") {
  if (typeof obj !== "object" || obj === null) {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.map((item) => sanitizedObject(item, wildKeys, replaceWith));
  }
  const sanitized = {};
  const keys = Object.keys(obj);

  for (const key of keys) {
    if (wildKeys.includes(key)) {
      sanitized[key] = replaceWith;
    } else {
      sanitized[key] = sanitizedObject(obj[key], wildKeys, replaceWith);
    }
  }
  return sanitized;
}
