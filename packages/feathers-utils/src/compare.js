import { log } from "@lpgroup/utils";
import fs from "node:fs";
import path from "node:path";
import { deepSortDiff } from "./deep-sort-diff.js";
import { setGetProperActiveCWD, getTestName } from "./axiosStateUtils.js";
import { sanitizedObject } from "./sanitizedObject.js";

const { error } = log("utils");

export function removeEditedKeys(diff, ignoreKeyCompare) {
  if (diff === undefined) return [];
  return diff.filter((row) => {
    if (
      (row.kind === "E" || row.kind === "D" || row.kind === "A" || row.kind === "N") &&
      row.path
    ) {
      // If any of the json keys in the row.path array are to be
      // ignored. The filter shouldn't return them.
      if (row.path.some((v) => ignoreKeyCompare.includes(v))) return false;
    }
    return true;
  });
}

const writeJsonToFile = (directory, fileName, data, overwrite = true) => {
  const filePath = path.join(directory, `${fileName}.json`);
  const jsonData = JSON.stringify(data, null, 2);
  const isPathExist = fs.existsSync(filePath);
  if (overwrite || !isPathExist) {
    fs.writeFileSync(filePath, jsonData);
    if (!isPathExist) {
      error(
        `##############################\n Written for the first time:\n ${filePath} \n##############################\n`,
      );
    } else if (overwrite) {
      error(
        `##############################\n Overwritten due to diff:\n ${filePath} \n##############################\n`,
      );
    }
  }
};

const readJsonFromFile = (directory, fileName) => {
  const filePath = path.join(directory, `${fileName}.json`);
  try {
    if (!fs.existsSync(filePath)) {
      return null;
    }
    const jsonData = fs.readFileSync(filePath, "utf8");
    const data = JSON.parse(jsonData);
    return data;
  } catch (err) {
    error(`Error reading JSON file: ${filePath} ${err}`);
    return null;
  }
};

function checkExpectedFile(response, options, request = {}) {
  const directory = setGetProperActiveCWD(options?.expectedFile);
  const nameOfTest = getTestName();
  const { method = "manual" } = request;
  const storedExpected = readJsonFromFile(directory, `${nameOfTest}-${method}-expected`) || {};
  const { ignoreKeyCompare = [] } = options;
  const diff = removeEditedKeys(deepSortDiff(response, storedExpected, options), ignoreKeyCompare);
  if (diff.length > 0) {
    writeJsonToFile(
      directory,
      `${nameOfTest}-${method}-expected`,
      sanitizedObject(response, ignoreKeyCompare),
    );
    if (options.writeOriginal) {
      writeJsonToFile(
        directory,
        `${nameOfTest}-${method}-expected-original`,
        storedExpected,
        false,
      );
    }
    if (options.writeRequest) {
      writeJsonToFile(directory, `${nameOfTest}-${method}-request`, request?.data || {});
    }
    if (options.writeDiff) {
      writeJsonToFile(directory, `${nameOfTest}-${method}-diff`, diff);
    }
    if (Object.keys(storedExpected).length !== 0) {
      error("Diff: ", JSON.stringify(diff, null, 2));
    }

    return true;
  }

  return false;
}

/**
 *
 * @param {*} response
 * @param {*} options {
 *   expected: {},
 *   ignoreKeyCompare: ["key"]
 * }
 */
export function checkExpected(response, options, request) {
  if (options.expectedFile) {
    return checkExpectedFile(response, options, request);
  }

  if ("expected" in options) {
    const { ignoreKeyCompare = [] } = options;
    // TODO: Fungerar utan denna?
    // if (!requiredKeys.every((k) => k in response)) {
    //   debug(`Response need the following keys ${requiredKeys}`);
    // }
    const diff = removeEditedKeys(
      deepSortDiff(response, options.expected, options),
      ignoreKeyCompare,
    );
    if (diff.length > 0) {
      error("Response: ", JSON.stringify(response, null, 2));
      error("Diff: ", JSON.stringify(diff, null, 2));
      return true;
    }
  }
  return false;
}
