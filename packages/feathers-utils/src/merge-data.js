import { mergeWith, find, remove } from "lodash-es";

function equalKeys(o1, o2, key) {
  return o1 && o2 && key in o1 && key in o2 && o1[key] === o2[key];
}

function hasOnlyIndexKey(obj) {
  const keys = Object.keys(obj);
  return keys.length === 1 && ["_id", "alias"].includes(keys[0]);
}

function customizer(targetValue, srcValue) {
  if (Array.isArray(targetValue) && Array.isArray(srcValue)) {
    const hasTargetId = targetValue.some((v) => v?._id || v?.alias);
    const hasSrcId = srcValue.some((v) => v?._id || v?.alias);

    if (targetValue.length > 0 && srcValue.length > 0 && hasTargetId !== hasSrcId)
      throw new Error("Both target and source need to have _id, alias or no _id, alias");

    if (srcValue.length === 0 && hasTargetId) return targetValue;
    if (!hasSrcId) return srcValue;

    for (let i = 0; i < srcValue.length; i += 1) {
      const value = srcValue[i];
      const targetToUpdate = find(
        targetValue,
        (o) => equalKeys(o, value, "_id") || equalKeys(o, value, "alias"),
      );
      if (targetToUpdate) {
        // Remove the object if only _id or alias is in the value object from srcValue
        if (hasOnlyIndexKey(value)) {
          remove(targetValue, (v) => equalKeys(v, value, "_id") || equalKeys(v, value, "alias"));
        } else {
          mergeWith(targetToUpdate, value, customizer);
        }
      }
      // Add object if it has other keys than only _id or alias. One index key means that
      // the value was supposed to be deleated.
      else if (!hasOnlyIndexKey(value)) {
        targetValue.push(value);
      }
    }
    return targetValue;
  }
  return undefined;
}
export function mergeData(source1, source2) {
  const target = {};
  mergeWith(target, source1, source2, customizer);
  return target;
}
