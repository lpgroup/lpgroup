import deepDiff from "deep-diff";
import { deepSortObjectArray } from "./deep-sort-object-array.js";

const { diff } = deepDiff;

export function deepSortDiff(a, b, options) {
  const sortedA = deepSortObjectArray(a, options);
  const sortedB = deepSortObjectArray(b, options);

  const result = diff(sortedA, sortedB);
  return result || [];
}
