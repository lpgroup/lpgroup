import path from "node:path";
import fs from "node:fs";
import { program } from "commander";
import { log } from "@lpgroup/utils";

const { error } = log("import-cli");

export function getCmdLineArgs() {
  program
    .version("0.0.1", "--version", "output the current version")
    .option(
      "-e, --environment <env>",
      "Environment select configuration from .import.json",
      "default",
    )
    .option("-w, --cwd <folder>", "Folder to import from", "./import")
    .option("-t, --extension <reqexp>", "Regexp of file extension to include", ".js$")
    .option("-i, --include <reqexp>", "Regexp to files to only include", "")
    .option("-x, --exclude <reqexp>", "Regexp to files to exclude", "")
    .option("-v, --verbose", "Regexp to files to exclude", false)
    .parse(process.argv);

  program.opts().cwd = path.resolve(program.opts().cwd);
  if (!fs.existsSync(program.opts().cwd)) {
    error(`Folder doesn't exist: ${program.opts().cwd}`);
  }
  return { ...program.opts(), version: program.version() };
}

const loadJSON = (fPath) => JSON.parse(fs.readFileSync(new URL(fPath, import.meta.url)));

export async function getConfig(env) {
  let config = {};
  const filePath = path.resolve(".import.json");
  if (fs.existsSync(filePath)) {
    config = loadJSON(`file:///${filePath}`);
  } else {
    throw Error(`.import.json doesn't exist.`);
  }

  if (config.environments?.[env]) {
    const confEnv = config.environments[env];
    const { server, user, password, readyServer, headers = {}, maxRPS } = confEnv.http || {};
    const { uri, queue, messagePrefix, version } = confEnv.nats || {};
    const { uri: rabbitUri, queue: rabbitQueue } = confEnv.rabbitmq || {};

    // password can be a name of an environment variable.
    const processedPassword = process.env[password] || password;

    return {
      environment: env,
      http: { server, user, password: processedPassword, readyServer, headers, maxRPS },
      nats: { uri, queue, messagePrefix, version },
      rabbitmq: { uri: rabbitUri, queue: rabbitQueue },
      requiredKeys: config.requiredKeys || {},
      ignoreKeyCompare: config.ignoreKeyCompare || {},
    };
  }
  throw Error(`Environment (${env}) doesn't exist in .import.json`);
}
