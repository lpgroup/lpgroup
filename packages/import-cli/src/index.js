/**
 * Helper functions that can be used by the import scripts
 */
import fs from "node:fs";
import os from "node:os";
import { resolve, join } from "node:path";
import { PDFDocument } from "pdf-lib";
import { log } from "@lpgroup/utils";

const { info } = log("import-cli");

const { readFile } = fs.promises;

export { description, she, info, comment } from "./log.js";
export { axios, nats, rabbitmq } from "./plugins.js";
export * from "./config.js";
export { checkExpected } from "@lpgroup/feathers-utils";

/**
 * Load a pdf-file into a base64 uri string.
 */
export async function loadPdf(fileName) {
  const fullPath = resolve(fileName);
  const file = await readFile(fullPath);
  const pdf = await PDFDocument.load(file);
  const uri = await pdf.saveAsBase64({ dataUri: true });
  return uri;
}

/**
 * Load a pdf-file into a base64 uri string.
 */
export async function loadPdfToBase64(fileName) {
  const fullPath = resolve(fileName);
  const file = await readFile(fullPath);
  const uri = Buffer.from(file).toString("base64");
  return `data:application/pdf;base64,${uri}`;
}

export function writeTmpJson(fileName, data) {
  const jsonData = JSON.stringify(data, null, 2);
  const tempFilePath = join(os.tmpdir(), fileName);
  info(`Write temporary settings to ${fileName}`, data);
  fs.writeFileSync(tempFilePath, jsonData);
}

export function readTmpJson(fileName) {
  const tempFilePath = join(os.tmpdir(), fileName);
  const fileData = fs.readFileSync(tempFilePath, "utf-8");
  const data = JSON.parse(fileData);
  info(`Read temporary settings from ${fileName}`, data);
  return data;
}
