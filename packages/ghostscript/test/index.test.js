import fs from "node:fs";
import { fileURLToPath } from "node:url";
import { dirname } from "node:path";
import { encode } from "js-base64";
import { optimize } from "../src/index.js";

const { readFile, writeFile, unlink } = fs.promises;
// eslint-disable-next-line no-underscore-dangle
const __dirname = dirname(fileURLToPath(import.meta.url));

describe("Ghostscript", () => {
  let input1;
  let input2;
  let input3;
  const fileIn1 = `${__dirname}/flytt_uppdrag.pdf`;
  const fileOut1 = `${__dirname}/flytt_uppdrag_optimized.pdf`;

  const fileIn2 = `${__dirname}/pdf-1.6.pdf`;
  const fileOut2 = `${__dirname}/pdf-1.6_optimized.pdf`;

  const fileIn3 = `${__dirname}/seb.pdf`;
  const fileOut3 = `${__dirname}/seb_optimized.pdf`;

  beforeAll(async () => {
    input1 = await readFile(fileIn1);
    input2 = await readFile(fileIn2);
    input3 = await readFile(fileIn3);
  });

  beforeEach(async () => {
    await Promise.all([unlink(fileOut1), unlink(fileOut2), unlink(fileOut3)]).catch(() => {});
  });

  test("Optimize golden", async () => {
    const output = await optimize({ input: input1 });
    await writeFile(fileOut1, output, "binary");
    expect(output.length).toBeGreaterThan(45000);
    expect(output.length).toBeLessThan(670000);
  });

  test("File with some errors in", async () => {
    const output = await optimize({ input: input2 });
    await writeFile(fileOut2, output, "binary");
    expect(output.length).toBeGreaterThan(45000);
    expect(output.length).toBeLessThan(670000);
  });

  test("Generats segment fault", async () => {
    const output = await optimize({ input: input3 });
    await writeFile(fileOut3, output, "binary");
    expect(output.length).toBeGreaterThan(90000);
    expect(output.length).toBeLessThan(930000);
  });

  test("Optimize error 1", async () => {
    const output = await optimize({ input: input1 });
    const base64 = `data:application/pdf;base64,${encode(output)}`;
    await expect(optimize({ input: base64 })).rejects.toThrow(Error);
  });

  // TODO: This test fails.
  // eslint-disable-next-line jest/no-commented-out-tests
  // test("Optimize error 2", async () => {
  //   return expect(optimize({ input: "asdfasdfasdfasdf" })).rejects.toEqual(
  //     new Error("Can't optimize pdf GPL Ghostscript 9.52: Unrecoverable error, exit code 1")
  //   );
  // });
});
