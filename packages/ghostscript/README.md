# ghostscript

[![npm version](https://badge.fury.io/js/%40lpgroup%2Fghostscript.svg)](https://badge.fury.io/js/%40lpgroup%2Fghostscript) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/ghostscript/badge.svg)](https://snyk.io/test/npm/@lpgroup/ghostscript)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Flatten and optimize PDF with shell command Ghostscript `gsx`.

## Install

The npm module requires shell command Ghostscript to be installed on the machine.

```sh
# OSX
brew install ghostscript

# Alpine
apk update
apk add imagemagick ghostscript-fonts ghostscript

# Debian/ubuntu
apt-get update
apt-get install ghostscript
```

Installation of the npm

```bash
npm install @lpgroup/ghostscript
```

## Example

```js
const { readfilesync, writeFile } = require("fs");
const { resolve } = require("path");
const { optimize } = require("@lpgroup/ghostscript");

const fileIn = resolve("./test/flytt_uppdrag.pdf");
const fileOut = resolve("./test/flytt_uppdrag_optimized.pdf");

const input = readfilesync(fileIn);
optimize({ input }).then((output) => {
  writeFile(fileOut, output, "binary");
});
```

## API

### `env DEBUG`

This package uses the [debug](https://www.npmjs.com/package/debug) npm. Debug messages are printed to stdout if the follwoing is set.

```bash
export DEBUG=ghostscript
```

### `optimize({options})`

Following are all values that can be used in options and the default values that are used if none is entered.

```js
optimize({
  input,
  compressFonts: true,
  embedAllFonts: true,
  subsetFonts: true,
  dpi: 300,
  colorConversionStrategy: "RGB",
});
```

## Raw shell command

Below is the shell command executed by node, if you'd like to optimize it further yourself. Credit goes to
Matt DesLauriers and the repo http://github.com/mattdesl/gsx-pdf-optimize

Removed according to https://stackoverflow.com/questions/49686527/ghostscritpt-converted-file-not-view-able-in-adobe-reader
-dPDFSETTINGS=/screen
-dCompatibilityLevel=1.5

```sh
cat test/large-file.pdf | \
gsx -sDEVICE=pdfwrite \
-dNOPAUSE \
-dQUIET \
-dBATCH \
-dSubsetFonts=true \
-dCompressFonts=true \
-dEmbedAllFonts=true \
-sOutputFile=- - > flytt_uppdrag_optimized.pdf
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
