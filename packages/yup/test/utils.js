import * as yup from "../src/index.js";

export function validate(data, schema) {
  const valid = yup.validate(data, yup.object(schema));
  /* eslint-disable-next-line jest/valid-expect */
  return expect(valid);
}
