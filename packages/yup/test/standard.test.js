/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
import * as yup from "../src/index.js";
import { validate } from "./utils.js";

describe("Standard", () => {
  test("id valid", async () => {
    const schema = { key: yup.id() };
    await validate({ key: 0 }, schema).resolves.toStrictEqual({ key: "0" });
    await validate({ key: 13 }, schema).resolves.toStrictEqual({ key: "13" });
    await validate({ key: "37" }, schema).resolves.toStrictEqual({ key: "37" });

    await validate({}, schema).resolves.not.toBe("");
  });

  test("id invalid null", async () => {
    const schema = { key: yup.id() };
    const msg = "key cannot be null";
    await validate({ key: null }, schema).rejects.toThrow(msg);
  });

  test("id invalid empty", async () => {
    const schema = { key: yup.id() };
    const msg = 'key must match the following: "/^[A-Za-z\\d](?:[A-Za-z\\d_-]*[A-Za-z\\d])?$/"';
    await validate({ key: "" }, schema).rejects.toThrow(msg);
  });

  test("invalid object transform", async () => {
    const schema = { key: yup.id() };
    await validate({ key: { key2: "value" } }, schema).rejects.toThrow(
      "key must be a `string` type, but the final value was:",
    );
    await validate({ key: ["13", "37"] }, schema).rejects.toThrow(
      "key must be a `string` type, but the final value was:",
    );
  });

  test("alias invalid", async () => {
    const schema = { key: yup.alias() };
    const invalidIds = [
      "space inbetween",
      "contains.dot",
      "dashEnd-",
      "-dashStart",
      "_undescoreStart",
      "undescoreEnd_",
      "forward/slash",
      "percemt%char",
      "comma,char",
      "exclamation!char",
      "pound#char",
      "ampersand&char",
      "at@char",
      "specials^&*()+char",
    ];
    for (let i = 0; i < invalidIds.length; i++) {
      await validate({ key: invalidIds[i] }, schema).rejects.toThrow(
        'key must match the following: "/^[A-Za-z\\d](?:[A-Za-z\\d_-]*[A-Za-z\\d])?$/"',
      );
    }
  });

  test("alias valid", async () => {
    const schema = { key: yup.alias() };
    const validIds = [
      "lowercase",
      "dash-in-between",
      "under_score_in_between",
      "UPPERCASE",
      "UPPERandlower",
      "123numberstart",
      "numberend123",
      "12345",
    ];
    for (let i = 0; i < validIds.length; i++) {
      await validate({ key: validIds[i] }, schema).resolves.toStrictEqual({
        key: validIds[i].toLowerCase(),
      });
    }
  });

  test("socialSecurityNumber valid", async () => {
    const schema = { key: yup.socialSecurityNumber() };
    const validIds = ["19761116-2970", "20030919-4363", "19581130-8443"];
    for (let i = 0; i < validIds.length; i++) {
      await validate({ key: validIds[i] }, schema).resolves.toStrictEqual({
        key: validIds[i]?.toLowerCase().replace("-", ""),
      });
    }
  });

  test("socialSecurityNumber valid null", async () => {
    const schema = { key: yup.socialSecurityNumber() };
    const validIds = [null, undefined];
    for (let i = 0; i < validIds.length; i++) {
      await validate({ key: validIds[i] }, schema).resolves.toStrictEqual({
        key: null,
      });
    }
  });

  test("socialSecurityNumber invalid", async () => {
    const schema = { key: yup.socialSecurityNumber() };
    const invalidIds = ["abc", "123", "19761116", "", "761116-2970"];
    for (let i = 0; i < invalidIds.length; i++) {
      await validate({ key: invalidIds[i] }, schema).rejects.toThrow(
        "Invalid social security number (yyyymmdd-xxxx or yyyymmddxxxx)",
      );
    }
  });

  test("amount valid", async () => {
    const schema = { key: yup.amount() };
    const validIds = ["40", "040", "40 123", 40, 40, 40123];
    const responseIds = [40, 40, 40123, 40, 40, 40123];
    for (let i = 0; i < validIds.length; i++) {
      await validate({ key: validIds[i] }, schema).resolves.toStrictEqual({
        key: responseIds[i],
      });
    }
  });

  test("amount valid null", async () => {
    const schema = { key: yup.amount().nullable() };
    const validIds = [null, ""];
    for (let i = 0; i < validIds.length; i++) {
      await validate({ key: validIds[i] }, schema).resolves.toStrictEqual({
        key: null,
      });
    }
  });

  test("amount invalid", async () => {
    const schema = { key: yup.amount() };
    const invalidIds = ["a", "a40", "40a", "40.10"];
    for (let i = 0; i < invalidIds.length; i++) {
      await validate({ key: invalidIds[i] }, schema).rejects.toThrow();
    }
  });
});
