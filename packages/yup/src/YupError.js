// Exception that convers yupError to featherserror
//
import errors from "@feathersjs/errors";

export class YupError extends errors.FeathersError {
  constructor(yupError) {
    const data = {};
    if (yupError.inner) {
      data.errors = yupError.inner.map((val) => ({
        message: val.message,
        path: val.path,
        value: val.value,
      }));
    }
    super(yupError.message, null, 400, yupError.name, data);
    if (this.inheritedStack) this.inheritedStack.push(yupError.stack);
    else this.inheritedStack = [yupError.stack];
  }
}
