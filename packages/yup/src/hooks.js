import { validate, validateKeys } from "./utils.js";
// TODO: Can probably remove options.schema
export function validReq(options = {}) {
  return async (context) => {
    if (context.method === "create" || context.method === "update") {
      context.data = await validate(
        context.data,
        options?.schema?.request || context.service.options.schema.request,
      );
    } else if (context.method === "patch") {
      // if external request validate field is included in pick.
      if (context.params.provider && options.pick) {
        validateKeys(context.data, options.pick);
      }
      // TODO: Check that only keys from requestSchema
      // are in the request json.
    }
    return context;
  };
}

export function validDB() {
  return async (context) => {
    context.data = await validate(context.data, context.service.options.schema.all);
    return context;
  };
}
