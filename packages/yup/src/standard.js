import { nanoid as nano } from "nanoid";
import { v4 as uuidv4 } from "uuid";
import * as yup from "yup";
import { isValidEmails } from "@lpgroup/utils";
import Personnummer from "personnummer";
import Organisationsnummer from "organisationsnummer";

import { arrayObject, fromDateTime, object, string, timestamp, toDateTime } from "./general.js";

const LABEL_TEXT_MIN_SIZE = 1;
const LABEL_TEXT_MAX_SIZE = 85;
const SHORT_TEXT_MAX_SIZE = 250;
const MEDIUM_TEXT_MAX_SIZE = 600;
const LONG_TEXT_MAX_SIZE = 50000;
const URL_MAX_SIZE = 1024;
const IMAGE_ALT_MAX_SIZE = 125;

// no space, url slug
const slugRegEx = /^[A-Za-z\d](?:[A-Za-z\d_-]*[A-Za-z\d])?$/;

// TODO: rename id, nanoid and uuid maybe autoUuid
export function id() {
  return yup.lazy(() => yup.string().matches(slugRegEx).default(uuidv4()));
}

// TODO: Validate accoring to below (also check internet)
// This regex: (A-Za-z0-9_-)
// and with a length of 21 charachters
export function nanoid() {
  return yup.lazy(() => yup.string().matches(slugRegEx).default(nano()));
}

export function uuid() {
  return string().matches(slugRegEx);
}

export function alias() {
  return string().matches(slugRegEx).min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE).lowercase();
}

export function title() {
  return string().min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE);
}

export function labelText() {
  return string().max(LABEL_TEXT_MAX_SIZE);
}

export function shortText() {
  return string().max(SHORT_TEXT_MAX_SIZE);
}

export function mediumText() {
  return string().max(MEDIUM_TEXT_MAX_SIZE);
}

export function longText() {
  return string().max(LONG_TEXT_MAX_SIZE);
}

export function description() {
  return object({
    short: shortText().defaultNull(),
    text: longText().defaultNull(),
  });
}

export function imageUri() {
  return yup.string();
}

export function pdfUri() {
  return yup
    .string()
    .nullable()
    .matches(/^data:application\/pdf;base64/);
}

export function referenceNumber() {
  return yup.number().integer().min(0);
}

// This can hold both positive and negative numbers.
export function amount() {
  return yup
    .number()
    .integer()
    .transform((val) => (Number.isInteger(Number.parseInt(val, 10)) ? val : null));
}

export function positiveAmount() {
  return amount().positive();
}

export function currency() {
  return yup.string().oneOf(["sek"]);
}

export function vatId() {
  return yup.string().oneOf(["vat-06", "vat-12", "vat-25"]);
}

export function bankGiro() {
  return labelText().defaultNull();
}

export function postGiro() {
  return labelText().defaultNull();
}

export function email() {
  return string().lowercase().test("is-email", isValidEmails).max(MEDIUM_TEXT_MAX_SIZE);
}

export const emails = () =>
  yup.string().test("emails", "Invalid email format", (value) => {
    if (!value) return true;
    const emailSchema = yup.string().email();
    return value.split(/[;,]/).every((emailValue) => emailSchema.isValidSync(emailValue.trim()));
  });

export function password() {
  return yup
    .string()
    .matches(
      /^(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{15,200})/,
      "Password must contain at least one number, one special character, one capital letter, and be at least 15 characters long.",
    );
}

// TODO
// const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
// yup.string().matches(phoneRegExp, "Ange ett giltigt telefonnummer"),
export function phone() {
  return string().max(LABEL_TEXT_MAX_SIZE);
}

export function tags() {
  return string().min(0).max(MEDIUM_TEXT_MAX_SIZE).lowercase();
}

export function url() {
  return string().max(URL_MAX_SIZE);
}

export function images() {
  return arrayObject({
    _id: id(),
    src: url().required(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).required(),
  });
}

export function notRequiredImage() {
  return object({
    _id: id(),
    src: url().defaultNull(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).defaultNull(),
  });
}

export function socialSecurityNumber() {
  return string()
    .defaultNull()
    .transform((val) => val?.replace("-", ""))
    .test("is-ssn", "Invalid social security number (yyyymmdd-xxxx or yyyymmddxxxx)", (val) => {
      if (val === "") return false;
      if (!val) return true; // Empty value is allowed
      if (val.length !== 12 && val.length !== 13) return false;
      const ssn = val?.replace("-", ""); // Remove dash if it exists
      return Personnummer.valid(ssn);
    });
}

export function organisationNumber() {
  return string()
    .defaultNull()
    .transform((val) => val?.replace("-", ""))
    .test(
      "is-organisation-number",
      "Invalid organisation number (xxxxxx-xxxx or xxxxxxxxxx)",
      (val) => {
        if (val === "") return false;
        if (!val) return true; // Empty value is allowed
        const orgNo = val.replace("-", ""); // Remove dash if it exists
        return Organisationsnummer.valid(orgNo);
      },
    );
}

export function address() {
  return labelText().defaultNull();
}

export function coAddress() {
  return labelText().defaultNull();
}

export function city() {
  return labelText().defaultNull();
}

export function zipcode() {
  return labelText().defaultNull();
}

export function country() {
  return labelText().defaultNull();
}

export function state() {
  return labelText().defaultNull();
}

export function addressObject() {
  return object({
    address: address(),
    coAddress: coAddress(),
    zipcode: zipcode(),
    city: city(),
    state: state(),
    country: country(),
  });
}

export function emailContact() {
  return object({
    lastName: labelText().defaultNull(),
    firstName: labelText().defaultNull(),
    email: email().defaultNull(),
    phone: phone().defaultNull(),
  });
}

export function location() {
  return object({
    latitude: yup.number().min(-80).max(80).defaultNull(),
    longitude: yup.number().min(-180).max(180).defaultNull(),
  });
}

export function publish() {
  return object({
    active: yup.boolean().default(false),
    from: fromDateTime().defaultNull(),
    to: toDateTime("from").defaultNull(),
  });
}

export function changed() {
  return object({
    by: uuid().required(),
    at: timestamp().required(),
  });
}

export function owner() {
  return object({
    user: object({
      _id: uuid().required(),
    }).required(),
    organisation: object({
      alias: alias().required(),
    }).required(),
  });
}

export function userOwner() {
  return object({
    user: object({
      _id: uuid().required(),
    }).required(),
  });
}

export function meta() {
  return yup.lazy((value) => {
    switch (typeof value) {
      case "number":
        return yup.number();
      case "string":
        return yup.string();
      case "object":
        return yup.object();
      // This set the default value to {}
      case "undefined":
        return yup.object().default({});
      default:
        return yup.mixed();
    }
  });
}

export function videoTimeReference() {
  return yup.number().integer().min(0);
}
