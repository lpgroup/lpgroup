import * as yup from "yup";
import { omit, isFunction } from "lodash-es";
import * as general from "./general.js";
import { YupError } from "./YupError.js";

const schemaToJson = (schema, value) => {
  if (!schema) return {};
  return isFunction(schema) ? schema(value) : schema;
};

const getKeys = (schema) => (value) => {
  return Object.keys(schema.resolve({ value }).fields);
};

// Used to build request and database schemas used by the services.
// This is the two main validation schemas.
// myLock - can exist on all collections
export function buildValidationSchema(requestSchema, dbSchema) {
  const requestLazySchema = yup.lazy((value) => general.object(schemaToJson(requestSchema, value)));

  const dbLazySchema = yup.lazy((value) =>
    general.object(schemaToJson(dbSchema, value)).shape({ myLock: general.string() }),
  );

  const allLazySchema = yup.lazy((value) => {
    const s1 = requestLazySchema.resolve({ value });
    const s2 = dbLazySchema.resolve({ value });
    return s1.concat(s2);
  });

  const override = (overrideSchema) => {
    const requestOverrideLazySchema = yup.lazy((value) => {
      const s1 = requestLazySchema.resolve({ value });
      const s2 = overrideSchema.request.resolve({ value });
      return s1.concat(s2);
    });

    const dbOverrideLazySchema = yup.lazy((value) => {
      const s1 = dbLazySchema.resolve({ value });
      const s2 = overrideSchema.db.resolve({ value });
      return s1.concat(s2);
    });

    const allOverrideLazySchema = yup.lazy((value) => {
      const s1 = allLazySchema.resolve({ value });
      const s2 = overrideSchema.all.resolve({ value });
      return s1.concat(s2);
    });

    return {
      request: requestOverrideLazySchema,
      db: dbOverrideLazySchema,
      all: allOverrideLazySchema,
      requestSchema: null,
      dbSchema: null,
      override: null,
      getRequestKeys: getKeys(requestOverrideLazySchema),
      getDbKeys: getKeys(dbOverrideLazySchema),
      getAllKeys: getKeys(allOverrideLazySchema),
      removeDbSchemaKeys: (mongoRecord) => {
        const keys = getKeys(dbOverrideLazySchema)(mongoRecord);
        return omit(mongoRecord, [...keys, "url"]);
      },
    };
  };

  return {
    request: requestLazySchema,
    db: dbLazySchema,
    all: allLazySchema,
    requestSchema: null,
    dbSchema: null,
    override,
    getRequestKeys: getKeys(requestLazySchema),
    getDbKeys: getKeys(dbLazySchema),
    getAllKeys: getKeys(allLazySchema),
    removeDbSchemaKeys: (mongoRecord) => {
      const keys = getKeys(dbLazySchema)(mongoRecord);
      return omit(mongoRecord, [...keys, "url"]);
    },
  };
}

export function buildValidationSchemaMulti(requestSchema, dbSchema) {
  return {
    request: general.arrayObject(requestSchema),
    db: general.array(
      general.object(requestSchema).shape(dbSchema).shape({ myLock: general.string() }),
    ),
    requestSchema,
    dbSchema,
    removeDbSchemaKeys: (mongoRecord) =>
      omit(mongoRecord, [...Object.keys(dbSchema), "myLock", "url"]),
  };
}

export function validate(data, validateSchema) {
  return validateSchema
    .validate(data, { strict: false, abortEarly: false, stripUnknown: false })
    .catch((err) => {
      throw new YupError(err);
    });
}

/**
 * Object keys in `data` must exist in array `keys`
 * Otherwise throw exception
 */
export function validateKeys(data, keys) {
  Object.keys(data).forEach((key) => {
    if (!keys.includes(key)) {
      throw new YupError({ message: `"${key}" is not allowed` });
    }
  });
}
