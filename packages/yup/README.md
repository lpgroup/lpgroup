# @lpgroup/yup

[![npm version](https://badge.fury.io/js/%40lpgroup%2Fyup.svg)](https://badge.fury.io/js/%40lpgroup%2Fyup) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/yup/badge.svg)](https://snyk.io/test/npm/@lpgroup/yup)[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Collection of yup validation and helper functions for projects based on feathersjs.

## Install

Installation of the npm

```sh
npm install @lpgroup/yup
```

## Example

```js
const yup = require("@lpgroup/yup");

const schema = { key: yup.string().required() };
```

### Hooks

```js
/* xxx.yup.js */
const cy = require("@lpgroup/yup");

const requestSchema = {
  _id: cy.uuid().required(),
  alias: cy.string().required(),
  name: cy.string().required(),
  createdAt: cy.timestamp().defaultNull(),
  changedAt: cy.timestamp().defaultNull(),
};

const dbSchema = {
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.owner(),
};

module.exports = cy.buildValidationSchema(requestSchema, dbSchema);
module.exports.requestSchema = requestSchema;
```

```js
/* xxx.service.js */
const { Xxx } = require("./xxx.class");
const hooks = require("./xxx.hooks");
const schema = require("./xxx.yup");

module.exports = (app) => {
  const options = { id: "alias", schema };
  app.use("/xxx", new Xxx(options, app));
  const service = app.service("xxx");
  service.hooks(hooks);
};
```

```js
/* xxx.hooks.js */
const { validReq } = require("@lpgroup/yup");
const { validDB } = require("@lpgroup/yup");

module.exports = {
  before: {
    create: [validReq(), changeData(), validDB()],
    update: [validReq(), changeData(), validDB()],
    patch: [validReq(), changeData(), validDB()],
  },
};
```

## API

### `general`

### `hooks`

#### `validReq(objSchema)`

```js
validReq(xxx);
```

#### `validDB(objSchema)`

```js
validDB(xxx);
```

#### `yup.array(objSchema)`

```js
const schema = { key: yup.array({ key: yup.string() }).required() };
```

## Development

When developing this NPM your can patch yup to give better error message.

```sh
npm run patch-package
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
