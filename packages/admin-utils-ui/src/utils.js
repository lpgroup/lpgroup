/* eslint-disable n/no-unsupported-features/node-builtins */

const parseDate = (date) => {
  const d = new Date(date);
  // Year
  const year = d.getFullYear();
  // Month
  let month = (d.getMonth() + 1).toString();
  month = month.length > 1 ? month : `0${month}`;
  // Day
  let day = d.getDate().toString();
  day = day.length > 1 ? day : `0${day}`;
  // hours
  let hours = d.getHours().toString();
  hours = hours.length > 1 ? hours : `0${hours}`;
  // minutes
  let minutes = d.getMinutes().toString();
  minutes = minutes.length > 1 ? minutes : `0${minutes}`;
  // seconds
  let seconds = d.getSeconds().toString();
  seconds = seconds.length > 1 ? seconds : `0${seconds}`;

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
};

const getBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

const getFileTitle = (filename) => {
  let string = filename.substr(0, filename.lastIndexOf(".")) || filename;
  string = string.replace(/[^0-9a-z]/gi, " ");
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const parseJwt = (token) => {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split("")
      .map((c) => {
        return `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`;
      })
      .join(""),
  );

  return JSON.parse(jsonPayload);
};

const setToken = (token) => {
  localStorage.setItem("accessToken", token);
};

const getToken = (tokenName) => {
  const accessToken = localStorage.getItem(tokenName);
  if (!accessToken) {
    return null;
  }
  const decodedToken = parseJwt(accessToken);
  const isExpired = new Date(decodedToken.exp * 1000) < new Date().getTime();
  if (isExpired) {
    localStorage.removeItem("accessToken");
    return null;
  }
  return accessToken;
};

const setConfigWithToken = (tokenName) => {
  const accessToken = getToken(tokenName);
  return {
    headers: {
      "Content-Type": "application/json",
      "Authorization": accessToken,
    },
  };
};

const makeSlugToTitle = (slug) =>
  slug
    .split("-")
    .map((word) => {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(" ");

const shortenUUID = (uuid) =>
  `${uuid.substr(0, uuid.indexOf("-"))}...${uuid.substr(uuid.lastIndexOf("-"), uuid.length - 1)}`;

const shortenEmail = (email) => `${email.substr(0, email.indexOf("@") + 6)}...`;

const shortenString = (string, count) => string.substr(0, count);

// https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url
const validUrl = (string) => {
  const pattern = new RegExp(
    "^(https?:\\/\\/)+" + // protocol
      "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
      "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
      "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
      "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i",
  ); // fragment locator

  return !!pattern.test(string);
};

// https://stackoverflow.com/questions/46155/whats-the-best-way-to-validate-an-email-address-in-javascript
const validEmail = (string) =>
  String(string)
    .toLocaleLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    );

const toTitleCase = (string) =>
  string.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());

const hasWhiteSpace = (string) => /\s/g.test(string);

const setStorage = (name, value) => {
  localStorage.setItem(name, JSON.stringify(value));
};

const getStorage = (name) => {
  const value = localStorage.getItem(name);
  return JSON.parse(value);
};

const removeStorage = (name) => {
  localStorage.removeItem(name);
};

export default {
  parseDate,
  getBase64,
  getFileTitle,
  hasWhiteSpace,
  parseJwt,
  setToken,
  getToken,
  setStorage,
  getStorage,
  removeStorage,
  setConfigWithToken,
  shortenUUID,
  shortenEmail,
  shortenString,
  toTitleCase,
  makeSlugToTitle,
  validEmail,
  validUrl,
};
