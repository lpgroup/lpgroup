import { Healthy } from "./healthy.class.js";
import hooks from "./healthy.hooks.js";

export default (app) => {
  const options = {
    paginate: app.get("paginate"),
  };
  app.use("/healthy", new Healthy(options, app));
  const service = app.service("healthy");
  service.hooks(hooks);
};
