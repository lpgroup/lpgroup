# @lpgroup/feathers-admin-services

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-admin-services.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-admin-services) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-admin-services/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-admin-services)
[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

General feathersjs admin services

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-admin-services
```

## Usage

In `src/servies/index.js`

```js
const { ready, healthy } = require("@lpgroup/feathers-admin-services");

app.configure(log);
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
