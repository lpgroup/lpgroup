import { log } from "../src/index.ts";

const { debug } = log("unittest");

describe("debug log", () => {
  test("Golden path", async () => {
    expect(debug("Unit Test")).toEqual(undefined);
  });
});
