import { money } from "../src/index.ts";

describe("money", () => {
  test("Golden path", async () => {
    expect(money(12)).toEqual("mo 12");
  });
});
