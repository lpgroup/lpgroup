import { formatFileName, removeCircularReferences } from "../src/index.ts";

describe("General", () => {
  test("formatFileName", async () => {
    expect(formatFileName("cow")).toEqual("cow.pdf");
    expect(formatFileName("cow moo")).toEqual("cow moo.pdf");
    expect(formatFileName("cow*moo")).toEqual("cow_moo.pdf");
    expect(formatFileName("/cow/moo")).toEqual("_cow_moo.pdf");
  });

  test("removeCircularReferences", async () => {
    const obj = [];
    obj.push([
      {
        key: "regular",
        circular: obj,
        nestedArr: [
          {
            nestedObj: { key: "another", circular: obj },
            circular: obj,
          },
        ],
        thisIsUndefined: undefined,
        thisIsNull: null,
        func: () => {
          return "cow";
        },
      },
    ]);

    expect(removeCircularReferences(obj)).toMatchObject([
      [
        {
          circular: "<circular>",
          func: "<function>",
          key: "regular",
          nestedArr: [
            { circular: "<circular>", nestedObj: { circular: "<circular>", key: "another" } },
          ],
          thisIsNull: null,
          thisIsUndefined: "<undefined>",
        },
      ],
    ]);
  });
});
