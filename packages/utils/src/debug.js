/* eslint-disable no-console */
import debugRoot from "debug";
import { isObjectLike } from "lodash-es";
import { filterDeepObject } from "./object.js";
import { removeCircularReferences } from "./general.js";

const IN_PRODUCTION = process.env.NODE_ENV === "production";

function write(output) {
  return (...args) => {
    const msg = [];
    args.forEach((arg) => {
      const cleanArg = removeCircularReferences(arg);
      if (isObjectLike(cleanArg)) {
        const keys = ["uri"];
        const s = filterDeepObject(cleanArg, keys, "<removed by debug>");
        if (IN_PRODUCTION) msg.push(JSON.stringify(s).replace(/\r?\n/g, ""));
        else msg.push(JSON.stringify(s, null, "  "));
      } else {
        msg.push(cleanArg);
      }
    });
    output(msg.join(" "));
  };
}

function info(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:info`);
  dbg.log = write(console.log);
  return dbg;
}

function warning(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:warning`);
  dbg.log = write(console.error);
  return dbg;
}

function error(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:error`);
  dbg.log = write(console.error);
  return dbg;
}

function debug(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:debug`);
  dbg.log = write(console.log);
  return dbg;
}

// Cached debug functions for used namespaces
const debugs = {};

export function log(name, namespace = "lpgroup") {
  const key = `${namespace}:${name}`;
  if (!debugs[key])
    debugs[key] = {
      debugRoot,
      info: info(name, namespace),
      warning: warning(name, namespace),
      error: error(name, namespace),
      debug: debug(name, namespace),
    };

  return debugs[key];
}

const debugValueStack = [];

export function disableDebug() {
  debugValueStack.push(debugRoot.disable());
}

export function enableDebug() {
  if (debugValueStack.length > 0) {
    const lastValue = debugValueStack.pop();
    debugRoot.enable(lastValue);
  }
}
