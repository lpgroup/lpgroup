// This is a copy of https://github.com/manishsaraan/email-validator/blob/master/index.js
// But with regex checking for invalid characters
// Thanks to:
// http://fightingforalostcause.net/misc/2006/compare-email-regex.php
// http://thedailywtf.com/Articles/Validating_Email_Addresses.aspx
// http://stackoverflow.com/questions/201323/what-is-the-best-regular-expression-for-validating-email-addresses/201378#201378
// https://en.wikipedia.org/wiki/Email_address  The format of an email address is local-part@domain, where the
// local part may be up to 64 octets long and the domain may have a maximum of 255 octets.[4]

/* eslint-disable prefer-destructuring */

const tester = /\s+/;

export function isValidEmail(email) {
  if (!email) return false;

  const emailParts = email.split("@");

  if (emailParts.length !== 2) return false;

  const account = emailParts[0];
  const address = emailParts[1];

  if (account.length > 64) return false;
  if (address.length > 255) return false;

  const domainParts = address.split(".");
  if (domainParts.some((part) => part.length > 63)) {
    return false;
  }

  return !tester.test(email);
}

// Return true if all emails are valid.
export function isValidEmails(emails) {
  if (emails) {
    return !emails.split(/[,;]/).find((email) => {
      let emailToValidate = email;

      const regExp = /[^<>]+(?=>)/g;
      const matches = email.match(regExp);
      if (matches && matches.length !== 0) emailToValidate = matches[0];

      // return true if there are any invalid adress.
      return !isValidEmail(emailToValidate);
    });
  }
  return true;
}
