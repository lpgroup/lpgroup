import { isArray, isObject, isPlainObject } from "lodash-es";

export function ObjectArray() {
  const objectArrayHandler = {
    get(target, name) {
      if (name === "target") return target;
      // eslint-disable-next-line no-param-reassign
      if (!target[name]) target[name] = [];
      return target[name];
    },
  };

  return new Proxy({}, objectArrayHandler);
}

/**
 * Replace `key` in deep object with `value`
 */
export function filterDeepObject(obj, keys, value, acc = {}) {
  if (isObject(obj)) {
    // eslint-disable-next-line no-restricted-syntax
    for (const i in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, i)) {
        if (keys.includes(i)) {
          acc[i] = value;
        } else if (isArray(obj[i])) {
          acc[i] = filterDeepObject(obj[i], keys, value, []);
        } else if (isPlainObject(obj[i])) {
          acc[i] = filterDeepObject(obj[i], keys, value, {});
        } else {
          acc[i] = obj[i];
        }
      }
    }
    return acc;
  }
  return obj;
}
