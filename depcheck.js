/* eslint-disable import/no-unresolved, no-unused-vars, import/no-extraneous-dependencies */

//
// Importing packages that are used by scripts and not used by code.
// This will disable the warning from "yarn sync-dep" and the "depcheck"
//

import ba from "@nx/js";
import bc from "typescript";
import bd from "react";
import be from "react-dom";
import bf from "jest-environment-jsdom";
import bg from "@testing-library/react";
import bj from "@vitejs/plugin-react";
import bk from "@vitest/coverage-v8";
