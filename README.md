# lpgroup

[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

We are an organisation involved in several startups. This is our main monorepo for javascript npms:s that are used in some of our projects. They are free to be used by anybody according to the MIT License. If you do, we are grateful if you contribute back with new features and bugfixes. On this gitlab organisation you might also find other repos that does not fit into this repo..

## Installation

```sh
yarn add @lpgroup/feathers-auth-service
yarn add @lpgroup/feathers-counters-service
yarn add @lpgroup/feathers-example-ng
yarn add @lpgroup/feathers-k8s-probe-services
yarn add @lpgroup/feathers-mongodb-hooks
yarn add @lpgroup/feathers-plugins
yarn add @lpgroup/feathers-utils
yarn add @lpgroup/ghostscript
yarn add @lpgroup/import-cli
yarn add @lpgroup/utils
yarn add @lpgroup/yup
yarn add @lpgroup/docs
yarn add @lpgroup/admin-ui
yarn add @lpgroup/admin-utils-ui
```

## Packages

See the packages folder for source code and readme for each package/npm.

| Rule                                                                                   | Description | Version                                                                                                                                                    | Security                                                                                                                                                       |
| -------------------------------------------------------------------------------------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [@lpgroup/feathers-example-ng](apps/feathers-example-ng/README.md)                     |             |                                                                                                                                                            |                                                                                                                                                                |
| [@lpgroup/feathers-auth-service](packages/feathers-auth-service/README.md)             |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-auth-service.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-auth-service)             | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-auth-service/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-auth-service)         |
| [@lpgroup/feathers-k8s-probe-services](packages/feathers-k8s-probe-services/README.md) |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-k8s-probe-services.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-k8s-probe-services) | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-auth-service/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-auth-service)         |
| [@lpgroup/feathers-mongodb-hooks](packages/feathers-mongodb-hooks/README.md)           |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-mongodb-hooks.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-mongodb-hooks)           | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-mongodb-hooks/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-mongodb-hooks)       |
| [@lpgroup/feathers-plugins](packages/feathers-plugins/README.md)                       |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-plugins.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-plugins)                       | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-plugins/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-plugins)                   |
| [@lpgroup/feathers-utils](packages/feathers-utils/README.md)                           |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-utils.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-utils)                           | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-utils/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-utils)                       |
| [@lpgroup/ghostscript](packages/ghostscript/README.md)                                 |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Fghostscript.svg)](https://badge.fury.io/js/%40lpgroup%2Fghostscript)                                 | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/ghostscript/badge.svg)](https://snyk.io/test/npm/@lpgroup/ghostscript)                             |
| [@lpgroup/import-cli](packages/import-cli/README.md)                                   |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Fimport-cli.svg)](https://badge.fury.io/js/%40lpgroup%2Fimport-cli)                                   | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/import-cli/badge.svg)](https://snyk.io/test/npm/@lpgroup/import-cli)                               |
| [@lpgroup/utils](packages/utils/README.md)                                             |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Futils.svg)](https://badge.fury.io/js/%40lpgroup%2Futils)                                             | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/utils/badge.svg)](https://snyk.io/test/npm/@lpgroup/utils)                                         |
| [@lpgroup/yup](packages/yup/README.md)                                                 |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Fyup.svg)](https://badge.fury.io/js/%40lpgroup%2Fyup)                                                 | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/yup/badge.svg)](https://snyk.io/test/npm/@lpgroup/yup)                                             |
| [@lpgroup/docs](/packages/docs/README.md)                                              |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Fdocs.svg)](https://badge.fury.io/js/%40lpgroup%2Fdocs)                                               | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/docs/badge.svg)](https://snyk.io/test/npm/@lpgroup/docs)                                           |
| [@lpgroup/feathers-counters-service](packages/feathers-counters-service/README.md)     |             | [![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-counters-service.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-counters-service)     | [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-counters-service/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-counters-service) |

## Start feathers example service

```sh
// Start feathers example rest api
yarn workspace @lpgroup/feathers-example-ng dev

// Import data to the rest api
yarn workspace @lpgroup/feathers-example-ng lpimport
```

## Contribute

Simple instructions how to install this in development mode.

### OSX: Install correct node version

This process can be used with most of our projects.

```sh
# Uninstall node and install nvm
brew uninstall --ignore-dependencies node
brew uninstall --force node
brew update
brew install nvm
mkdir ~/.nvm

# Add to ~/.zshrc or ~/.bash_profile
# export NVM_DIR=~/.nvm
# source $(brew --prefix nvm)/nvm.sh

# Install correct node version from .nvmrc
nvm use
nvm install

# Install packages and test code
yarn
yarn test
```

### Use your development version from another projects

In your @lpgroup monorepo run the following commands to symlink your versions into the global node_modules.

```sh
cd packages/feathers-auth-service && yarn link && cd ../..
cd packages/feathers-k8s-probe-services && yarn link && cd ../..
cd packages/feathers-mongodb-hooks && yarn link && cd ../..
cd packages/feathers-plugins && yarn link && cd ../..
cd packages/feathers-utils && yarn link && cd ../..
cd packages/ghostscript && yarn link && cd ../..
cd packages/import-cli && yarn link && cd ../..
cd packages/utils && yarn link && cd ../..
cd packages/yup && yarn link && cd ../..
cd packages/docs && yarn link && cd ../..
cd packages/feathers-counters-service && yarn link && cd ../..
cd packages/vite-plugin-replace && yarn link && cd ../..
cd packages/admin-ui && yarn link && cd ../..
cd packages/admin-utils-ui && yarn link && cd ../..
```

In your application run the following to symlink your applications node_modules to the global node_modules, that are linked to your @lpgroup monorepo.

```sh
# Check which npms your are using
ls -hal node_modules/@lpgroup

# Run yarn link for each that your are using
yarn link @lpgroup/feathers-auth-service
yarn link @lpgroup/feathers-k8s-probe-services
yarn link @lpgroup/feathers-mongodb-hooks
yarn link @lpgroup/feathers-plugins
yarn link @lpgroup/feathers-utils
yarn link @lpgroup/ghostscript
yarn link @lpgroup/import-cli
yarn link @lpgroup/utils
yarn link @lpgroup/yup
yarn link @lpgroup/docs
yarn link @lpgroup/feathers-counters-service
yarn link @lpgroup/vite-plugin-replace
yarn link @lpgroup/admin-ui
yarn link @lpgroup/admin-utils-ui

# Double check that they are correctly linked
ls -hal node_modules/@lpgroup
```

## Developer Flow

- Create a ticket on Trello
- Creates a new branch or fork for the issue
- Create a merge request on gitlab
- Merge the changes to master after a code review
- When a milestone is completed create a new release.

      yarn
      yarn test
      # Lerna changes the versions of the packages
      # Creates a tag in the remote repo,
      # pushes the changes to GitLab.
      # Gitlab ci/cd will build and push to npmjs.
      yarn set-version

## Helper commands

#### `yarn workspaces info`

Display the workspace dependency tree of your current project.

#### `yarn workspace @lpgroup/yup lint`

Run specific command in a specific workspace

#### `yarn workspace @lpgroup/yup add eslint --dev`

Adding a dev dependency to a workspace

#### `yarn add eslint --dev -W`

Add dependency to all workspaces

#### `yarn workspace @lpgroup/feathers-auth-service add @lpgroup/yup@1.1.0 -D`

Adding a local dependency to a local workspace

#### `yarn jest -- ghostscript`

Only run jest in ghostscript

#### `yarn lerna add @lpgroup/utils --scope=@lpgroup/feathers-auth-service`

Add package from monorepo to another package in the monorepo
