#!/bin/sh
find . -not -path '*/node_modules/*' -type f -name 'package.json' -print | while read file; do echo ; echo ; echo $file ; npx package-json-validator -r -w -f $file; done
