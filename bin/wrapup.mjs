#!/usr/bin/env zx --quiet
// INSTALL:
//  npm install zx --global

async function countCommits(since, until) {
  const users = (await $`git log --format='%an' | sort -u`)
    .toString()
    .split("\n")
    .filter((user) => user !== "");

  const longestUsernameLength = Math.max(...users.map((user) => user.length));

  echo("Commit Count Report");
  echo(`Since: ${since}`);
  echo(`Until: ${until}`);
  echo("------------------------");

  let totalCount = 0;
  let totalNewLines = 0;
  let totalDeletedLines = 0;
  let totalModifiedLines = 0;

  for (const user of users) {
    const count = (
      await $`git log --since="${since}" --until="${until}" --author=${user} --pretty=format:"" | wc -l`
    )
      .toString()
      .trim();
    const newLines = (
      await $`git log --since="${since}" --until="${until}" --author=${user} --pretty=tformat: --numstat | awk '{ add += $1 } END { print add }'`
    )
      .toString()
      .trim();
    const deletedLines = (
      await $`git log --since="${since}" --until="${until}" --author=${user} --pretty=tformat: --numstat | awk '{ del += $2 } END { print del }'`
    )
      .toString()
      .trim();
    const modifiedLines = (
      await $`git log --since="${since}" --until="${until}" --author=${user} --pretty=tformat: --numstat | awk '{ mod += $1 + $2 } END { print mod }'`
    )
      .toString()
      .trim();
    const newLinesCount = newLines ? Number.parseInt(newLines) : 0;
    const deletedLinesCount = deletedLines ? Number.parseInt(deletedLines) : 0;
    const modifiedLinesCount = modifiedLines ? Number.parseInt(modifiedLines) : 0;
    const padding = " ".repeat(longestUsernameLength - user.length + 2);
    echo(
      `${user}${padding}${count} commits, ${newLinesCount} new lines, ${deletedLinesCount} deleted lines, ${modifiedLinesCount} modified lines`,
    );
    totalCount += Number.parseInt(count);
    totalNewLines += newLinesCount;
    totalDeletedLines += deletedLinesCount;
    totalModifiedLines += modifiedLinesCount;
  }

  const padding = " ".repeat(longestUsernameLength - "TOTAL".length + 2);
  echo(
    `TOTAL${padding}${totalCount} commits, ${totalNewLines} new lines, ${totalDeletedLines} deleted lines, ${totalModifiedLines} modified lines`,
  );
}

async function calculateCommitsPerYear(sinceYear) {
  const currentYear = new Date().getFullYear();
  const years = Array.from(
    { length: currentYear - sinceYear + 1 },
    (v, k) => k + Number.parseInt(sinceYear),
  );

  for (const year of years) {
    const since = `${year}-01-01`;
    const until = `${year}-12-31`;

    const count = (
      await $`git log --since="${since}" --until="${until}" --pretty=format:"" | wc -l`
    )
      .toString()
      .trim();
    echo(`Year: ${year} (${count} commits)`);
  }
}

await calculateCommitsPerYear("2018");
echo("");
await countCommits("2024-01-01", "2024-12-31");
