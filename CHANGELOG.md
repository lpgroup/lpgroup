## 1.9.3 (2025-01-27)

### 🩹 Fixes

- **nats:** remove unnecessary queue option from subscription
- **nats:** sub before pub

### ❤️  Thank You

- Arlukin

## 1.9.2 (2025-01-24)

### 🩹 Fixes

- update Yarn to version 4.6.0
- yarn.lock
- **wrapup:** enhance commit counting with new, deleted, and modified lines
- **yup:** remove length validation for organisation number

### ❤️  Thank You

- Arlukin

## 1.9.1 (2024-11-14)

### 🩹 Fixes

- another force set-version

### ❤️  Thank You

- Arlukin

## 1.9.0 (2024-11-14)

### 🚀 Features

- node v22.11

### 🩹 Fixes

- sync-dep
- use .yarn/relases
- biome lint and formatting

### ❤️  Thank You

- Arlukin

## 1.8.5 (2024-11-12)

### 🩹 Fixes

- **feathers-utils:** cache don't delete on correct id

### ❤️  Thank You

- Arlukin

## 1.8.4 (2024-11-10)

### 🩹 Fixes

- **axios:** retryHandler works for more errors than 502

### ❤️  Thank You

- Arlukin

## 1.8.3 (2024-10-22)

### 🩹 Fixes

- **saml:** email need to be lowerCase

### ❤️  Thank You

- Martin Palmér

## 1.8.2 (2024-10-22)

### 🩹 Fixes

- force set-version

### ❤️  Thank You

- Arlukin

## 1.8.1 (2024-10-22)

### 🩹 Fixes

- **axios:** handle httpsAgent on instance
- **import-cli:** allow self signed cert

### ❤️  Thank You

- Arlukin

## 1.8.0 (2024-10-18)

### 🚀 Features

- do not output newline in production logs.

### 🩹 Fixes

- sync-dep
- **sys:** saml test
- **yup:** yuperror restore

### ❤️  Thank You

- Arlukin
- Martin Palmér

## 1.7.3 (2024-10-10)

### 🩹 Fixes

- sync-dep
- sync-dep

### ❤️  Thank You

- Arlukin

## 1.7.2 (2024-09-30)


### 🩹 Fixes

- yarn.lock
- sync-dep

### ❤️  Thank You

- Arlukin

## 1.7.1 (2024-09-30)


### 🩹 Fixes

- need a new version

### ❤️  Thank You

- Martin Palmér

## 1.7.0 (2024-09-30)


### 🚀 Features

- **import:** add all config to import test context

### 🩹 Fixes

- display inner error in yupError.
- **errors:** display inner error without stringfy message.
- **nats:** subExpect errors should be resolved

### ❤️  Thank You

- Arlukin
- Martin Palmér

## 1.6.0 (2024-09-18)


### 🚀 Features

- add axios.delete, depcricate remove

### 🩹 Fixes

- **nats:** an array result in nc.req is a valid response.

### ❤️  Thank You

- Arlukin

## 1.5.9 (2024-08-30)


### 🩹 Fixes

- **axios:** options.headers doesnt always exist

### ❤️  Thank You

- Arlukin

## 1.5.8 (2024-08-30)


### 🩹 Fixes

- **axios:** options.headers doesnt always exist

### ❤️  Thank You

- Arlukin

## 1.5.7 (2024-08-28)


### 🩹 Fixes

- **feathers-utils:** axios methods should take headers

### ❤️  Thank You

- Arlukin

## 1.5.6 (2024-08-23)


### 🩹 Fixes

- set-query didnt handle searchOperators

### ❤️  Thank You

- Arlukin

## 1.5.5 (2024-08-22)


### 🩹 Fixes

- CounterStatistics bug after eslint fix

### ❤️  Thank You

- Arlukin

## 1.5.4 (2024-08-22)


### 🩹 Fixes

- force gitlab build

### ❤️  Thank You

- Arlukin

## 1.5.3 (2024-08-22)


### 🩹 Fixes

- lint

### ❤️  Thank You

- Arlukin

## 1.5.2 (2024-08-22)


### 🩹 Fixes

- yarn upgrade
- move feathers-example-ng and lpgroup-docs to /apps
- rename lpgroup-docs to example-docs
- format:pkg
- **ghostscript:** add test for segmentation fault pdf
- **set-query:** update hook to convert to boolean

### ❤️  Thank You

- Arlukin
- Martin Palmér

## 1.5.1 (2024-05-08)


### 🩹 Fixes

- yarn.lock

### ❤️  Thank You

- Arlukin

## 1.5.0 (2024-05-08)


### 🚀 Features

- add knip

### 🩹 Fixes

- lint
- eslint v9 for js files BC: not working for md, mdx, json,
- eslint for ts
- eslint v9, js, jsx, ts, tsx, json
- sync-dep
- eslint9 reconfig
- knip
- sync-dep
- lint
- **feathers-auth-service:** missing ?.
- **nats:** remove queue from broadcast

### ❤️  Thank You

- Arlukin
- Casiano Fernandez
- srhqmp

## 1.4.6 (2024-04-11)


### 🩹 Fixes

- trigger rebuild

### ❤️  Thank You

- Arlukin

## 1.4.5 (2024-04-11)


### 🩹 Fixes

- trigger rebuild

### ❤️  Thank You

- Arlukin

## 1.4.4 (2024-04-11)


### 🩹 Fixes

- trigger rebuild

### ❤️  Thank You

- Arlukin

## 1.4.3 (2024-04-11)


### 🩹 Fixes

- trigger rebuild

### ❤️  Thank You

- Arlukin

## 1.4.2 (2024-04-11)


### 🩹 Fixes

- trigger build

### ❤️  Thank You

- Arlukin

## 1.4.1 (2024-04-11)


### 🩹 Fixes

- yarn.lock
- format
- remove lpimport waitUntilExpected
- lpimport with expectedFile
- lpimport
- remove unused run-rs
- sync-dep
- **admin-ui:** add the ability to customize button text
- **admin-ui:** custom form save button text
- **admin-ui:** AddButton test
- **feathers-plugins:** nats.sub should take optionsOverride
- **feathers-utils:** add more merge-data test
- **import-cli:** handle nats.version
- **import-cli:** ingoreKeyCompare will be save as <ignored>
- **import-cli:** export checkExpected
- **import-cli:** error logs
- **import-cli:** nats expectedFile
- **import-cli): errorHandler supports expectedFile fix(import-cli): writeRequest fix(import-cli:** ignoreKeyCompare on arrays
- **merge-data:** Replace simple array with empty array
- **sanitizedObject:** pass replaceWith in recursive call

### ❤️  Thank You

- Arlukin
- Casiano Fernandez
- Daniel Lindh
- Martin Palmér
- srhqmp

## 1.4.0 (2024-03-21)


### 🚀 Features

- husky will run 'yarn run test' before 'git push'
- configured codecov.io coverage report
- added badges to all readmes
- added @lpgroup/feathers-k8s-probe-services /healthy and /ready k8s probes
- added --extension to import-cli
- axios plugin wait for server
- added deep-sort-object-array util function
- added node-execute-directory utils function
- axios errorhandler checkExpected
- added axios ignoreError option
- feathers-utils added merge-data and deep-sort-diff
- new service /users/:userId/upgrade
- added user features
- internalParams added clearSession options
- feathers-auth added getPermissions()
- added /organisations/xx/grants
- POST/organisation/xx/grants ignores dub err
- deepSortDiff added noArraySort
- added /organisations/xx/grants
- POST/organisation/xx/grants ignores dub err
- deepSortDiff added noArraySort
- protocol middleware handles websockets
- added express error handler
- added /users/xxx/organisations
- added @lpgroup/utils skeleton
- updated monorepo readme
- auth can set anonymous user when not logged in
- adding typescript and ts eslint.
- upgraded dep
- added feathers-example-ng
- yup.email() handles null
- added maxRPS to .import.json
- validReq({ pick: ["tags", "references"] })
- added link.sh, easier to get symlinks in dev projects
- plugin nats.pub take options override
- Add multible privilegies with grants
- added onboarding to feathers-auth-service
- added changelog when set-version
- add working scripts for windows
- add props that may be use for mene entry
- fix props entry diplay on mobile and browser view
- add other parsed h1 elements in the menu bar
- improve collapse and expand only the clicked item in the menu
- push #{url} to url-bar this will help in referencing
- add login and logout ui
- feathers-example-ng is now working
- add api-key stragegy
- implement apiKey expiration
- add feathers-counter-service
- add auth on feathers-counter-service
- implement autoIncrement on counter-service
- apiKey cascade delete when user is deleted
- add dBschema added, changed & owner
- throw error if user does not exist when creating api-key
- add visitCounter to status route
- add description field in counters
- add a countResult and return as params in a hook
- export createOrUpdateCounter function
- import.json config in import folders.
- RabbitMQ reconnect
- cache
- added vite-pliugin-replace
- add vite-plugin-replace
- add organisations/xx/users
- add organisation-leave
- counter hook has more options
- counter can build alias from context.data
- added requestLimitter middleware
- add request-limit hook to authentication
- headers middleware
- cache find result for privilegies and organisations.
- initialize docs react18
- add debug.warning function
- oauth for fb, google and github
- register oAuthStrategy to user
- add admin-ui and admin-utils-ui packages
- password in .import.js can be an env var
- organiastion-grants allows all privileges note: this might be a security issue
- add /admin/log
- add timer hook to profile endpoints
- node 18
- sync-dep
- **admin-ui:** add CounterStatistics component
- **admin-ui:** setup vitest
- **admin-ui:** convert jsx to typescript
- **auth:** added ipNumber, userAgent, isOnline and status to users field
- **auth:** add verified to users field
- **auth:** add profileImageUrl to users
- **example-ng:** added "articles" endpoint as an example
- **feathers-example:** added mock 502 endpoint
- **feathers-example-ng:** update config
- **feathers-utils:** extracted error-handler hook
- **feathers-utils:** setIsWebCrawler hook
- **import-cli:** add write and readTmpJson
- **nats:** add req function for syncronized nats call
- **utils:** add executeSequentially and executeInParallel to run an async function in parallell or seriall on an array
- **yup:** implement password policy
- **yup:** add emails validator that is , or ; separated

### 🩹 Fixes

- correct homepage link in package.json This link is used by npmjs
- jest config is in root
- lint and prettier config is in root
- updated coverage badge in readme
- more accurate text in all readme:s
- more accurate text in all readme:s
- prettier missed lerna.json locally
- prettier missed lerna.json locally
- prettier missed lerna.json locally
- lerna and prettier used diffrent conf format
- updated package.json format
- feathers-k8s-probe-services improved readme
- eslint/prettier use quoteProps: consistent
- comments and debug print
- added clone-data hook
- updated yup schema organisation
- default values for .import.json
- import ignoreKeyCompare used on deleted keys
- removed feathers-mongodb Official feathers-mongodb now supports session
- upgraded dependencies
- added auth hook.
- Added defaultnull on organisations.yup
- added permissions to organisations
- cache axios authToken and increase maxRPS to 200 for import
- disallowEnvironment didn't work
- replaced runAllImports with executeDirectory in import-cli Will now execute all files in a directory sync and all sub folders async
- yup lazyObject failed on none objects
- improved axios debug message
- check-permission crashed when no user was authenticated
- dep upgrade
- breakout feathers-plugins from feathers-utils
- executeDirectory correct console output
- checkExpected uses deep-sort-diff
- eslint problems
- refactoring users
- added eslint rules
- rename validation functions validateRequest-> validReq validateDatabase-> validDB
- buildItemHook run all rows async
- lpgroup.yup exports standard yup
- populate-privileges uses buildItemHook
- eslint updated class-methods-use-this Disable eslint check for feathersjs override functions.
- updated permissions
- mongodb session didn't handle exception
- axios invalid accessToken on reused instance
- minior changes
- memoize bug in axios
- load/patch-data throw NotFound
- using hook discard instead of protect This to elimnate the need of context.dispatch
- updated permissions
- using owner.organisation.alias instead of _id
- merged blake accounts into users
- eslint problem
- added users.active and yup requiredments
- transactions throw exceptions when session doesn't exist
- eslint
- improved exception debuging
- updated packages
- yup.string had invalid exception when validating invalid value []
- eslint
- internalParams clearSession didn't clear correctly
- updated packages
- yup test
- test node-execute-directory didn't work in ci/cd
- disabled error-session 405 debug message
- eslint
- user.privileges handles no params
- reauthenticate.
- updated dependencies
- added husky exe permissions
- added /users/xx/permissions
- cascade delete of grants from organisations and users
- set owner.organisation from changed hook
- axios lisents to options.errorHandlerWithException
- checkpermission invalid default param
- grants remove didnt work
- refactoring sleep
- nats.waitForSubExpect didn't unsubscribe
- request-log handles axios errors
- checkExpected ignores kind "A"
- launch.json using yarn instead of npm
- added unittest for deep-sort-diff
- yarn lint
- axios will forward GeneralErrors
- added /users/xx/permissions
- cascade delete of grants from organisations and users
- set owner.organisation from changed hook
- axios lisents to options.errorHandlerWithException
- checkpermission invalid default param
- grants remove didnt work
- refactoring sleep
- nats.waitForSubExpect didn't unsubscribe
- request-log handles axios errors
- checkExpected ignores kind "A"
- launch.json using yarn instead of npm
- added unittest for deep-sort-diff
- yarn lint
- axios will forward GeneralErrors
- changed mainDomain to webDomain
- users url in result was invalid
- better logging in axios and
- organisation.url should be on alias
- bug in url hook
- esm upgrade part 1
- esm upgrade part 2 - yup
- esm upgrade part 3: utils
- esm upgrade part 4: ghostscript
- esm upgrade part 6: feathers-utils
- esm upgrade part 7: config files
- esm upgrade part8: feathers-mongodb-hooks
- esm upgrade part 9: feathers-plugins
- esm upgrade part 10: config files
- esm upgrade part 11: import-cli
- esm upgrade part 12: feathers-k8s-probe not tested
- esm upgrade part 13: config files
- esm upgrade part 14: feathers-auth-service
- removed invalid feathers-auth-service-example
- yarn prettier
- esm upgrade part 15: remove .js on import
- esm upgrade part 16: yarn lint
- working with example project
- dep upgrade
- yarn eslint
- disabled test for node-execute-directory
- yarn prettier
- upgrade gitlab node ersion
- ci/cd deploy only runs jest prettier and eslint gets corrupted with set-version
- ci/cd needs to build before jest
- yarn prattier on package.json
- updated eslint
- better error log
- axios errorhandler pass FJS GenerlaError correct
- reusing uuid too much in tests
- improved axios errorHandlerExceptions
- Didn't use the * permission correct in or statements
- dep update
- yarn pretterier
- sync-dep
- lerna version
- trim all query parameters in setQuery hook
- dynamic import support for windows
- yarn sync-dep
- yarn prettier
- axios errorhandler didn't handle undefined response
- node-execute-directory, better error output
- ghostscript added stderr in pdf
- yarn sync-dep
- added k8Probe default export
- removed $in from checkpermission when not needed
- pdfUri can be nullable
- added apiDomain to uri hook
- sync-dep + prettier
- small refactoring
- added feathers-auth-service/utils getCachedUser
- nats validate can handel no yup scema
- yarn prettier
- yarn syncep
- url hook should not use webDomain
- removed ghostscript typescript config
- added comments
- yarn sync-dep
- yarn prettier
- yarn prettier
- yarn prettier
- ci set-version push tags
- /authentication only keeps valid keys in result
- plugin rabbitmq handle exception
- updated yup
- added loadPdfToBase64 without converstion
- added catch messages to gcs
- return more user info when login.
- yarn sync-dep
- possible to use organisation and user service without overide
- yarn sync-dep
- prettier, lint
- yarn sync-dep
- yarn.lock
- handle crash when mdx file has no h1 tags
- handle codeblock crash when no metastring provided
- handle crash on generic codeblock with no language
- order of h1 and h2 in sidebar
- axios debug didn't work after upgrade
- rename street to address
- .defaultNull() on address
- styling of 'Attribute' stop working due to @mdx-js/mdx update
- warning on validate_dom nesting with Typhography
- convert .at query to number, be able to search on dates.
- improve axios error handling
- spacing typhography in attribute
- feathers-sync should be done with nats prefix
- request-log returns error when url is invalid
- url hook uses bulidItemHook
- yarn sync-dep
- adapt to mongodb 4
- use github/arlukin/feathers-mongodb
- api-key ._id should be id, added api-key.name
- add spacing below <attibutes>
- add docs to link.sh
- added public status service
- wait for nats to connect before ready
- added ObjectArray
- upgrade node 14.16.0 -> 14.18.1
- startTransaction in nested session
- add vscode.env
- description in counter hook
- sidebar scrollIntoView on reload
- handle burst request on counters updateOrCreateCount
- handle counter response on updateOrCreate counters
- updateOrCreateCounter
- sync-dep
- change upCount to lpgroup transaction code.
- sync-dep
- sync-dep
- sync-dep
- mongodb
- counter bunch (create+patch)
- yup validation, remove all unlimitted string
- setting debug to stdout
- added lpgroup debug
- eslint
- remove debug from package.json
- added debug test
- filter jwt expired log message
- add info, debug and error log functions
- remove sync info log Co-authored-by: Martin Palmér <martin.palmer.develop@gmail.com>
- debug should handle class objects
- ignore dublicate key errors created by axios
- eslint
- remove serviceName
- add absoluteSubject and replyTo functionality on nats
- change debug to log
- set to standard git config files
- run prettier on tsx, mdx, html, css
- cache works on emits between pods. added ServiceCache for users and organisations
- ignore eslint for vite-plugin-replacer
- test deploy vite-plugin-replace
- update package.json
- try to deplay
- .gitignore ignores to much
- sort package.json and make vite-plugin public
- vite-plugin-replace don't work on jsx
- cache is now working with alias
- sync-dep with react 18
- change vite-plugin-mdx ->@mdx-js/rollup
- make debug.info print objects properly
- The cache invoker in CreateCache is to slow
- import doesn't ignore New items properly
- remove cache don't return result
- organisation-join should only emit not join chanel
- syn-dep
- join and leave services need create function
- update join test
- make counter work as before hook
- dublicate organisation permissions problem
- restore correct code
- buildValidationSchema handle lazy on root in schema
- eslint
- merge-data
- sync-dep
- merge-data
- sync-dep
- invalid import of requestLimit:w
- remove request-limit limitRequest middleware is enough
- middleware handle websocket
- yarn.lock and eslint
- return ipv6 from remoteAdress middleware
- organisation-grants shouldn´t use userId as _id
- eslint
- add formatFileName to utils
- yarn sync-dep
- downgrade "feathers-hooks-common" due to cjs problem and upgrade amqlib
- one more downgrade of feathers-hooks-common
- grants userId and id should be separate fields
- grants userId and id should be separate fields
- sort all package.json with sort-package-json
- missing dependencies
- add lpgroup-docs (harper copy)
- the MDXProvider bug
- node version 14.19.3
- use ServiceCache for Privileges
- eslint
- eslint config change wanr on extraneous and remove LPDocs test.
- typo
- rabbitmq reconnect.
- use import.js instead of import.json
- legit close of rabbitmq
- sync-dep
- sync-dep
- lint
- upgrade feathers-hooks-common
- downgrade to nats 1.4.12
- lpimport
- id tests
- deleted user deletes apiKey
- cascade delete Promise.all should have a return
- organisation grants cascade function when user is deleted
- remove shape unused function
- slug validation should not contains space
- if no source array exists return in mergeData
- if no source array exists return in mergeData
- consitent use of stringify
- typescript config for @lpgroup/docs
- jsonsort packags/*/
- typos and error handling in import-cli
- sync-dep
- filterDeepObject handles array of keys
- axios errorHandler should always log errors
- Remove the lowercase on axios urls.
- updated axios error handler
- docs sample
- yup.email() handles ,; separeated emails
- remove one of the close routines
- yup email need to handle åäö
- yup email need to handle åäö
- axios writes to many errors in lpimport
- docs sample
- lerna build with dependencies
- yarn.lock
- yarn lint
- export oauth service
- sync-dep
- delete ignored file
- change ports from 80xx to 82xx
- convert default import to named import
- dependency warnings and some dep upgrades
- docs yarn dev
- apply config file docs
- readme
- correct colors import
- update feathers-example import
- refactor and force standard-organisation on org-grants
- update error message on request-log
- sync-dep
- rewrite counters to skip transaction
- eslint
- more eslint
- all uri in debug log will be stripped
- add feathers-counters-service to link
- add log for all nats error
- add timer for ghostscript
- make ghostscript faster Optimization of one file took 10s, now 4s.
- change user.status to user.onlineStatus
- rename status to onlineStatus
- prevent user to escalate privilege
- eslint
- docs max-width
- upgrade to vite 3
- organisations grant test
- remove organisation grants collection
- refactor organisations grants
- cascade function if superUser deletes organisation
- decrease oauth github scope
- add incomeCounter to nats.subExpect
- increase size of yup.url to 1024
- sync-dep
- Make UserAvatar work with facebook images
- export config from import-cli
- axios add error output for Internal timeout
- subExpect should use configOptions
- typo in import-cli
- spelling privilegies -> privileges
- jest tests should write debug messages
- remove organisation transaction error
- upgrade to vite 4.0
- sync-dep
- oauth circular structure to json
- lint
- yup socialSecurityNumber should be 13 chars
- yarn.lock
- sync-dep
- axios retryHandler for 502 response, postPut method
- ipRemoteAddres
- sync-dev
- middleware
- upgrade nx
- use organisationNumber as yup validation
- yup.amount transfer empty string to null
- update node engines
- update minor npm-check-updates
- udpate package in docs
- import-cli requires assertion
- DEP0151: Main index lookup and extension searching
- upgrade @mui
- yarn.lock
- sync-dep
- remov styled component that crashed build
- prettier
- depricated nx setting
- remove child packages yarn.lock
- invalid entry point for lpgroup/docs
- downgrade mongodb
- unittest of yup.amount
- docs styles
- docs style table
- docs, move menu button to the left
- docs invert logo only when darkmode
- discardWhenNoPriv should give superUser priv
- sync-dep
- upgrade node v20
- upgrade to node 20
- import error
- update passwords
- import
- remove extensionless
- sync-dep
- apiKey as long text validation
- add pre validation to apiKey as password
- lpimport test
- changed hook, user and date
- log exemption for invalid auth
- yarn
- replace jest with vitest
- replace all Greta Harper references
- sync-dep
- lpimport
- sync-dep
- sync-dep
- eslint
- mongodb breaking changes
- clean up scripts in package.json
- import in sync with code
- lint and cleanup
- remove babel
- upgrade node 20.9.0 > 20.11.1
- remove unused request on nats publish
- upgrade axios to 1.6.8
- authentication object, featureGroupId
- allow old password validation
- increase min pass len to 15
- lpimport expectedFile
- expectedFile adjustments
- lint
- sync-dep
- build all typescript projects
- test after typescript changes
- sync-dep
- **admin-ui:** fetch button
- **admin-ui:** use theme styles
- **admin-ui:** additional props for SearchBar
- **admin-ui:** import mui colors
- **admin-ui:** update components
- **admin-ui:** update TextInput
- **admin-ui:** allow SearchBar default value
- **admin-ui:** logout color should be black
- **admin-ui:** upgrade apexcharts and react-apexcharts
- **admin-ui:** upgrade apexcharts and react-apexcharts
- **admin-ui:** use recharts in CounterStatistics
- **admin-ui:** CounterStatistics - mobile responsive
- **admin-ui:** AddedChanged export and AddButton id
- **admin-ui:** remove proptypes of some components
- **admin-ui:** add PropTypes to UserAvatar
- **admin-ui:** button colors
- **admin-ui:** lint
- **admin-ui:** lint
- **admin-ui:** Button props
- **admin-ui:** remove tooltip when button is disabled
- **admin-ui:** prop-types import
- **admin-ui:** remove prop-types and fix errors
- **admin-ui:** add defaultValue to SelectInput
- **admin-utils-ui:** remove fixed token name
- **api-keys:** validate apiKey before hash
- **api-keys:** validReq and validDb before apiKey hash
- **aut-service:** organisations-grants handle changes in patchData
- **auth:** prevent removing of self grant; to ensure that there will be always a user left in an organisation
- **auth:** migrate isUserOnline
- **counters:** alias should not have dot character
- **docs:** upgrade to mui (partially)
- **docs:** tweak, codeblock, columns, autlogin
- **docs:** tweak, codeblock, columns, autlogin
- **docs:** url on login
- **docs:** React 18
- **docs:** body fontStyle
- **docs:** add optional CSSProperties sxTopBar
- **docs:** test sxTopBar
- **docs:** css
- **example:** import REST middleware
- **example-ng:** lpimport
- **example-ng:** lpimport
- **feathers-auth-service:** optimize /authentication
- **feathers-auth-service:** add privileges to user data
- **feathers-auth-service:** revert setUserData need channel fuctionality
- **feathers-auth-service:** allow-apiKey invalid exception thrown
- **feathers-auth-service:** don't validate the password when login Only do that when setting password Otherwise current passwords needs to be changed for all users
- **feathers-auth-service:** wrong path to utils in package.json
- **feathers-example:** remove limitRequest middleware.
- **feathers-example-ng:** SIGTERM handling
- **feathers-mongodb-hooks:** refactor, add debug, lockData handles _id
- **feathers-mongodb-hooks:** lockDocument need to be run in serial
- **feathers-plugins:** close mongodb correct
- **feathers-plugins:** better nats error handling
- **feathers-plugins:** gcs import
- **feathers-plugins:** add a callback to subExpect
- **feathers-utils:** merge-data shouldn't add _id object
- **feathers-utils:** add threshold for timer hook
- **feathers-utils:** correct url calculation on hooks
- **feathers-utils:** error message for executeDirectory
- **feathers-utils:** add log to remoteAdress and change ip order
- **feathers-utils:** add docs for remoteAdress
- **feathers-utils:** retry request when got 502 response
- **feathers-utils:** apply exponential delay on axios 502 response
- **feathers-utils:** invalid async in retryHandler
- **feathers-utils:** split middleware into socket and rest functions
- **feathers-utils:** middleware socket refactor
- **feathers-utils:** middleware socket refactor
- **feathers-utils:** remote adress
- **feathers-utils:** middleware socket refactor
- **import-cli:** she, info, comment, decription should print objects
- **import-cli:** lpimport support expectedFile, testName,  writeRequest, writeDiff,
- **import-cli:** writeRequest, added writeOriginal
- **import-cli:** expectedFile, dont print response and diff in the terminal
- **jest:** turn of some verbosity
- **lpgroup-docs:** sample
- **lpgroup-docs:** sample
- **mongodb:** retry when error "WriteConflict"
- **nats:** waitForSubExpect returns result
- **nats:** subExpect returns sid and result
- **nats:** imporved error handling for req
- **nats:** replyTo becomes "null" string.
- **nats:** delete nc.subscribes after all are handled
- **nats:** added reqSub and broadcast methods.
- **nats:** order of params and need to be string in request.
- **oauth:** error message spelling
- **organisations-users:** omit some values instead of picking, to return more values from users.
- **plugin-replace:** map as null
- **removeCircularReferences:** can't handle all types of structures
- **seed:** update example seed
- **test:** badgateway lpimport
- **test:** remove debug
- **utils:** modify merge-data
- **utils:** remove circular ref in debug output
- **utils:** invalid extension for ts files
- **vite-plugin-replace:** eslint
- **vite-plugin-replace:** remove unused deps
- **vite-plugin-replace:** cant set to empty string
- **yup:** added buildValidationSchemaLazy and to deprecate buildValidationSchema
- **yup:** added videoTimeReference function
- **yup:** restrict alias to special chars including dot and space
- **yup:** validate SSN correct
- **yup:** validate organisationNumber correct

### 🔥 Performance

- fix slow performance issue and re-renders

### ❤️  Thank You

- Arlukin
- Casiano Fernandez
- casianojrfernandez
- Daniel Lindh
- John Carlo Cunanan
- Martin Palmér
- srhqmp