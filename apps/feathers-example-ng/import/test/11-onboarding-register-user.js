import { axios, info } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    await ax.login().then(async (o) => {
      await o.remove(`/users/${config.userId}`, { ignoreError: true });
    });

    info("fail to board using simplepassword");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: config.userId,
        email: config.user,
        password: "simplepassword",
        firstName: "Reverend",
        lastName: "Custer",
        phone: "073-6265449",
      },
      { expectedFile: "user-with-invalid-password-fail" },
    );

    await ax.post(
      "/onboarding/register-user",
      {
        _id: config.userId,
        email: config.user,
        password: config.password,
        firstName: "Reverend",
        lastName: "Custer",
        phone: "073-6265449",
      },
      { expectedFile: "valid-user" },
    );
  });
};
