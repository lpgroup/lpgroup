import { axios, description, she, comment } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Sample Articles CRUD");
    const standardUser = await ax.login(config.user, config.password);

    she("can create");
    const { _id: articleId } = await standardUser.post(
      "/articles",
      { type: "NEWS", tags: [{ _id: "3", text: "triangle" }] },
      { expectedFile: "articles" },
    );

    she("can get");
    await standardUser.get(`/articles/${articleId}`, { expectedFile: "articles" });

    she("can patch primitive field");
    await standardUser.patch(
      `/articles/${articleId}`,
      { content: "Hello World" },
      { expectedFile: "articles-primitive" },
    );

    she("can patch an array field with an new _id");
    comment("If array contains elements with alias or _id merge that object.");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "4", text: "quadrilateral" }] },
      {
        expectedFile: "articles-array-new-id",
      },
    );

    she("can patch an array field with same _id");
    comment("if item contains _id or alias with other field will update the item");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "3", text: "isoceles" }] },
      {
        expectedFile: "articles-array-same-id",
      },
    );

    she("can patch an array with _id only");
    comment("If item only contains _id or alias remove that item.");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "4" }] },
      {
        expectedFile: "articles-array-id-only",
      },
    );

    she("can patch an array without _id");
    comment("If one of the item in the array do not contain _id or alias, error");
    await standardUser.post("/admin/log", { message: "THIS SHOULD WRITE AN ERROR MESSAGE" });
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ text: "pentagon" }] },
      {
        expectedFile: "articles-array-without-id",
      },
    );
  });
};
