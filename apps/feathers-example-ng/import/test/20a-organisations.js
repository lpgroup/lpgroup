import { axios, she, description } from "@lpgroup/import-cli";
import config from "./config.js";

const { userId } = config;
export const extraUserId = "daeb025c-480e-42bf-95fc-55ca7bd271fe";

export default async () => {
  return axios().then(async (ax) => {
    description("Test organisation, permissions and grants");

    await ax.login().then(async (o) => {
      await o.remove(`/users/${userId}`, { ignoreError: true });
      await o.remove(`/users/${extraUserId}`, { ignoreError: true });
      await o.remove("/organisations/carcosa", { ignoreError: true });
    });

    she("create user");
    await ax.post("/onboarding/register-user", {
      _id: userId,
      email: config.user,
      password: config.password,
      firstName: "Reverend",
      lastName: "Custer",
      phone: "073-6265449",
    });

    await ax.post("/onboarding/register-user", {
      _id: extraUserId,
      email: "extra-onboarding@carcosa.se",
      password: config.password,
      firstName: "Molly",
      lastName: "Millions",
      phone: "073-6265449",
    });

    await ax.login(config.user, config.password, true).then(async (o) => {
      she("create organisation, with new user");
      // userId will get standard_organisation access when creating the organisation
      await o.post(
        "/organisations",
        {
          alias: "carcosa",
          name: "Carcosa R&D",
          title: "Carcosa R&D",
        },
        { expectedFile: "organisations" },
      );

      await o.get("/organisations");
      await o.get("/organisations/carcosa", { expectedFile: "organisations-carcosa" });

      await o.get(`/users/${userId}`, {
        expectedFile: "users",
      });

      await o.get("/organisations/cafe-not-exist", {
        expectedFile: "organisations-not-exist-fail",
      });

      await o.patch(
        "/organisations/carcosa",
        { phone: "123-456" },
        { expectedFile: "organisations" },
      );

      await o.put(
        "/organisations/carcosa",
        {
          alias: "carcosa",
          name: "Carcosa R&D",
          title: "Carcosa R&D",
        },
        { expectedFile: "organisations" },
      );

      await o.get(`/users/${userId}/permissions`, {
        expectedFile: "users-permissions",
      });

      she("grant organisation permissions to extra user");
      await o.post("/organisations/carcosa/grants", { userId: extraUserId });
      await o.get(`/users/${extraUserId}/permissions`, {
        expectedFile: "users-permissions-w-grants",
      });
      await o.get(`/users/${extraUserId}/organisations`, {
        expectedFile: "users-organisations",
      });

      await o.get("/organisations/carcosa/users", {
        expectedFile: "organisations-users",
        noArraySort: true,
      });

      const grants = await o.get("/organisations/carcosa/grants", {
        expectedFile: "organisations-grants",
        noArraySort: true,
      });

      const grant = grants.data.filter((v) => v.userId === extraUserId)[0];
      await o.remove(`/organisations/carcosa/grants/${grant.userId}`);
      await o.get("/organisations/carcosa/users", {
        expectedFile: "organisations-users-after-ungrant",
      });
    });
  });
};
