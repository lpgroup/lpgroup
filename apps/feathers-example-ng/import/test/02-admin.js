import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    await ax.login().then(async (o) => {
      await o.post(
        "/admin/log",
        { message: "default log" },
        {
          expectedFile: "admin-log",
        },
      );
      await o.post(
        "/admin/log",
        { type: "info", message: "info log" },
        { expectedFile: "admin-log" },
      );
      await o.post(
        "/admin/log",
        { type: "error", message: "error log" },
        { expectedFile: "admin-log" },
      );
      await o.post(
        "/admin/log",
        { type: "warning", message: "warning log" },
        { expectedFile: "admin-log" },
      );
      await o.post(
        "/admin/log",
        { type: "debug", message: "debug log" },
        { expectedFile: "admin-log" },
      );
      await o.post(
        "/admin/log",
        { type: "invalid", message: "debug log" },
        {
          expectedFile: "admin-log-err",
        },
      );
    });
  });
};
