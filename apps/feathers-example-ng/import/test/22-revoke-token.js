/*
 dependencies: 11-onboarding-register-user.js
*/
import { axios, she, info, description, comment } from "@lpgroup/import-cli";
import onboardingUser from "./config.js";

export default async () => {
  description("Revoke token with 'delete' method on /authentication");
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      info("get Id");
      const user = await o.get(`/users?email=${onboardingUser.user}`);
      userId = user.data[0]?._id;
      if (!userId) throw new Error("User doesnt exist");
    });

    await ax.login(onboardingUser.user, onboardingUser.password, true).then(async (o) => {
      she("can perform get with valid accessToken ");
      await o.get(`/users/${userId}`, {
        expectedFile: "w-valid-accessToken",
      });

      she("can revoke the token");
      await o.remove("/authentication", {
        expectedFile: "revoke-token",
        noArraySort: true,
        ignoreKeyCompare: ["accessToken", "_id", "ipNumber"],
      });

      comment("Token is not revoked");
      she("can still perform get after revoke");
      await o.get(`/users/${userId}`, {
        // expected: {
        //   name: "NotAuthenticated",
        //   message: "Token revoked",
        //   code: 401,
        //   className: "not-authenticated",
        //   errors: {},
        // },
        expectedFile: "req-after-revoke-token",
      });
    });
  });
};
