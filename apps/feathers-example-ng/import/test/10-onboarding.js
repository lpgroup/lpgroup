import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    ax.get("/onboarding", {
      expectedFile: "onboarding",
    });
  });
};
