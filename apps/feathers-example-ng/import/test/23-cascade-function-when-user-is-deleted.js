/*
 dependencies: 20-organisations.js
*/
import { axios, description, she, info } from "@lpgroup/import-cli";
import config from "./config.js";

const user = {
  email: "usercascadedelete@carcosa.se",
  password: "TestingTesting@321",
  firstName: "Usercascade",
  lastName: "Delete",
  phone: "073-7654321",
};

export default async () => {
  return axios().then(async (ax) => {
    description("Simulate cascade functions when user is deleted");

    const superUser = await ax.login();

    info("cleanup");
    const targetUser = await superUser.get(`/users?email=${user.email}`);
    const id = targetUser.data[0] ? targetUser.data[0]._id : undefined;

    if (id) {
      info(`cleanup user, ${id}`);
      await superUser.remove(`/users/${id}`);
    }

    she("creates a user");
    const userObject = await ax.post("/onboarding/register-user", user, {
      expectedFile: "onboarding-register-user",
    });

    she("creates apiKey #1");
    const expires = new Date().setDate(new Date().getDate() + 1);
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "KingKongDingDongPlingPlong@123", expires },
      {
        expectedFile: "apiKey1",
        ignoreKeyCompare: ["expires", "userId", "_id", "changed", "added", "url"],
      },
    );

    she("creates apiKey #2");
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "QueenDreamBeanIsVeryMean@123", expires },
      {
        expectedFile: "apiKey2",
        ignoreKeyCompare: ["expires", "userId", "_id", "changed", "added", "url"],
      },
    );

    she("creates apiKey #3");
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "JackTheSlackIsNotWhack@123", expires },
      {
        expectedFile: "apiKey3",
        ignoreKeyCompare: ["expires", "userId", "_id", "changed", "added", "url"],
      },
    );

    she("checks all created apiKeys");
    await superUser.get(`/users/${userObject._id}/api-keys`, {
      expectedFile: "all-apiKeys",
      ignoreKeyCompare: ["data"],
    });

    she("organisationCreator grants to an organisation");
    const organisationCreator = await ax.login(config.user, config.password);
    const userGrants = await organisationCreator.post(`/organisations/${config.alias}/grants`, {
      userId: userObject._id,
    });

    she(`checks the grant to ${config.alias} organisations`);
    await superUser.get(`/organisations/${config.alias}/grants/${userObject._id}`, {
      expectedFile: "organisation-check-grants",
      ignoreKeyCompare: ["userId"],
    });

    she("deletes the user to see cascade effect");
    await superUser.remove(`/users/${userObject._id}`, {
      expectedFile: "user-cascade-delete-api-key",
    });

    she("should not return all apiKeys created.");
    await superUser.get(`/users/${userObject._id}/api-keys`, {
      expectedFile: "users-after-cascade-delete-api-key",
      ignoreKeyCompare: ["data"],
    });

    she(`checks the grant to ${config.alias} organisations, should not be found`);
    await superUser.get(`/organisations/${config.alias}/grants/${userGrants._id}`, {
      expectedFile: "org-grants-after-cascade-delete-api-key",
    });
  });
};
