import { axios, info, description } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    description("Invalid authentication");

    info("invalid device login");
    await ax.post(
      "/authentication",
      {
        password: null,
        email: null,
        _id: "random-id",
        strategy: "device",
      },
      { expectedFile: "invalid-device-creds-fail" },
    );

    info("invalid local login");
    await ax.post(
      "/authentication",
      {
        password: "thisistest",
        email: "test@gmail.com",
        strategy: "local",
      },
      { expectedFile: "invalid-local-creds-fail" },
    );
  });
};
