import { axios, description, info, she } from "@lpgroup/import-cli";

const userCreds = {
  userId: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
  email: "uniqueuser@carcosa.se",
  password: "Very-Unique-Password@123",
};

export default async () => {
  return axios().then(async (ax) => {
    description("Preform user escalate privilege");
    const superUser = await ax.login();

    info("cleanup");
    await superUser.remove(`/users/${userCreds.userId}`, { ignoreError: true });

    info("should gets error if privileges field is provided in create");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: userCreds.userId,
        email: userCreds.email,
        password: userCreds.password,
        firstName: "User",
        lastName: "Pentester",
        phone: "073-6265449",
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expectedFile: "onboarding-w-privileges",
      },
    );

    she("creates as regular user");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: userCreds.userId,
        email: userCreds.email,
        password: userCreds.password,
        firstName: "User",
        lastName: "Pentester",
        phone: "073-6265449",
      },
      {
        expectedFile: "onboarding-std",
      },
    );

    she("logins with email and password");
    const regularUser = await ax.login(userCreds.email, userCreds.password);

    she("able to get admin/test as superUser");
    await superUser.get("/admin", { expectedFile: "superuser-to-admin" });

    info("should fail to get admin as regular user");
    await regularUser.get("/admin", {
      expectedFile: "std-to-adminEndpoint-fail",
    });

    await regularUser.get(`/users/${userCreds.userId}`, { expectedFile: "users-self" });

    info("should fail to escalate privilege using patch");
    await regularUser.patch(
      `/users/${userCreds.userId}`,
      {
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expectedFile: "escalate-privilege-fail",
      },
    );

    she("can do basic patch");
    await regularUser.patch(
      `/users/${userCreds.userId}`,
      { firstName: "Cloe" },
      { expectedFile: "users-self" },
    );

    info("should fail perform admin privilege");
    await regularUser.get("/admin", { expectedFile: "std-user-to-adminEndpoint-fail" });

    info("should fail to escalate privilege using put");
    await regularUser.put(
      `/users/${userCreds.userId}`,
      {
        _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
        type: "user",
        profileImageUrl: null,
        onlineStatus: "ONLINE",
        oAuthStrategy: null,
        isOnline: false,
        userAgent: "axios/1.6.8",
        ipNumber: "::ffff:127.0.0.1",
        verified: false,
        phone: "073-6265449",
        lastName: "Pentester",
        firstName: "User",
        email: "uniqueuser@carcosa.se",
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expectedFile: "std-user-escalate-fail",
      },
    );

    info("should fail perform admin privilege");
    await regularUser.get("/admin", {
      expectedFile: "std-user-to-adminEndpoint-fail",
    });
  });
};
