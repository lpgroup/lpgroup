/*
  dependencies: load.js, 11-onboarding-resgister-user.js
*/

import { axios, description, info, she } from "@lpgroup/import-cli";
import config from "./config.js";

const apiKeyId = "36a5c18f-2422-46db-9868-cc469bb72054";

export default async () => {
  return axios().then(async (ax) => {
    description("Creating API-key");

    let userId;
    await ax.login().then(async (o) => {
      info("get ids");
      const user = await o.get(`/users?email=${config.user}`);
      userId = user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");

      info("cleanup API-key");
      await o.remove(`/users/${userId}/api-keys/${apiKeyId}`, { ignoreError: true });
    });

    info("cannot create when user is not authenticated");

    await ax.post(
      `/users/${userId}/api-keys`,
      { apiKey: config.apiKey },
      { expectedFile: "anonymous-user-fail" },
    );

    info("cannot create when Login but not authorized");

    await ax.login(config.user, config.password).then(async (o) => {
      await o.post(
        `/users/${userId}/api-keys`,
        { apiKey: config.apiKey },
        { expectedFile: "not-permitted-fail" },
      );
    });

    she("can create as super user (JWT)");
    const current = new Date();
    await ax.login().then(async (o) => {
      await o.post(
        `/users/${userId}/api-keys`,
        {
          _id: apiKeyId,
          name: "test key",
          apiKey: config.apiKey,
          // one day
          expires: current.setDate(current.getDate() + 1),
        },
        {
          expectedFile: "permitted-user",
          ignoreKeyCompare: ["expires", "changed", "added"],
        },
      );
      info("throws a General Error if apiKey is already created");

      await o.post(
        `/users/${userId}/api-keys`,
        {
          name: "test key",
          apiKey: config.apiKey,
        },
        {
          expectedFile: "duplicated-apiKey-fail",
          // message contains generated apiKey
          ignoreKeyCompare: ["message"],
        },
      );
    });

    await ax.useApiKey("wrongapikey").then(async (o) => {
      info("throws NotAuthenticated error if apiKey is provided but not found in database");

      await o.post(
        `/users/${userId}/api-keys`,
        { apiKey: config.apiKey },
        { expectedFile: "request-w-wrongkey-fail" },
      );
    });

    await ax.useApiKey(config.apiKey).then(async (o) => {
      she("can get user with apiKey");

      await o.get(`/users/${userId}`, {
        expectedFile: "request-w-rightkey",
      });
    });
  });
};
