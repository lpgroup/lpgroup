import { axios, description, info } from "@lpgroup/import-cli";
import afetch from "axios";

export default async () => {
  return axios().then(async () => {
    description("Rate Limit bucketSize of 80, expires @ 2sec");
    const arr = Array.from({ length: 90 });
    const bunchRequest = () =>
      arr.forEach((o, i) => {
        afetch
          .get("http://localhost:8290/")
          .then(() => {
            process.stdout.write(`${i + 1}. `);
          })
          .catch((e) => info(`${i + 1}. ${e.message}`));
      });

    bunchRequest();

    // wait for 3 sec and request again

    await new Promise((suc) => {
      setTimeout(suc, 3000);
    }).then(() => bunchRequest());

    await new Promise((s) => {
      setTimeout(s, 1000);
    });
  });
};
