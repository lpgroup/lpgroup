/*
To peform all Standar User permission granted by default:

      "users": userGUP,
      "users/:userId/permissions": userGFUP,
      "users/:userId/organisations": userF,
      "organisations": userC,
      "organisations/:organisationAlias/join": allC,
      "organisations/:organisationAlias/users": ["find"],
*/

// dependencies: 20-organizations.js

import { axios, she, comment, info, description } from "@lpgroup/import-cli";

export const carcosaOrganization = {
  alias: "carcosa",
  name: "Carcosa R&D",
  title: "Carcosa R&D",
};

export const standardUser1 = {
  _id: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
  email: "user1@carcosa.se",
  password: "The-Green-Lantern@123",
  firstName: "First user1",
  lastName: "Last user1",
  phone: "111-8888888",
};

export const standardUser2 = {
  _id: "c5ae7467-bcce-4f23-b55b-4a71719cd0d2",
  email: "user2@carcosa.se",
  password: "The-Green-Lantern@123",
  firstName: "First user1",
  lastName: "Last user1",
  phone: "111-8888888",
};

export default async () => {
  return axios().then(async (ax) => {
    description("Cleanup");
    await ax.login().then(async (o) => {
      await o.remove(`/users/${standardUser1._id}`, { ignoreError: true });
      await o.remove(`/users/${standardUser2._id}`, { ignoreError: true });
      await o.remove("/organisations/broker2", { ignoreError: true });
    });

    description("Standard User performs regular actions");

    she("can create own credentials");
    await ax.post("/onboarding/register-user", standardUser1, {
      expectedFile: "onboarding-register-user",
    });

    await ax.post("/onboarding/register-user", standardUser2, {
      expectedFile: "onboarding-register-user2",
    });

    she("can login using local credentials and get accessToken");
    await ax.post(
      "/authentication",
      {
        strategy: "local",
        email: standardUser1.email,
        password: standardUser1.password,
      },
      {
        expectedFile: "auth-accesstoken",
        ignoreKeyCompare: ["accessToken", "privileges", "ipNumber"],
      },
    );

    await ax.login(standardUser1.email, standardUser1.password).then(async (o) => {
      she("can perform `get` to own id");

      await o.get(`/users/${standardUser1._id}`, {
        expectedFile: "user-own-id",
        ignoreKeyCompare: ["changed", "added", "ipNumber"],
      });

      info("can not perform `get` to other users(onboarding) id");
      await o.get(`/users/${standardUser2._id}`, {
        expectedFile: "user-attempt-other-id-fail",
        ignoreKeyCompare: ["changed", "added", "ipNumber"],
      });

      she("can perform `patch` to own id");
      await o.patch(
        `/users/${standardUser1._id}`,
        {
          phone: "234523-123",
        },
        { expectedFile: "users-self" },
      );

      she("can join existing organisations");
      // TODO: We shouldn't be able to join an organisation we don't have access to.
      await o.post(`/organisations/${carcosaOrganization.alias}/join`, null, {
        expectedFile: "todo-organisation-join",
      });

      she("can perform `put` to own id");
      // await o.put(
      //   `/users/${standardUser1._id}`,
      //   {
      //     _id: standardUser1._id,
      //     email: standardUser1.email,
      //     firstName: standardUser1.firstName,
      //     lastName: standardUser1.lastName,
      //     phone: standardUser1.phone,
      //     password: standardUser1.password,
      //   },
      //   {
      //     expected: {
      //       _id: standardUser1._id,
      //       owner: {
      //         user: {
      //           _id: standardUser1._id,
      //         },
      //       },
      //       changed: {
      //         by: standardUser1._id,
      //         at: 1634024376887,
      //       },
      //       added: {
      //         by: "superuser",
      //         at: 1634024372662,
      //       },
      //       active: true,
      //       type: "device",
      //       phone: standardUser1.phone,
      //       lastName: standardUser1.lastName,
      //       firstName: standardUser1.firstName,
      //       email: standardUser1.email,
      //       url: `http://localhost:8290/users/${standardUser1._id}`,
      //     },
      //   }
      // );

      she("can create organisation");
      const orgData = {
        alias: "broker2",
        name: "Broker2 LTD.",
        title: "Broker2 LTD.",
        organisationNumber: "202100-5489",
      };

      await o.post("/organisations", orgData, { expectedFile: "organisations-new" });

      she("can find owned organization");

      await o.get(`/organisations?alias=${orgData.alias}`, {
        expectedFile: "organisations-find-owned",
        ignoreKeyCompare: ["data"],
      });

      she("can find others organisation");

      await o.get("/organisations?alias=carcosa", {
        expectedFile: "organisation-find-other",
        ignoreKeyCompare: ["data"],
      });

      she("can get owned organsation");

      await o.get(`/organisations/${orgData.alias}`, {
        expectedFile: "organisations-owned",
      });

      she("can get others organisation");

      await o.get("/organisations/carcosa", {
        expectedFile: "organisations-others",
      });

      she("can patch owned organisation");

      await o.patch(
        `/organisations/${orgData.alias}`,
        {
          phone: "3223311-444",
        },
        {
          expectedFile: "organisations-owned",
        },
      );

      info("can not patch others organisation");

      await o.patch(
        "/organisations/carcosa",
        {
          phone: "3223311-444",
        },
        { expectedFile: "organisations-others-fail" },
      );

      // she("can put owned organisation")

      // await o.put(`/organisations/${orgData.alias}`, orgData, { expected: orgExpected });

      info("can not put others organisation");

      await o.put("/organisations/carcosa", orgData, {
        expectedFile: "organisations-others-fail",
      });

      info("can not remove owned organisation");

      await o.remove(`/organisations/${orgData.alias}`, {
        expectedFile: "organisations-owned-fail",
      });

      info("can not remove others organisation");

      await o.remove("/organisations/carcosa", {
        expectedFile: "organisations-others-fail",
      });

      she("can find owned permissions");

      await o.get(`/users/${standardUser1._id}/permissions`, {
        expectedFile: "users-permission-own",
      });

      she("can find others permissions");
      comment(
        "The userId in users/:userId/permissions is unused it will always return owned permission",
      );

      // TODO: Shouldnt have access to this
      await o.get(`/users/${standardUser2._id}/permissions`, {
        expectedFile: "todo-should-have-no-access",
      });

      she("can find owned organisation users/:userId/organisations (list) ");

      await o.get(`/users/${standardUser1._id}/organisations`, {
        expectedFile: "users-organisations-owned",
      });

      she("can find others organisation users/:userId/organisations (list) ");

      await o.get(`/users/${standardUser2._id}/organisations`, {
        expectedFile: "users-organisations-others",
      });

      await o.post("/organisations/carcosa/grants", { userId: standardUser2._id });

      await o.get(`/users/${standardUser2._id}/organisations`, {
        expectedFile: "users-organisations-others-w-grants",
      });
    });
  });
};
