import { axios, checkExpected, she, ignoreKeyCompare, description } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Testing checkExpected function");

    const standardUser = await ax.login(config.user, config.password);
    const articleInstance = await standardUser.post("/articles", {
      type: "NEWS",
      tags: [{ _id: "3", text: "triangle" }],
    });

    she("checks with expected args");
    checkExpected(articleInstance, {
      expected: {
        _id: "757cce7b-1808-43c8-a89f-e9d7405a17d2",
        tags: [{ _id: "3", text: "triangle" }],
        content: null,
        type: "NEWS",
        slug: null,
        url: "http://localhost:8290/articles/757cce7b-1808-43c8-a89f-e9d7405a17d2",
      },
      ignoreKeyCompare: ["_id", "url"],
    });

    she("checks with expectedFile args");
    checkExpected(articleInstance, {
      expectedFile: "article-test",
      ignoreKeyCompare: ignoreKeyCompare(),
    });
  });
};
