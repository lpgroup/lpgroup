import { axios, description, she, info } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Grant service should not remove grant to self");
    const orgOwner = await ax.login(config.user, config.password);

    she("orgOwner can get the organisation");
    await orgOwner.get(`/organisations/${config.alias}`, { expectedFile: "orgOwner-organisation" });

    info("cleanup, remove grants for extraUser");
    const grant = await orgOwner.get(`/organisations/carcosa/grants/${config.extraUserId}`, {
      expected: {
        name: "NotFound",
        message: `No record found for id '${config.extraUserId}'`,
        code: 404,
        className: "not-found",
        errors: {},
      },
    });
    if (grant) {
      await orgOwner.remove(`/organisations/carcosa/grants/${grant.userId}`, {});
    }

    she("orgOwner can give grant to extraUser");
    const extraUserGrant = await orgOwner.post(
      `/organisations/${config.alias}/grants`,
      { userId: config.extraUserId },
      {
        expectedFile: "organisations-give-grants",
      },
    );

    info("cannot remove self grant");
    const otherUser = await ax.login("extra-onboarding@carcosa.se", config.password);
    await otherUser.remove(`/organisations/carcosa/grants/${extraUserGrant.userId}`, {
      expectedFile: "organisations-ungrant-self-fail",
    });

    she("orgOwner can remove grant to extraUser");
    await orgOwner.remove(`/organisations/carcosa/grants/${extraUserGrant.userId}`, {
      expectedFile: "organisations-ungrant-others",
    });
  });
};
