import { axios, info, description, she } from "@lpgroup/import-cli";

const now = new Date();
const group = "status-accessed-monthly_visit";
const period = `${now.getFullYear()}-${`0${now.getMonth() + 1}`.slice(-2)}`;
const _id = `${group}-${period}`;

export default async () => {
  return axios().then(async (ax) => {
    const counterExpected = {
      _id,
      group,
      period,
      value: 0,
      description: "Total number of calls this month",
      changed: {
        by: "superuser",
        at: 1636853224396,
      },
      added: {
        by: "superuser",
        at: 1636853224396,
      },
      owner: {
        user: {
          _id: "superuser",
        },
      },
      url: "http://localhost:8290/counters/status-visits-2021-11",
    };

    description("Simulate visit counters on /status?user=admin");
    let { accessedTotal, accessedTotalUser, accessedMonthlyVisit } =
      await ax.get("/status?user=admin");
    she(
      `can 'get' /status?user=admin, accessedTotal: ${accessedTotal} accessedTotalUser: ${accessedTotalUser} accessedMonthlyVisit: ${accessedMonthlyVisit}`,
    );

    accessedTotal += 1;
    accessedTotalUser += 1;
    accessedMonthlyVisit += 1;

    await ax.get("/status?user=admin", {
      expected: {
        accessedTotal,
        accessedTotalUser,
        accessedMonthlyVisit,
        application: "@lpgroup/feathers-example-ng",
        version: "1.3.34",
        env: "development",
        node_env: "NA",
        node_config_env: "NA",
        started: 1636717611329,
        time: 1636718061206,
        uptimeSeconds: 449.877,
        online: true,
        nats: {
          uri: "nats://localhost:4222",
          queue: "feathers-example-ng-dev",
          // versions: {},
        },
      },
      ignoreKeyCompare: ["uptimeSeconds", "time", "started", "version"],
    });

    description("Access thru counters endpoint");
    info("login as admin");
    await ax.login().then(async (o) => {
      she(`can 'get' counter value`);
      await o.get(`/counters/${_id}`, {
        expected: { ...counterExpected, value: accessedMonthlyVisit },
      });

      she("can 'patch' counter");
      await o.patch(
        `/counters/${_id}`,
        { value: 5 },
        {
          expected: { ...counterExpected, value: 5 },
        },
      );

      she("can 'put' counter");
      await o.put(
        `/counters/${_id}`,
        {
          _id,
          group,
          period,
          value: 200,
          description: "Total number of calls this month",
        },
        { expected: { ...counterExpected, value: 200 } },
      );

      she("can 'post' counter");
      await o.post(
        "/counters",
        {
          _id: "test-create",
          group,
          period,
          value: 200,
          description: "Test counter",
        },
        {
          expected: {
            ...counterExpected,
            _id: "test-create",
            description: "Test counter",
            value: 200,
          },
        },
      );

      she("can 'delete' counter");
      o.remove("/counters/test-create");
    });

    description("Perform bunch 1 request to simulate counter session");

    const arr = Array.from({ length: 200 });
    arr.forEach(() => {
      ax.get("/status?user=admin").then((res) => {
        info(`VisitCount:${res.accessedTotalUser}`);
      });
    });
  });
};
