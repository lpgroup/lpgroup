import { axios, info } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      const user = await o.get("/users?email=onboarding@carcosa.se");
      userId = user.data[0]?._id;
      if (!userId) throw new Error("User doesnt exist");
    });

    await ax.login(config.user, config.password).then(async (o) => {
      await o.get("/users/not-exist-123", { expectedFile: "id-not-exist-fail" });

      // No permission to super user
      await o.get("/users/620373d9-aba2-4566-9757-baa646dfacd1", {
        expectedFile: "no-permission-fail",
      });

      await o.patch(
        "/users/not-exist-123",
        { phone: "not-exist-123" },
        { expectedFile: "id-not-exist-fail" },
      );

      await o.patch(
        "/users/620373d9-aba2-4566-9757-baa646dfacd1",
        { phone: "no-permission-for-this-super-user" },
        { expectedFile: "no-permission-fail" },
      );

      await o.patch(`/users/${userId}`, { phone: "123 456 789" }, { expectedFile: "std-user" });

      info("fail to patch easy password");
      await o.patch(
        `/users/${userId}`,
        { password: "easypassword" },
        { expectedFile: "easy-password-fail" },
      );

      await o.get(`/users/${userId}`, { expectedFile: "std-user" });

      await o.get("/organisations", { expectedFile: "std-user-org" });
    });
  });
};
