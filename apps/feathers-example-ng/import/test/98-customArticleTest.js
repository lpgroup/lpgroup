import { axios, description, she } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Test import-cli, expectedFile");
    const standardUser = await ax.login(config.user, config.password);

    she("can create new article");
    await standardUser.post(
      "/articles",
      { type: "NEWS", tags: [{ _id: "3", text: "triangle" }] },
      {
        expectedFile: "sampleCreateArticle",
        writeDiff: true,
        writeOriginal: true,
        writeRequest: true,
      },
    );
  });
};
