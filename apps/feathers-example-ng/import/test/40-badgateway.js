import { axios, description, she } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    description("Bad gateway");

    she("should wait longer when server is overloaded, 'find'");
    await ax.get("/badgateway", {
      expectedFile: "badgateway-find-fail",
    });

    she("should wait longer when server is overloaded, 'get'");
    await ax.get("/badgateway/502", {
      expectedFile: "badgateway-fail",
    });

    she("should wait longer when server is overloaded, 'post'");
    await ax.post(
      "/badgateway",
      { samplePayload: "isEmpty" },
      {
        expectedFile: "badgateway-fail",
      },
    );

    she("should wait longer when server is overloaded, 'put'");
    await ax.put(
      "/badgateway/1",
      { samplePayload: "isEmpty" },
      {
        expectedFile: "badgateway-fail",
      },
    );

    she("should wait longer when server is overloaded, 'patch'");
    await ax.patch(
      "/badgateway/1",
      { samplePayload: "isEmpty" },
      {
        expectedFile: "badgateway-fail",
      },
    );
    she("should wait longer when server is overloaded, 'postPut'");
    await ax.postPut(
      "/badgateway",
      "_id",
      { _id: "uniqueId", samplePayload: "isEmpty" },
      { expectedFile: "badgateway" },
    );

    she("should wait longer when server is overloaded, 'remove'");
    await ax.remove("/badgateway/1", { expectedFile: "badgateway" });

    await ax.get("/");
  });
};
