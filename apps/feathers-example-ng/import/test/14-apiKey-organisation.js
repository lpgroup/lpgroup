/*
  dependencies: 13.create-api-key.js
*/

import { axios, description, info, she, comment } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Standard User actions using apiKey as credentials to /organisation route");

    let userId;
    await ax.login().then(async (o) => {
      info("get ids");
      const user = await o.get(`/users?email=${config.user}`);
      userId = user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");

      info("cleanup API-key");
      await o.remove("/organisations/broker1", { ignoreError: true });
    });

    await ax.useApiKey(config.apiKey).then(async (o) => {
      const data = {
        alias: "broker1",
        name: "Broker1 LTD.",
        title: "Broker1 LTD.",
        organisationNumber: "202100-5489",
      };

      she("performs create with correct key");

      await o.post("/organisations", data, { expectedFile: "validkey" });

      she("performs find with correct key");

      await o.get("/organisations?alias=broker1", {
        expectedFile: "validkey-find",
        ignoreKeyCompare: ["data"],
      });

      she("performs get with correct key");

      await o.get("/organisations/broker1", { expectedFile: "validkey" });

      she("performs patch with correct key");

      await o.patch("/organisations/broker1", { phone: "333-444" }, { expectedFile: "validkey" });

      she("performs put with correct key");

      await o.put("/organisations/broker1", data, { expectedFile: "validkey" });

      info("cannot performs remove with correct key");

      await o.remove("/organisations/broker1", {
        expectedFile: "validkey-but-disallowed-fail",
      });

      comment("Note: find and get methods are 'public' for /organisation");
    });
  });
};
