import { axios } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    await ax.login().then(async (o) => {
      await o.post("/admin/truncate/database");
    });
  });
};
