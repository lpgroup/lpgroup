import { UsersBase } from "@lpgroup/feathers-auth-service";

export class Users extends UsersBase {
  constructor(options, app) {
    super(options);
    this.app = app;
  }
}
