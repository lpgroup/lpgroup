import { usersService } from "@lpgroup/feathers-auth-service";
import { Users } from "./users.class.js";

export default (app) => {
  return usersService(app, { Users });
};
