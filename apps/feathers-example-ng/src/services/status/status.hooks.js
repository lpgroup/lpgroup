// Application hooks that run for every service
import { counter } from "@lpgroup/feathers-counters-service";
import { disallow } from "feathers-hooks-common";

export default {
  before: {
    all: [],
    find: [],
    get: [disallow()],
    create: [disallow()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [
      counter("status-accessed-total", null, "accessedTotal", 1, "Total number of calls"),
      counter(
        "status-accessed-monthly_visit",
        "yyyy-MM",
        "accessedMonthlyVisit",
        1,
        "Total number of calls this month",
      ),
      counter(
        "status-accessed-total-{user}",
        null,
        "accessedTotalUser",
        1,
        "Total number of calls for specific user",
      ),
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
