/* eslint-disable no-unused-vars */
export class Status {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
    this.started = Date.now();
  }

  async find(params) {
    const time = Date.now();
    const uptime = Math.abs(this.started - time) / 1000;
    return {
      application: this.app.get("application"),
      version: this.app.get("version"),
      env: this.app.get("env"),
      environment: this.app.get("environment"),
      node_env: process.env.NODE_ENV || "NA",
      node_config_env: process.env.NODE_CONFIG_ENV || "NA",
      started: this.started,
      time,
      uptimeSeconds: uptime,
      online: true,
      nats: {
        uri: this.app.get("nats").uri,
        queue: this.app.get("nats").queue,
        versions: this.app.get("nats").versions,
      },
    };
  }
}
