import { auth } from "@lpgroup/feathers-auth-service/hooks";
import { disallowEnvironment } from "@lpgroup/feathers-utils/hooks";
import { disallow } from "feathers-hooks-common";

export default {
  before: {
    all: [auth(), disallowEnvironment({ NODE_CONFIG_ENV: "prod" })],
    find: [disallow()],
    get: [disallow()],
    create: [],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
