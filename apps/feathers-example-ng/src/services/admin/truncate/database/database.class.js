/* eslint-disable no-unused-vars */
import { onPluginReady } from "@lpgroup/feathers-plugins";
import load from "../../../../load.js";

export class TruncateDatabase {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async create(data, params) {
    const { database } = await onPluginReady("mongodb");
    const collectionNames = (await database.listCollections().toArray())
      .filter((v) => v.type === "collection")
      .map((v) => ({ name: v.name }));

    await Promise.all(
      Object.values(collectionNames).map(async (collection) => {
        await database.collection(collection.name).deleteMany();
      }),
    );
    await load(this.app);
    return collectionNames;
  }
}
