import { Admin } from "./admin.class.js";
import hooks from "./admin.hooks.js";

export default (app) => {
  const options = {};
  app.use("/admin", new Admin(options, app));
  const service = app.service("admin");
  service.hooks(hooks);
};
