import { Root } from "./root.class.js";
import hooks from "./root.hooks.js";

export default (app) => {
  const options = {};
  app.use("/", new Root(options, app));
  const service = app.service("");
  service.hooks(hooks);
};
