export class Root {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    const host = `${params.protocol}://${params.headers.host}`;

    return {
      authentication_url: `${host}/authentication`,
      countries_url: `${host}/countries`,
      organisation_url: `${host}/organisations`,
      users_url: `${host}/users`,
      vats_url: `${host}/vats`,
    };
  }
}
