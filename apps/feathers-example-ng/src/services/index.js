// LP Group services
import { log } from "@lpgroup/feathers-admin-services";
import {
  authentication,
  organisationsGrants,
  organisationsJoin,
  organisationsUsers,
  privileges,
  onboarding,
  registerUser,
  usersPermissions,
  usersOrganisations,
  usersApiKeys,
} from "@lpgroup/feathers-auth-service";

import { counters } from "@lpgroup/feathers-counters-service";

// k8s probes
import k8ProbeServices from "@lpgroup/feathers-k8s-probe-services";

// Admin
import adminTruncateDatabase from "./admin/truncate/database/database.service.js";
import admin from "./admin/admin.service.js";

// Users
import users from "./users/users/users.service.js";

// Organisations
import organisations from "./organisations/organisations/organisations.service.js";

// Root
import root from "./root/root.service.js";
import status from "./status/status.service.js";

// Sample
import articles from "./articles/articles.service.js";
import badgateway from "./badgateway/badgateway.service.js";

export default (app) => {
  // root-endpoints (Admin, Root etc.) needs to be configured after subendpoints (status).
  // This to let app.use override GET pathes.

  // Admin services
  app.configure(log);

  // Authorization
  app.configure(authentication);
  app.configure(privileges);
  app.configure(usersPermissions);
  app.configure(usersOrganisations);
  app.configure(onboarding);
  app.configure(registerUser);
  app.configure(organisationsGrants);
  app.configure(organisationsJoin);
  app.configure(organisationsUsers);
  app.configure(usersApiKeys);

  // k8s probes
  app.configure(k8ProbeServices);

  app.configure(counters);

  // Admin
  app.configure(adminTruncateDatabase);
  app.configure(admin);

  // Users
  app.configure(users);

  // Organisation
  app.configure(organisations);

  // Root
  app.configure(root);
  app.configure(status);

  // Sample
  app.configure(articles);
  app.configure(badgateway);
};
