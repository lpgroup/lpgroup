import { Articles } from "./articles.class.js";
import hooks from "./articles.hooks.js";
import schema from "./articles.yup.js";

export default (app) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema,
  };
  app.use("/articles", new Articles(options, app));
  const service = app.service("articles");
  service.hooks(hooks);
};
