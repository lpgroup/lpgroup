import { Service } from "feathers-mongodb";
import { onPluginReady } from "@lpgroup/feathers-plugins";

export class Articles extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("articles");
    });
  }

  async find(params) {
    const newParams = { ...params, query: { ...params.query, $sort: { "added.at": -1 } } };
    return super.find(newParams);
  }
}
