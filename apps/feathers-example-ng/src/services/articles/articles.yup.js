import errors from "@feathersjs/errors";
import * as cy from "@lpgroup/yup";

const getBaseType = (value, yups) => {
  switch (value.type) {
    case "NEWS":
      return {
        ...yups,
        content: cy.longText().defaultNull(),
        tags: cy.arrayObject({
          _id: cy.id(),
          text: cy.alias().defaultNull(),
        }),
      };
    case "SPORTS":
      return {
        ...yups,
        content: cy.longText().defaultNull(),
        src: cy.url().defaultNull(),
      };
    case "OPINION":
      return {
        ...yups,
        content: cy.longText().defaultNull(),
        field: cy.longText().defaultNull(),
      };
    case "POLITICS":
      return {
        ...yups,
        content: cy.longText().defaultNull(),
        subContent: cy.longText().defaultNull(),
      };

    case "FEATURE":
      return {
        ...yups,
        content: cy.longText().defaultNull(),
        imageSrc: cy.url().defaultNull(),
      };

    default:
      throw new errors.GeneralError(
        "type must match the following: /(NEWS|SPORTS|OPINION|POLITICS|FEATURE)/",
      );
  }
};
const requestSchema = (value) => {
  const subSet = {
    _id: cy.id(),
    slug: cy.labelText().defaultNull(),
    type: cy
      .labelText()
      .matches(/(NEWS|SPORTS|OPINION|POLITICS|FEATURE)/)
      .default("NEWS"),
  };
  return getBaseType(value, subSet);
};

const dbSchema = {
  //   added: cy.changed(),
  //   changed: cy.changed(),
  //   owner: cy.owner(),
};

export default cy.buildValidationSchema(requestSchema, dbSchema);
