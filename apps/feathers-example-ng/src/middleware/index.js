import {
  ioHeaders,
  ioProtocol,
  ioRemoteAddress,
  restHeaders,
  restProtocol,
  restRemoteAddress,
} from "@lpgroup/feathers-utils";

export const ioMiddleware = (socket, next) => {
  ioHeaders(socket, ["user-agent", "x-forwarded-for", "x-real-ip"]);
  ioProtocol(socket);
  ioRemoteAddress(socket);

  next();
};

export const restMiddleware = (app) => {
  app.use(restHeaders(app));
  app.use(restProtocol(app));
  app.use(restRemoteAddress(app));
};
