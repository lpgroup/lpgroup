import compress from "compression";
import helmet from "helmet";
import cors from "cors";
import feathers from "@feathersjs/feathers";
import configuration from "@feathersjs/configuration";
import express from "@feathersjs/express";
import socketio from "@feathersjs/socketio";
import { log } from "@lpgroup/utils";
import { errorHandler } from "@lpgroup/feathers-utils";
import plugins from "./plugins/index.js";
import { restMiddleware } from "./middleware/index.js";
import services from "./services/index.js";
import appHooks from "./app.hooks.js";
import channels from "./channels.js";

const { error } = log("app");

const app = express(feathers());

// This service is behind a proxy.
// Will affect remoteip and protocol amongst other things.
app.enable("trust proxy");

// Load app configuration
app.configure(configuration());

// Enable security, CORS, compression and body parsing
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cors());
app.use(compress());
app.use(express.json({ limit: "10mb" }));
app.use(express.urlencoded({ limit: "10mb", extended: true }));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(
  socketio(
    {
      path: "/ws/",
      maxHttpBufferSize: 1e7,
      pingTimeout: 45000,
    },
    (io) => {
      restMiddleware(app, io);
      // Default in feathersjs is 64, increase to remove MaxListenersExceededWarning.
      io.sockets.setMaxListeners(80);
    },
  ),
);

app.configure(plugins);
app.configure(restMiddleware);
app.configure(services);
app.configure(channels);

// Configure a middleware for 404s and the error handler
// Always return rest message
app.use(express.notFound());
app.use(errorHandler());
app.hooks(appHooks);

// Close down the application if killed or getting signals.
function cleanup(signal) {
  error(`${signal} signal received on ${app.get("application")}.`);
  app.emit("SIGTERM");
}
process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);

// Catches all unhandles promise exceptions
process.on("unhandledRejection", (reason, p) => {
  const f = () => {
    error(`Unhandled Rejection at: Reason: ${reason.message} Stack: ${reason.stack}\n\n`);
    cleanup();
    // eslint-disable-next-line n/no-process-exit
    process.exit(1);
  };
  p.then(f).catch(f);
});

export default app;
