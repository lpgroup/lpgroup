import { log } from "@lpgroup/utils";

const { info } = log("app");

// TODO: What happens when several instances starts up? Will they overwrite each other?
//       Is using PUT/Update enough to avoid missing user in database?

const allStar = {
  query: [{ key: "_id", param: "*" }],
  methods: ["*"],
};

const allC = {
  query: [{ key: "_id", param: "*" }],
  methods: ["create"],
};

const allFG = {
  query: [{ key: "_id", param: "*" }],
  methods: ["find", "get"],
};

const userC = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["create"],
};

const userF = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["find"],
};

const userGUP = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["get", "update", "patch"],
};

const userGFUP = {
  query: [{ key: "owner.user._id", param: "userId" }],
  methods: ["get", "find", "update", "patch"],
};

const orgGUP = {
  query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
  methods: ["get", "update", "patch"],
};

const orgF = {
  query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
  methods: ["find"],
};

const orgGFCUPR = {
  query: [{ key: "owner.organisation.alias", param: "organisationAlias" }],
  methods: ["get", "find", "create", "update", "patch", "remove"],
};

/**
 * Used by users not authenticated with /authentication
 */
function getPublicPrivilege() {
  return {
    alias: "public",
    name: "Public",
    permissions: {
      vats: allFG,
      countries: allFG,
      organisations: allFG,
    },
  };
}

/**
 * Used by users of type: device, who authenticate with deviceId
 */
function getDeviceUser() {
  return {
    alias: "device_user",
    name: "Device user",
    permissions: {},
  };
}

/**
 * Used by users of type: user, who have email/password  /authentication
 */
function getStandardUser() {
  return {
    alias: "standard_user",
    name: "Standard user",
    permissions: {
      ...getDeviceUser().permissions,
      "articles": allStar,
      "users": userGUP,
      "users/:userId/permissions": userGFUP,
      "users/:userId/organisations": userF,
      "organisations": userC,
      "organisations/:organisationAlias/join": allC,
    },
  };
}

function getStandardOrganisations() {
  return {
    alias: "standard_organisation",
    name: "Standard organisation",
    permissions: {
      "organisations": orgGUP,
      "organisations/:organisationAlias/grants": orgGFCUPR,
      "organisations/:organisationAlias/users": orgF,
    },
  };
}

function getSuperUser() {
  const allPermissions = {
    query: [{ key: "_id", param: "*" }],
    methods: ["*"],
  };
  return {
    alias: "super_user",
    name: "Super user",
    permissions: {
      "articles": allPermissions,
      "admin": allPermissions,
      "admin/log": allPermissions,
      "admin/status": allPermissions,
      "admin/truncate/database": allPermissions,
      "counters": allPermissions,
      "organisations": allPermissions,
      "organisations/:organisationAlias/grants": allPermissions,
      "privileges": allPermissions,
      "users": allPermissions,
      "users/:userId/organisations": allPermissions,
      "users/:userId/permissions": allPermissions,
      "users/:userId/api-keys": allPermissions,
      "registers/employers": allPermissions,
      "registers/employers/:organisationNumber/signatories": allPermissions,
      "registers/employers/:organisationNumber/signatories/:signatoriesId/uri": allPermissions,
      "registers/employers/:organisationNumber/signatories/:signatoriesId/raw": allPermissions,
      "registers/employers/:organisationNumber/set-signatories": allPermissions,
      "registers/employers/:organisationNumber/history": allPermissions,
      "registers/employers/:organisationNumber/fetch": allPermissions,
    },
  };
}

async function createPrivileges(app) {
  const service = app.service("privileges");
  const params = { superUser: true };
  await service.remove("public", params).catch(() => {});
  await service.remove("device_user", params).catch(() => {});
  await service.remove("standard_user", params).catch(() => {});
  await service.remove("standard_organisation", params).catch(() => {});
  await service.remove("super_user", params).catch(() => {});
  await service.create(getPublicPrivilege(), params);
  await service.create(getDeviceUser(), params);
  await service.create(getStandardUser(), params);
  await service.create(getStandardOrganisations(), params);
  await service.create(getSuperUser(), params);
  info("Created privileges: standard_user, standard_organisations, super_user");
}

async function createUser(userId, email, privilegesAlias, app) {
  const params = { superUser: true };
  const request = {
    _id: userId,
    email,
    phone: "1-800-not-available",
    password: "The-Yellow-King@123",
    firstName: privilegesAlias,
    lastName: privilegesAlias,
    type: "user",
    privileges: [{ privilegesAlias, params: {} }],
  };

  const result = await app
    .service("users")
    .create(request, params)
    .catch((err) => {
      if (err.message.startsWith("E11000 duplicate key error collection")) {
        return app.service("users").update(userId, request, params);
      }
      throw err;
    });
  info("Created user: ", email);
  return result;
}

export default async (app) => {
  const result = await Promise.all([
    createPrivileges(app),
    createUser("c6e75342-2160-4703-94d8-b0bb291ea3c3", "system@carcosa.se", "super_user", app),
    createUser(
      "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
      "system.import@carcosa.se",
      "super_user",
      app,
    ),
    createUser("2127505c-6703-4985-9e98-5a8f483011a8", "system.nats@carcosa.se", "super_user", app),
    createUser("5a1f6ebb-5e8e-4158-add1-ebf391314cee", "anonymous@carcosa.se", "public", app),
  ]);
  return { "system@carcosa.se": result["1"] };
};
