/* eslint-disable no-console */
import Table from "table-layout";
import { onPluginsReady, closePlugins } from "@lpgroup/feathers-plugins";
import { log } from "@lpgroup/utils";
import app from "./app.js";
import load from "./load.js";

const { info } = log("app");

const port = app.get("port");

console.log("feathers-example Engine (%s %s)", app.get("application"), app.get("version"));
const table = new Table(
  [
    { name: "env:", value: app.get("env") },
    { name: "environment: ", value: app.get("environment") },
    { name: "node_env: ", value: process.env.NODE_ENV || "NA" },
    { name: "node_config_env: ", value: process.env.NODE_CONFIG_ENV || "NA" },
    { name: "debug: ", value: process.env.DEBUG || "NA" },
  ],
  { maxWidth: 60 },
);

console.log(table.toString());

onPluginsReady().then(() => {
  const server = app.listen(port);
  server.on("listening", async () => {
    await load(app);
    app.set("status").ready = "OK";
    const serviceList = Object.keys(app.services);
    info("Providing the following endpoints");
    info(serviceList.join("\n"));
    info(`Listening on: http://${app.get("host")}:${port}`);

    app.on("SIGTERM", async () => {
      await closePlugins();
      server.close();
    });
  });
});
