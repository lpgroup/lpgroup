import { addPluginWithOptions, mongodb, nats, sync, axios } from "@lpgroup/feathers-plugins";

export default (app) => {
  addPluginWithOptions("mongodb", mongodb, { uri: app.get("mongodb") });
  addPluginWithOptions("nats", nats, app.get("nats"));
  addPluginWithOptions("axios", axios, {});
  addPluginWithOptions("sync", sync, { app });
};
