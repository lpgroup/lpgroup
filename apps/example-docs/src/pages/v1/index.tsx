/* eslint-disable n/no-unsupported-features/es-syntax */

import React, { useEffect } from "react";
import { useSetUser, useUser } from "@lpgroup/docs";
import Introduction from "./01-introduction.mdx";
import CommonInformation from "./04-common-information.mdx";
import Errors from "./99-errors.mdx";
import { password } from "../../config";

export default function Pages() {
  // @ts-ignore
  const nodeConfig = window.NODE_CONFIG_ENV;
  const { apiKey, isLogin } = useUser();
  const { setIsLogin, setApiKey } = useSetUser();
  const [userPages, setUserPages] = React.useState<any[]>([]);

  useEffect(() => {
    if (apiKey === password) {
      setIsLogin(true);
      setUserPages([]);
    } else {
      setApiKey("");
    }
  }, [apiKey]);

  useEffect(() => {
    if (!isLogin) return setUserPages([]);
  }, [isLogin]);

  if (nodeConfig === "uat" || nodeConfig === "prod") {
    const uatProdPages = [Introduction, CommonInformation, ...userPages, Errors];
    return (
      <>
        {uatProdPages.map((Page, idx) => (
          <Page key={idx} />
        ))}
      </>
    );
  }

  const intPages = [Introduction, CommonInformation, Errors];

  return (
    <>
      {intPages.map((Page, idx) => (
        <Page key={idx} />
      ))}
    </>
  );
}
