// Default font already imported via library
// const systemFont = `Raleway, Lato, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", sans-serif`;
const systemFont = `Lato, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", sans-serif`;

export const password = "cybercow";

export const isAuth = false;

// Optional tweaks
export const tweak = {
  textAlign: "justify",
  sideBarHeight: 51,
  sideBarWidth: { md: 256, lg: 290, xl: 310 },
  // Fallback font
  font: systemFont,
  // Default typography
  sxTopBar: {
    left: 0,
    width: { md: 256, lg: 290, xl: 310 },
    borderInline: "1px solid rgba(0, 0, 0, 0.12)",
  },
  typography: {
    h1: {
      fontSize: "2.2rem",
      fontWeight: 600,
      fontFamily: systemFont,
      color: "#3cf",
    },
    h2: {
      fontSize: "1.5rem",
      fontWeight: 500,
      fontFamily: systemFont,
    },

    h3: {
      fontSize: "1.3rem",
      fontWeight: 500,
      fontFamily: systemFont,
    },
    h4: {
      fontSize: "1rem",
      fontWeight: 500,
    },
    body1: {
      lineHeight: 1.4,
      fontFamily: systemFont,
    },
    body2: {
      "fontSize": 15,
      "fontFamily": systemFont,
      "&:hover": {
        color: "#6969ff",
      },
    },
  },
};
