import React from "react";
import LPDocs, { DocsSkeleton } from "@lpgroup/docs";
import { SampleLogo } from "./custom/SampleLogo";
import { tweak, isAuth } from "./config";

const Pages = React.lazy(() => import("./pages/v1"));

function App() {
  return (
    <LPDocs
      Logo={SampleLogo}
      sideBarHeight={tweak.sideBarHeight}
      isAuth={isAuth}
      textAlign={tweak.textAlign}
      sideBarWidth={tweak.sideBarWidth}
      typography={tweak.typography}
      // sxTopBar={tweak.sxTopBar}
    >
      <React.Suspense fallback={<DocsSkeleton />}>
        <Pages />
      </React.Suspense>
    </LPDocs>
  );
}

export default App;
