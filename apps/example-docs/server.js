/**
 * This file will be executed as a commonJS file, together with files in dist.
 */
/* eslint-disable no-underscore-dangle */

import path from "node:path";
import express from "express";

const __dirname = path.resolve();

const app = express();

app.get("/assets/*", (req, res) => {
  res.sendFile(path.join(__dirname, `dist/assets/${req.params["0"]}`));
});

app.use("/healthy", (req, res) => res.send("healthy"));
app.use("/ready", (req, res) => res.send("ready"));

app.get("*", (req, res) => {
  if (process.env.NODE_CONFIG_ENV === "prod") {
    res.sendFile(path.join(__dirname, "dist/index-prod.html"));
  } else if (process.env.NODE_CONFIG_ENV === "uat") {
    res.sendFile(path.join(__dirname, "dist/index-uat.html"));
  } else {
    res.sendFile(path.join(__dirname, "dist/index-int.html"));
  }
});

app.listen(8284, (err) => {
  if (err) {
    return console.error(err);
  }
  console.log("Listening at http://localhost:8284"); // eslint-disable-line no-console
  return undefined;
});
