/* eslint-disable unicorn/prefer-node-protocol */
/* eslint-disable unicorn/import-style */

import { resolve, dirname } from "node:path";
import { fileURLToPath } from "node:url";
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { VitePluginRadar } from "vite-plugin-radar";
import { visualizer } from "rollup-plugin-visualizer";
import dns from "node:dns";
import { VitePluginReplace } from "@lpgroup/vite-plugin-replace";
import remarkGfm from "remark-gfm";
import mdx from "@mdx-js/rollup";

dns.setDefaultResultOrder("verbatim"); // use localhost instead of 127.0.0.1
const directory = dirname(fileURLToPath(import.meta.url));

const replaceArr = [
  {
    src: "http://localhost:8189/harper-0.0.1.umd.js",
    production: "/harper/harper-0.0.1.umd.js",
  },
  { src: "http://localhost:8189/style.css", production: "/harper/style.css" },
  { src: "http://localhost:8180/", production: "/api/v1/" },
];

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  return {
    server: {
      port: 8284,
    },
    mode,
    build: {
      sourcemap: false,
      chunkSizeWarningLimit: 600,
      rollupOptions: {
        input: {
          prod: resolve(directory, "index-prod.html"),
          uat: resolve(directory, "index-uat.html"),
          int: resolve(directory, "index-int.html"),
        },
      },
    },

    plugins: [
      // @ts-ignore
      VitePluginReplace({ replace: replaceArr, mode }),
      react(),
      // @ts-ignore
      visualizer({
        open: false,
        gzipSize: true,
        brotliSize: true,
      }),
      mdx({
        // See https://mdxjs.com/advanced/plugins
        providerImportSource: "@mdx-js/react",
        remarkPlugins: [remarkGfm],
        rehypePlugins: [],
      }),
      VitePluginRadar({
        // Google Analytics tag injection
        analytics: {
          id: "G-4JWG9T7PT8",
        },
      }),
    ],
    optimizeDeps: {
      include: ["react/jsx-runtime"],
    },
  };
});
