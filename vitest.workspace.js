// eslint-disable-next-line import/no-unresolved
import { defineWorkspace } from "vitest/config";

export default defineWorkspace(["./vitest.config.mjs", "./packages/admin-ui/vite.config.js"]);
