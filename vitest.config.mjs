/* eslint-disable import/no-unresolved, import/no-extraneous-dependencies */
import react from "@vitejs/plugin-react";
import { defineConfig } from "vitest/config";

export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: "jsdom",
    css: true,
    setupFiles: "./viteTestSetup.js",
    coverage: {
      reporter: ["text", "html"],
    },
  },
});
